"""
Modelos para las pólizas y planes de seguro. Los planes constituyen un conjunto
de tipos servicios que las compañías ofrecen como producto. Las pólizas son los
contratos que los afiliados realizan con las compañías de seguro en el marco de
una póliza en particular.
"""
from django.db import models
from django.utils.translation import gettext_lazy as _

from locations.models import Organization
from services.models import ServiceType


class Plan(models.Model):
    """
    Plan. Conjunto de servicios que se ofrecen juntos como producto.
    """

    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        related_name="plans",
        verbose_name=_("organización"),
    )
    service_types = models.ManyToManyField(
        ServiceType, related_name="plans", verbose_name=_("Servicios incluidos")
    )
    name = models.CharField(_("nombre"), max_length=200)
    private = models.BooleanField(_("es privado"), default=False)

    def save(self, *args, **kwargs):
        if self.organization:
            organization = Organization.objects.get(pk=self.organization.id)
            organization.has_plans = True
            organization.save()
        super(Plan, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Plan")
        verbose_name_plural = _("Planes")


class Policy(models.Model):
    """
    Póliza.
    """

    POLICY_TYPE = (
        ("INDIVIDUAL", _("Individual")),
        ("GROUP", _("Colectivo")),
    )
    plan = models.ForeignKey(
        Plan, on_delete=models.CASCADE, related_name="policies", verbose_name=_("plan")
    )
    sponsor = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        related_name="policies",
        verbose_name=_("contratante"),
        default="",
        blank=True,
        null=True,
    )
    code = models.CharField(_("nombre"), max_length=200)
    authorized_by = models.CharField(
        _("autorizado por"), max_length=50, blank=True, null=True
    )
    policy_type = models.CharField(
        _("tipo de contrato"), max_length=15, default="INDIVIDUAL", choices=POLICY_TYPE
    )

    def __str__(self):
        return f"{self.sponsor.name} {self.plan.name} {self.code}"

    class Meta:
        verbose_name = _("Póliza")
        verbose_name_plural = _("Pólizas")


class ValidatorPlanMapping(models.Model):
    """
    Correspondencia entre el Validador y los Planes.
    """

    insurance_company = models.CharField(
        _("empresa"),
        max_length=100,
    )
    branch = models.CharField(
        _("ramo"),
        max_length=100,
    )
    policy_type = models.CharField(
        _("tipo de póliza"),
        max_length=100,
    )
    policy_category = models.CharField(
        _("categoría de póliza"),
        max_length=100,
    )
    plan = models.CharField(
        _("plan"),
        max_length=100,
    )
    sponsor = models.CharField(_("contratante"), max_length=100)

    def __str__(self):
        return f"({self.insurance_company}, {self.branch}, {self.policy_type}, {self.policy_category}) > ({self.plan}, {self.sponsor}) "

    class Meta:
        verbose_name = _("Equivalencia Planes Validador")
        verbose_name_plural = _("Equivalencias Planes Validador")
        ordering = ("id",)
