from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import Plan, Policy, ValidatorPlanMapping


class PolicyResource(resources.ModelResource):
    class Meta:
        model = Policy


class PolicyAdmin(ImportExportModelAdmin):
    model = Policy
    fields = (("code"), "plan", "sponsor", "authorized_by", "policy_type")
    list_display = ("code", "affiliates_list", "plan_name", "sponsor_name")
    list_filter = ("policy_type", )
    search_fields = [
        "affiliates__first_name",
        "affiliates__last_name",
        "affiliates__dni",
        "plan__organization__name",
        "sponsor__name"
    ]

    def affiliates_list(self, obj):
        return "; ".join(
            [
                f"{kinship.affiliate.first_name}, {kinship.affiliate.last_name}: {kinship.kinship}"
                for kinship in obj.affiliatekinship_set.all()
            ]
        )
    affiliates_list.short_description = "Afiliados"

    def plan_name(self, obj):
        return obj.plan.name
    plan_name.short_description = "Plan"

    def sponsor_name(self, obj):
        return obj.sponsor.name
    sponsor_name.short_description = "Contratante"


class PlanResource(resources.ModelResource):
    class Meta:
        model = Plan


class PlanAdmin(ImportExportModelAdmin):
    model = Plan
    fields = (("name", "private"), "organization", "service_types")
    list_display = ("name", "organization", "private")
    list_filter = ("private", "service_types")
    search_fields = ["name", "organization__name"]


class ValidatorPlanMappingResource(resources.ModelResource):
    class Meta:
        model = ValidatorPlanMapping


class ValidatorPlanMappingAdmin(ImportExportModelAdmin):
    model = ValidatorPlanMapping


admin.site.register(Plan, PlanAdmin)
admin.site.register(Policy, PolicyAdmin)
admin.site.register(ValidatorPlanMapping, ValidatorPlanMappingAdmin)
