"""
Modelos, vistas y serializadores de planes y pólizas. Incluye también un recurso
con una tabla que permite mapear algunas categorías de Atención1 con el
Validador de Venemergencia.
"""

default_app_config = 'policies.apps.PoliciesConfig'
