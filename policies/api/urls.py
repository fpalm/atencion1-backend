#from rest_framework import routers
from django.urls import path

from . import views

app_name = 'policies'

urlpatterns = [
    path(
        'plans',
        views.PlanListView.as_view(),
        name='plan_list'
    ),
    path(
        'plans/<pk>',
        views.PlanDetailView.as_view(),
        name='plan_detail'
    ),
    path(
        'policies',
        views.PolicyListView.as_view(),
        name='policy_list'
    ),
    path(
        'policies/<pk>',
        views.PolicyDetailView.as_view(),
        name='policy_detail'
    ),
    path(
        'policies-simple',
        views.PolicySimpleListView.as_view(),
        name='policy_simple_list'
    ),
    path(
        'policies-simple/<pk>',
        views.PolicySimpleDetailView.as_view(),
        name='policy_simple_detail'
    ),
    path(
        'validator-plan-mapping',
        views.ValidatorPlanMappingView.as_view(),
        name='validator_plan_mapping'
    )
]
