from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator

from policies.models import Plan, Policy, ValidatorPlanMapping
from .serializers import (
    PlanSerializer,
    PolicySerializer,
    PolicySimpleSerializer,
    ValidatorPlanMappingSerializer,
)


# from profiles.custom_permissions import (IsAnyAuthenticatedUser,
#                                          IsOperatorUser,
#                                          IsCoordinatorUser,
#                                          IsManagerUser
#                                          )


@method_decorator(cache_page(60*2), name='dispatch')
class PlanListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Plan.objects.all()
    serializer_class = PlanSerializer

    filter_fields = (
        "organization",
        "service_types",
        "name",
        "private",
    )
    search_fields = (
        "^organization",
        "^service_types",
        "^name",
    )
    ordering_fields = (
        "organization",
        "service_types",
        "name",
    )


class PlanDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Plan.objects.all()
    serializer_class = PlanSerializer


class PolicyListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Policy.objects.all()
    serializer_class = PolicySerializer

    filter_fields = (
        "plan",
        "sponsor",
        "policy_type",
    )
    search_fields = (
        "^plan",
        "code",
        "^authorized_by",
    )
    ordering_fields = (
        "plan",
        "sponsor",
        "code",
    )


class PolicyDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Policy.objects.all()
    serializer_class = PolicySerializer


class PolicySimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Policy.objects.all()
    serializer_class = PolicySimpleSerializer

    filter_fields = (
        "plan",
        "sponsor",
        "policy_type",
    )
    search_fields = (
        "^plan",
        "code",
        "^authorized_by",
    )
    ordering_fields = (
        "plan",
        "sponsor",
        "code",
    )


class PolicySimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Policy.objects.all()
    serializer_class = PolicySimpleSerializer


class ValidatorPlanMappingView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ValidatorPlanMapping.objects.all()
    serializer_class = ValidatorPlanMappingSerializer
