from rest_framework import serializers

from affiliates.models import Affiliate, AffiliateKinship
from locations.api.serializers import OrganizationSerializer
from policies.models import Plan, Policy, ValidatorPlanMapping
from services.api.serializers import ServiceTypeSerializer


class PlanSerializer(serializers.ModelSerializer):

    organization = OrganizationSerializer()
    service_types = ServiceTypeSerializer(read_only=True, many=True)

    class Meta:
        model = Plan
        fields = (
            "id",
            "name",
            "organization",
            "service_types",
            "private",
        )


class KinshipAffiliateSerializer(serializers.ModelSerializer):

    depth = 1

    class Meta:
        model = Affiliate
        fields = "__all__"


class PolicyKinshipSerializer(serializers.ModelSerializer):

    affiliate = KinshipAffiliateSerializer(read_only=True)

    class Meta:
        model = AffiliateKinship
        fields = (
            "id",
            "affiliate",
            "kinship",
        )


class PolicyKinshipSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffiliateKinship
        fields = (
            "id",
            "affiliate",
            "kinship",
        )


class PolicySerializer(serializers.ModelSerializer):

    affiliates = PolicyKinshipSerializer(
        source="affiliatekinship_set", many=True, read_only=True
    )
    sponsor = OrganizationSerializer(read_only=True)
    plan = PlanSerializer(read_only=True)
    policy_type = serializers.CharField(source="get_policy_type_display")

    class Meta:
        model = Policy
        fields = [
            "id",
            "code",
            "plan",
            "sponsor",
            "affiliates",
            "authorized_by",
            "policy_type",
        ]


class PolicySimpleSerializer(serializers.ModelSerializer):

    #    affiliates = PolicyKinshipSimpleSerializer(source='affiliatekinship_set', many=True, read_only=True)
    #    plan = PlanSerializer(read_only=True)

    class Meta:
        model = Policy
        fields = [
            "id",
            "code",
            "plan",
            "sponsor",
            "affiliates",
            "authorized_by",
            "policy_type",
        ]

class ValidatorPlanMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ValidatorPlanMapping
        fields = "__all__"
