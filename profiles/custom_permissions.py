from django.contrib.auth.models import Group
from rest_framework import permissions


def _is_in_group(user, group_name):
    """
    Takes a user and a group name, and returns `True` if the user is in that group.
    """
    try:
        return Group.objects.get(name=group_name).user_set.filter(id=user.id).exists()
    except Group.DoesNotExist:
        return None


def _has_group_permission(user, required_groups):
    return any([_is_in_group(user, group_name) for group_name in required_groups])


SAFE_METHODS = ['GET', 'HEAD', 'OPTIONS']


class IsAdminUser(permissions.BasePermission):
    # group_name for super admin
    required_groups = ['Admin']

    def has_permission(self, request, view):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission

    def has_object_permission(self, request, view, obj):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission


class IsAnyAuthenticatedUser(permissions.BasePermission):
    # group_name for super admin
    required_groups = ['Admin', 'Operador', 'Despachador', 'Médico', 'Coordinador', 'Gerente']

    def has_permission(self, request, view):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission

    def has_object_permission(self, request, view, obj):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission


class IsOperatorUser(permissions.BasePermission):
    required_groups = ['Operador']

    def has_permission(self, request, view):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return (request.user and has_group_permission) or request.method in SAFE_METHODS

    def has_object_permission(self, request, view, obj):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission or request.method in SAFE_METHODS


class IsDispatcherUser(permissions.BasePermission):
    required_groups = ['Despachador']

    def has_permission(self, request, view):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission

    def has_object_permission(self, request, view, obj):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission


class IsPractitionerUser(permissions.BasePermission):
    required_groups = ['Médico']

    def has_permission(self, request, view):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission

    def has_object_permission(self, request, view, obj):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission


class IsCoordinatorUser(permissions.BasePermission):
    required_groups = ['Coordinador']

    def has_permission(self, request, view):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission

    def has_object_permission(self, request, view, obj):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission


class IsManagerUser(permissions.BasePermission):
    required_groups = ['Gerente']

    def has_permission(self, request, view):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission

    def has_object_permission(self, request, view, obj):
        has_group_permission = _has_group_permission(request.user, self.required_groups)
        return request.user and has_group_permission



class IsOwner(permissions.BasePermission):
    """
    Custom permission to allow only owners of an object or its person attribute
    to have access over it.

    Permissions are granted only when the received obj object or its person
    attribute (obj.person) matches the user that originated the request
    (request.user).
    """
    def has_object_permission(self, request, view, obj):
        if hasattr(obj, 'person'):
            return obj.person == request.user
        else:
            return obj == request.user
