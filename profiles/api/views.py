from django.contrib.auth.models import User
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from profiles.models import Profile
from .serializers import UserSerializer, ProfileSerializer, ProfileSimpleSerializer


# from profiles.custom_permissions import (IsAnyAuthenticatedUser,
#                                          IsOperatorUser,
#                                          IsCoordinatorUser,
#                                          IsManagerUser
#                                          )


class UserListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProfileListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    filter_fields = (
        "user",
        "dni",
    )
    search_fields = ("^dni",)


class ProfileDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

class ProfileSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Profile.objects.all()
    serializer_class = ProfileSimpleSerializer


class ProfileSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Profile.objects.all()
    serializer_class = ProfileSimpleSerializer
