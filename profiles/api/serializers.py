from rest_framework import serializers

from profiles.models import Profile
from django.contrib.auth.models import User, Group


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ("name",)


class UserSerializer(serializers.ModelSerializer):

    groups = GroupSerializer(many=True)

    class Meta:
        model = User
        fields = ("id", "username", "first_name", "last_name", "email", "groups")


class ProfileSerializer(serializers.ModelSerializer):

    user = UserSerializer()

    class Meta:
        model = Profile
        fields = (
            "id",
            "user",
            "phone_number",
            "photo",
            "birth_date",
            "dni",
        )


class ProfileSimpleSerializer(serializers.ModelSerializer):

    # user = UserSerializer()

    class Meta:
        model = Profile
        fields = ("user", "phone_number", "birth_date", "dni")

    # def create(self, validated_data):
    #     profile = Profile.objects.create(**validated_data)
    #     return profile

    def update(self, instance, validated_data):
        user = validated_data.get("user")
        instance.save()
        profile = super().update(instance, validated_data)
        Profile.objects.get(pk=instance.id).save()
        return profile
