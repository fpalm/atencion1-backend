from django.urls import path
from . import views

app_name = 'profiles'

urlpatterns = [
    path(
        'users',
        views.UserListView.as_view(),
        name='user_list'
    ),
    path(
        'users/<pk>',
        views.UserDetailView.as_view(),
        name='user_detail'
    ),
    path(
        'profiles',
        views.ProfileListView.as_view(),
        name='profile_list'
    ),
    path(
        'profiles/<pk>',
        views.ProfileDetailView.as_view(),
        name='profile_detail'
    ),
    path(
        'profiles-simple',
        views.ProfileSimpleListView.as_view(),
        name='profile_simple_list'
    ),
    path(
        'profiles-simple/<pk>',
        views.ProfileSimpleDetailView.as_view(),
        name='profile_simple_detail'
    ),
]