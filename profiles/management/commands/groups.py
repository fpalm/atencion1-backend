import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'atencion1.settings')

import django
from django.core.management.base import BaseCommand

django.setup()
from django.contrib.auth.models import Group

class Command(BaseCommand):
    help = 'Crea los roles de usuarios para Atención-1.'

    def handle(self, *args, **options):
        GROUPS = ['Admin', 'Operador', 'Despachador', 'Médico', 'Coordinador', 'Gerente', 'Asistente', 'Director']
        MODELS = ['user'] # Revisar esto

        for group in GROUPS:
            new_group, created = Group.objects.get_or_create(name=group)

            if created:
                self.stdout.write(self.style.SUCCESS('Grupo "%s" creado satisfactoriamente.' % new_group))
            else:
                self.stdout.write(self.style.SUCCESS('El grupo "%s" ya existía en la base de datos.' % new_group))