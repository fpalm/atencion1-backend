"""
Modelo de los perfiles de usuarios. Incluye información de los sellos y firmas
de los médicos.
"""

from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField


class Profile(models.Model):
    """
    Perfil.
    """

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, verbose_name=_("usuario")
    )
    phone_number = PhoneNumberField(_("número de teléfono"))
    photo = models.ImageField(
        _("foto de perfil"), upload_to="profile_photos/", blank=True
    )
    stamp = models.ImageField(
        _("sello médico"), upload_to="profile_stamps/", blank=True
    )
    signature = models.ImageField(
        _("firma del médico"), upload_to="profile_signatures/", blank=True
    )
    MPPS_id = models.IntegerField(_("número en MPPS"), blank=True, null=True)
    birth_date = models.DateField(_("fecha de nacimiento"), blank=True, null=True)
    dni = models.CharField(
        _("documento de identidad"),
        max_length=50,
        blank=False,
        unique=True,
        null=True,
        default=None,
        validators=[
            RegexValidator(
                regex="^[V|E|P]\d{6,9}$", message="Documento de identidad no válido",
            ),
        ],
    )

    def __str__(self):
        return f"Profile: {self.user.username} / {self.user.first_name} {self.user.last_name} / {self.dni}"


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
