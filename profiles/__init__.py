"""
Modelos, vistas y serializadores relacionados con los perfiles de los usuarios
de Atención1. En el directorio management/commands se encuentra un comando de
Python que permite crear automáticamente los grupos de usuarios, este es un paso
previo para el despliegue de la aplicación.
"""

default_app_config = 'profiles.apps.ProfilesConfig'
