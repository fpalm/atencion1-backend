from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import User, Profile


class UserResource(resources.ModelResource):
    class Meta:
        model = User
        fields = ['id', 'password', 'groups', 'username', 'first_name', 'last_name', 'email']


class ProfileResource(resources.ModelResource):
    class Meta:
        model = Profile
        fields = ['user', 'phone_number', 'birth_date', 'dni']


admin.site.unregister(User)


def add_profile_image(admin_class):
    def profile_image(self, obj):
        return mark_safe(
            '<img src="{url}" width="{width}" height={height} />'.format(
                url=obj.photo.url, width=obj.photo.width, height=obj.photo.height,
            )
        )

    profile_image.short_description = _("imágen de perfil")
    admin_class.profile_image = profile_image
    return admin_class


@add_profile_image
@admin.register(Profile)
class ProfileAdmin(ImportExportModelAdmin):
    model = Profile
    readonly_fields = ('profile_image',)
    verbose_name = _('imágen de perfil')


@add_profile_image
class ProfileInline(admin.TabularInline):
    model = Profile
    readonly_fields = ('profile_image',)
    verbose_name = _('imágen de perfil')


@admin.register(User)
class UserAdmin(UserAdmin, ImportExportModelAdmin):
    resource_class = UserResource
    list_display = ('username', 'is_staff', 'is_active', 'is_superuser')
    # inlines = (ProfileInline,)
