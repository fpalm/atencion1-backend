# Generated by Django 2.2.7 on 2020-09-23 22:40

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(max_length=128, region=None, verbose_name='número de teléfono')),
                ('photo', models.ImageField(blank=True, upload_to='profile_photos/', verbose_name='foto de perfil')),
                ('stamp', models.ImageField(blank=True, upload_to='profile_stamps/', verbose_name='sello médico')),
                ('signature', models.ImageField(blank=True, upload_to='profile_signatures/', verbose_name='firma del médico')),
                ('MPPS_id', models.IntegerField(blank=True, null=True, verbose_name='número en MPPS')),
                ('birth_date', models.DateField(blank=True, null=True, verbose_name='fecha de nacimiento')),
                ('dni', models.CharField(default=None, max_length=50, null=True, unique=True, validators=[django.core.validators.RegexValidator(message='Documento de identidad no válido', regex='^[V|E|P]\\d{6,9}$')], verbose_name='documento de identidad')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='usuario')),
            ],
        ),
    ]
