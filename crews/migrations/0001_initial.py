# Generated by Django 2.2.7 on 2020-09-23 22:40

import datetime
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc
import django.utils.timezone
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Base',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('base_name', models.CharField(max_length=50, verbose_name='base')),
                ('description', models.TextField(blank=True, null=True, verbose_name='descripción')),
                ('coverage', models.TextField(blank=True, null=True, verbose_name='cobertura')),
                ('schedule', models.TextField(blank=True, null=True, verbose_name='horario')),
            ],
            options={
                'verbose_name': 'Base de Operaciones',
                'verbose_name_plural': 'Bases de Operaciones',
            },
        ),
        migrations.CreateModel(
            name='Crew',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=255, verbose_name='nombre')),
                ('start', models.DateTimeField(blank=True, default=datetime.datetime(1980, 1, 1, 0, 0, tzinfo=utc), null=True, verbose_name='inicio de la guardia')),
                ('end', models.DateTimeField(blank=True, default=datetime.datetime(1980, 1, 1, 0, 0, tzinfo=utc), null=True, verbose_name='fin de la guardia')),
                ('base', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='crews', to='crews.Base', verbose_name='base')),
            ],
            options={
                'verbose_name': 'tripulación',
                'verbose_name_plural': 'tripulaciones',
            },
        ),
        migrations.CreateModel(
            name='MobileUnit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codename', models.CharField(max_length=50, verbose_name='nombre código')),
                ('unit_type', models.CharField(choices=[('ALPHA', 'Alfa'), ('BRAVO', 'Bravo'), ('DELTA', 'Delta'), ('OTHER', 'Otro')], max_length=6, verbose_name='tipo de unidad')),
                ('license_plate', models.CharField(max_length=7, verbose_name='matrícula')),
            ],
            options={
                'verbose_name': 'Unidad Móvil',
                'verbose_name_plural': 'Unidades Móviles',
            },
        ),
        migrations.CreateModel(
            name='Practitioner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='nombres')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='apellidos')),
                ('gender', models.CharField(blank=True, choices=[('FEMALE', 'Femenino'), ('MALE', 'Masculino')], max_length=20, verbose_name='género')),
                ('birthdate', models.DateField(blank=True, default=datetime.datetime(1980, 1, 1, 0, 0, tzinfo=utc), verbose_name='fecha de nacimiento')),
                ('home_address', models.TextField(blank=True, null=True, verbose_name='Dirección')),
                ('is_active', models.BooleanField(default=True, help_text='Indica si este usuario debe ser tratado como activo. Deselecciona esto en lugar de eliminar cuentas.', verbose_name='activo')),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, region=None, verbose_name='número de teléfono')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='fecha de ingreso')),
                ('dni', models.CharField(default='V00000000', max_length=50, validators=[django.core.validators.RegexValidator(message='Documento de identidad no válido', regex='^[V|E]+\\d{6,9}$')], verbose_name='documento de identidad')),
                ('is_paramedic', models.BooleanField(default=False, help_text='Indica si este miembro de la tripulación es paramédico.', verbose_name='es paramédico')),
                ('is_driver', models.BooleanField(default=False, help_text='Indica si este miembro de la tripulación puede conducir.', verbose_name='es conductor')),
                ('is_medic', models.BooleanField(default=False, help_text='Indica si este miembro de la tripulación es médico.', verbose_name='es médico')),
                ('is_coordinator', models.BooleanField(default=False, help_text='Indica si este miembro de la tripulación es coordinador.', verbose_name='es coordinador')),
            ],
            options={
                'verbose_name': 'profesional médico',
                'verbose_name_plural': 'profesionales médicos',
            },
        ),
        migrations.CreateModel(
            name='PractitionerRole',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('practitioner_role', models.CharField(choices=[('PARAMEDIC', 'Paramédico'), ('DRIVER', 'Conductor'), ('PRACTITIONER', 'Médico'), ('COORDINATOR', 'Coordinador')], max_length=15, verbose_name='rol del tripulante')),
                ('crew', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='crews.Crew')),
                ('practitioner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crews.Practitioner')),
            ],
            options={
                'verbose_name': 'Rol del profesional médico',
                'verbose_name_plural': 'Roles de los profesionales médicos',
            },
        ),
        migrations.AddField(
            model_name='crew',
            name='mobile_unit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='crews', to='crews.MobileUnit', verbose_name='unidad móvil'),
        ),
        migrations.AddField(
            model_name='crew',
            name='practitioners',
            field=models.ManyToManyField(related_name='crews', through='crews.PractitionerRole', to='crews.Practitioner', verbose_name='profesionales médicos'),
        ),
    ]
