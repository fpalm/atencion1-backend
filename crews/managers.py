from django.db import models
from django.db.models import Q


class CrewManager(models.Manager):
    """Manager para filtrar el modelo de Tripulaciones.
    """

    def current_day(self, date):
        """Todas las tripulaciones activas en el momento de la consulta.
        
        Arguments:
            date {datetime} -- Datetime object.
        
        Returns:
            list -- Lista de tripulaciones filtradas.
        """
        queryset = self.get_queryset()
        
        return queryset.filter(Q(start__lte=date), Q(end__gte=date))