"""
Modelos, vistas y serializadores relacionados con el modelo de tripulaciones.
Incluye modelos relacionados como unidades móviles, bases y profesionales
médicos que conforman las tripulaciones.
"""

default_app_config = 'crews.apps.CrewsConfig'