from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin


from .models import Base, MobileUnit, Crew, Practitioner, PractitionerRole


class BaseResource(resources.ModelResource):

    class Meta:
        model = Base


class BaseAdmin(ImportExportModelAdmin):
    model = Base


class MobileUnitResource(resources.ModelResource):

    class Meta:
        model = MobileUnit


class MobileUnitAdmin(ImportExportModelAdmin):
    model = MobileUnit


class PractitionerResource(resources.ModelResource):

    class Meta:
        model = Practitioner


class PractitionerAdmin(ImportExportModelAdmin):
    model = Practitioner


class CrewResource(resources.ModelResource):

    class Meta:
        model = Crew


class PractitionerRoleInLine(admin.TabularInline):
    model = PractitionerRole


class CrewAdmin(ImportExportModelAdmin):
    inlines=(PractitionerRoleInLine,)

class PractionerRoleResource(resources.ModelResource):

    class Meta:
        model = PractitionerRole


class PractionerRoleAdmin(ImportExportModelAdmin):
    model = PractitionerRole


admin.site.register(Base, BaseAdmin)
admin.site.register(MobileUnit, MobileUnitAdmin)
admin.site.register(Crew, CrewAdmin) 
admin.site.register(Practitioner, PractitionerAdmin)
admin.site.register(PractitionerRole, PractionerRoleAdmin)