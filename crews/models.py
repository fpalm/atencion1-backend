"""
Modelos relacionados con las tripulaciones. Una tripulación está determinada
por los profesionales médicos asignados a una unidad móvil por un período de
tiempo determinado, la guardia.
"""

from django.db import models

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.conf import settings

from affiliates.models import Affiliate
from crews.managers import CrewManager

import datetime, pytz
from django.core.validators import RegexValidator
from phonenumber_field.modelfields import PhoneNumberField

default_btime = datetime.datetime.strptime("1980-01-01", "%Y-%m-%d")
default_btime = default_btime.replace(tzinfo=pytz.timezone(settings.TIME_ZONE))


class MobileUnit(models.Model):
    """
    **Unidad Móvil**
    """

    UNIT_TYPE = (
        ("ALPHA", _("Alfa")),
        ("BRAVO", _("Bravo")),
        ("DELTA", _("Delta")),
        ("OTHER", _("Otro")),
    )

    codename = models.CharField(_("nombre código"), max_length=50,)
    unit_type = models.CharField(_("tipo de unidad"), max_length=6, choices=UNIT_TYPE)
    license_plate = models.CharField(_("matrícula"), max_length=7,)

    def __str__(self):
        return self.codename

    class Meta:
        verbose_name = _("Unidad Móvil")
        verbose_name_plural = _("Unidades Móviles")


class Base(models.Model):
    """
    **Base de Operaciones**
    """

    base_name = models.CharField(_("base"), max_length=50,)

    description = models.TextField(_("descripción"), null=True, blank=True)
    coverage = models.TextField(_("cobertura"), null=True, blank=True)
    schedule = models.TextField(_("horario"), null=True, blank=True)
    # TODO add organization

    def __str__(self):
        return self.base_name

    class Meta:
        verbose_name = _("Base de Operaciones")
        verbose_name_plural = _("Bases de Operaciones")


class Practitioner(models.Model):
    """
    **Profesional Médico**
    """

    GENDER = (
        ("FEMALE", _("Femenino")),
        ("MALE", _("Masculino")),
    )

    first_name = models.CharField(_("nombres"), max_length=150, blank=True)
    last_name = models.CharField(_("apellidos"), max_length=150, blank=True)
    gender = models.CharField(_("género"), max_length=20, blank=True, choices=GENDER)
    birthdate = models.DateField(
        _("fecha de nacimiento"), default=default_btime, blank=True
    )
    home_address = models.TextField(_("Dirección"), null=True, blank=True)
    # TODO foreignkey to address model
    is_active = models.BooleanField(
        _("activo"),
        default=True,
        help_text=_(
            "Indica si este usuario debe ser tratado como activo. "
            "Deselecciona esto en lugar de eliminar cuentas."
        ),
    )
    phone_number = PhoneNumberField(_("número de teléfono"), blank=True)
    date_joined = models.DateTimeField(_("fecha de ingreso"), default=timezone.now)
    dni = models.CharField(
        _("documento de identidad"),
        max_length=50,
        blank=False,
        default="V00000000",
        validators=[
            RegexValidator(
                regex="^[V|E]+\d{6,9}$", message="Documento de identidad no válido",
            ),
        ],
    )
    is_paramedic = models.BooleanField(
        _("es paramédico"),
        default=False,
        help_text=_("Indica si este miembro de la tripulación es paramédico."),
    )
    is_driver = models.BooleanField(
        _("es conductor"),
        default=False,
        help_text=_("Indica si este miembro de la tripulación puede conducir."),
    )
    is_medic = models.BooleanField(
        _("es médico"),
        default=False,
        help_text=_("Indica si este miembro de la tripulación es médico."),
    )
    is_coordinator = models.BooleanField(
        _("es coordinador"),
        default=False,
        help_text=_("Indica si este miembro de la tripulación es coordinador."),
    )

    def __str__(self):
        return "{0}, {1}".format(self.last_name, self.first_name)

    class Meta:
        verbose_name = _("profesional médico")
        verbose_name_plural = _("profesionales médicos")


class Crew(models.Model):
    """
    **Tripulación**

    Una tripulación está conformada por una conjunto de profesionales médicos
    asignados a una móvil durante un período de tiempo (guardia).
    """

    name = models.CharField(_("nombre"), max_length=255, blank=True,)
    start = models.DateTimeField(
        _("inicio de la guardia"), default=default_btime, blank=True, null=True
    )
    end = models.DateTimeField(
        _("fin de la guardia"), default=default_btime, blank=True, null=True
    )
    base = models.ForeignKey(
        Base, on_delete=models.CASCADE, related_name="crews", verbose_name=_("base")
    )
    mobile_unit = models.ForeignKey(
        MobileUnit,
        on_delete=models.CASCADE,
        related_name="crews",
        verbose_name=_("unidad móvil"),
    )
    practitioners = models.ManyToManyField(
        Practitioner,
        through="PractitionerRole",
        related_name="crews",
        verbose_name=_("profesionales médicos"),
    )

    objects = CrewManager()

    # def save(self, *args, **kwargs):
    #     mobile_unit_name = self.mobile_unit.codename
    #     start_date = self.start.date().strftime("%d-%m-%Y")
    #     watchcode = ['A', 'B', 'C']
    #     self.name = f"{mobile_unit_name}-" + f"{start_date}-" + f"{watchcode[self.start.date().day % 3]}"
    #     super(Crew, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _("tripulación")
        verbose_name_plural = _("tripulaciones")

    def __str__(self):
        return self.name


class PractitionerRole(models.Model):
    """
    **Rol del Profesional Médico** (en la tripulación)
    """

    ROLE = (
        ("PARAMEDIC", _("Paramédico")),
        ("DRIVER", _("Conductor")),
        ("PRACTITIONER", _("Médico")),
        ("COORDINATOR", _("Coordinador")),
    )

    crew = models.ForeignKey(Crew, on_delete=models.CASCADE, blank=True, null=True)
    practitioner = models.ForeignKey(Practitioner, on_delete=models.CASCADE)

    practitioner_role = models.CharField(
        _("rol del tripulante"), max_length=15, choices=ROLE
    )

    class Meta:
        verbose_name = _("Rol del profesional médico")
        verbose_name_plural = _("Roles de los profesionales médicos")
