#from rest_framework import routers
from django.urls import path
from . import views

app_name = 'crews'

urlpatterns = [
    path(
        'crews',
        views.CrewListView.as_view(),
        name='crews_list'
    ),
    path(
        'crews/<pk>',
        views.CrewDetailView.as_view(),
        name='crew_detail'
    ),
    path(
        'crews-simple',
        views.CrewSimpleListView.as_view(),
        name='crews_simple_list'
    ),
    path(
        'crews-simple/<pk>',
        views.CrewSimpleDetailView.as_view(),
        name='crew_simple_detail'
    ),
    path(
        'practitioners',
        views.PractitionerListView.as_view(),
        name='practitioner_list'
    ),
    path(
        'practitioners/<pk>',
        views.PractitionerDetailView.as_view(),
        name='practitioner_detail'
    ),
    path(
        'bases',
        views.BaseListView.as_view(),
        name='base_list'
    ),
    path(
        'bases/<pk>',
        views.BaseDetailView.as_view(),
        name='base_detail'
    ),
    path(
        'mobile-units',
        views.MobileUnitListView.as_view(),
        name='mobile-unit_list'
    ),
    path(
        'mobile-units/<pk>',
        views.MobileUnitDetailView.as_view(),
        name='mobile-unit_detail'
    ),
    path(
        'practitioner-roles',
        views.PractitionerRoleListView.as_view(),
        name='practitioner-role-list'
    ),
    path(
        'crews-today',
        views.CrewCurrentDayListView.as_view(),
        name='crew-of-today'
    ),
    
]
