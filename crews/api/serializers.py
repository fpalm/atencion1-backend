from rest_framework import serializers

from ..models import MobileUnit, Base, Practitioner, Crew, PractitionerRole


class MobileUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = MobileUnit
        fields = (
            "id",
            "codename",
            "unit_type",
            "license_plate",
        )


class BaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Base
        fields = (
            "id",
            "base_name",
        )


class PractitionerSerializer(serializers.ModelSerializer):

    gender = serializers.CharField(source="get_gender_display")

    class Meta:
        model = Practitioner
        fields = (
            "id",
            "first_name",
            "last_name",
            "gender",
            "birthdate",
            "home_address",
            "phone_number",
            "date_joined",
            "dni",
            "is_paramedic",
            "is_driver",
            "is_medic",
            "is_coordinator",
        )


class PractitionerSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Practitioner
        fields = ("id",)


class PractitionerRoleSerializer(serializers.ModelSerializer):

    practitioner = PractitionerSerializer(read_only=True)

    class Meta:
        model = PractitionerRole
        fields = (
            "practitioner",
            "practitioner_role",
        )


class PractitionerRoleSimpleSerializer(serializers.ModelSerializer):

    practitioner = PractitionerSimpleSerializer(read_only=True)

    class Meta:
        model = PractitionerRole
        fields = (
            "practitioner",
            "practitioner_role",
        )


class CrewSerializer(serializers.ModelSerializer):

    base = BaseSerializer()
    mobile_unit = MobileUnitSerializer()
    practitioners = PractitionerRoleSerializer(source="practitionerrole_set", many=True)

    class Meta:
        model = Crew
        fields = "__all__"


class CrewSimpleSerializer(serializers.ModelSerializer):

    practitioners = PractitionerRoleSerializer(
        source="practitionerrole_set", many=True, read_only=True
    )

    class Meta:
        model = Crew
        fields = (
            "id",
            "name",
            "start",
            "end",
            "base",
            "mobile_unit",
            "practitioners",
        )

    def create(self, validated_data):
        crew = Crew.objects.create(**validated_data)
        if "practitioners" in self.initial_data:
            practitioners = self.initial_data.get("practitioners")
            for practitioner in practitioners:
                pract_id = practitioner["practitioner"].get("id")
                practitioner_role = practitioner.get("practitioner_role")
                practitioner_instance = Practitioner.objects.get(pk=pract_id)
                PractitionerRole(
                    crew=crew,
                    practitioner=practitioner_instance,
                    practitioner_role=practitioner_role,
                ).save()
        crew.save()
        return crew

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.start = validated_data.get("start", instance.start)
        instance.end = validated_data.get("end", instance.end)
        instance.base = validated_data.get("base", instance.base)
        instance.mobile_unit = validated_data.get("mobile_unit", instance.mobile_unit)
        if "practitioners" in self.initial_data:
            practitioners = self.initial_data.get("practitioners")
            instance.practitioners.clear()
            for practitioner in practitioners:
                pract_id = practitioner["practitioner"].get("id")
                practitioner_role = practitioner.get("practitioner_role")
                practitioner_instance = Practitioner.objects.get(pk=pract_id)
                PractitionerRole(
                    crew=instance,
                    practitioner=practitioner_instance,
                    practitioner_role=practitioner_role,
                ).save()
        instance.save()
        return instance


class CrewMinimalSerializer(serializers.ModelSerializer):
    base_name = serializers.SerializerMethodField()
    mobile_unit_code = serializers.SerializerMethodField()

    class Meta:
        model = Crew
        fields = (
            "name",
            "base_name",
            "mobile_unit_code",
        )

    def get_base_name(self, obj):
        if obj.base:
            return obj.base.base_name
        else:
            return None

    def get_mobile_unit_code(self, obj):
        if obj.mobile_unit:
            return obj.mobile_unit.codename
        else:
            return None
