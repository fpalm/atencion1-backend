from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from django.utils import timezone

from .serializers import (
    MobileUnitSerializer,
    BaseSerializer,
    PractitionerSerializer,
    CrewSerializer,
    CrewSimpleSerializer,
    PractitionerRoleSerializer,
)
from ..models import MobileUnit, Base, Practitioner, Crew, PractitionerRole

# from profiles.custom_permissions import (IsAnyAuthenticatedUser,
#                                          IsOperatorUser,
#                                          IsCoordinatorUser,
#                                          IsManagerUser
#                                          )


class CrewListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Crew.objects.all()
    serializer_class = CrewSerializer

    filter_fields = (
        "start",
        "end",
        "base",
        "mobile_unit",
        "practitioners",
    )
    search_fields = ("^practitioners",)
    ordering_fields = (
        "start",
        "end",
    )


class CrewDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Crew.objects.all()
    serializer_class = CrewSerializer


class CrewSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Crew.objects.all()
    serializer_class = CrewSimpleSerializer

    filter_fields = (
        "start",
        "end",
    )
    ordering_fields = (
        "start",
        "end",
    )


class CrewSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Crew.objects.all()
    serializer_class = CrewSimpleSerializer


class CrewCurrentDayListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    serializer_class = CrewSerializer

    def get_queryset(self):
        date = timezone.now()  # Revisar esto.
        return Crew.objects.current_day(date)


class PractitionerListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Practitioner.objects.all()
    serializer_class = PractitionerSerializer

    filter_fields = (
        "is_paramedic",
        "is_driver",
        "is_medic",
        "is_coordinator",
    )
    search_fields = (
        "^first_name",
        "^last_name",
        "^dni",
    )
    ordering_fields = (
        "first_name",
        "last_name",
        "dni",
    )


class PractitionerDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Practitioner.objects.all()
    serializer_class = PractitionerSerializer


class BaseListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Base.objects.all()
    serializer_class = BaseSerializer

    ordering_fields = ("base_name",)


class BaseDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Base.objects.all()
    serializer_class = BaseSerializer


class MobileUnitListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = MobileUnit.objects.all()
    serializer_class = MobileUnitSerializer

    filter_fields = (
        "codename",
        "unit_type",
    )
    search_fields = (
        "^licence_plate",
        "^codename",
    )


class MobileUnitDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = MobileUnit.objects.all()
    serializer_class = MobileUnitSerializer


class PractitionerRoleListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = PractitionerRole.objects.all()
    serializer_class = PractitionerRoleSerializer


class PractitionerRoleDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = PractitionerRole.objects.all()
    serializer_class = PractitionerRoleSerializer
