Estructura de archivos
======================

Otros directorios
-----------------

`docs`

Documentación básica del backend de Atención1 (en construcción)

`emergensys-history`

Datos y scripts de Python para la migración de datos históricos de Emergensys.

`media`

Imágenes del modelo de perfiles: fotos de los usuarios, firmas y sellos médicos.

`static`

Imágenes y archivos estáticos requeridos por algunas plantillas del proyecto.
