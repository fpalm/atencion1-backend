.. _documentos:

==========
Documentos
==========

.. figure:: ../_static/documents_app.png
   :alt: Modelos de la aplicación documents
   :align: center

   Modelos de la aplicación ``documents``

Aplicación Documentos
=====================

.. automodule:: documents


Modelos Documentos
------------------

.. autosummary::
   :recursive:

   documents.models.DocumentType
   documents.models.MedicalDocument
   documents.models.LabDocument
   documents.models.HHCLabDocument

.. automodule:: documents.models
   :members:


