.. _lugares:

=======
Lugares
=======

.. figure:: ../_static/locations_app.png
   :alt: Modelos de la aplicación locations
   :align: center

   Modelos de la aplicación ``locations``

Aplicación Ubicaciones
======================

.. automodule:: locations


Modelos Ubicaciones
-------------------

.. autosummary::
   :recursive:

   locations.models.Country
   locations.models.AdminFirstLevel
   locations.models.AdminSecondLevel
   locations.models.AdminThirdLevel
   locations.models.Place
   locations.models.Address
   locations.models.OrganizationType
   locations.models.Organization
   locations.models.OrganizationAddress
   locations.models.Location
   locations.models.LocationAddress
   locations.models.LocationPhoneNumber


.. automodule:: locations.models
   :members:
