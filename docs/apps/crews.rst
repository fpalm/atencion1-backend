.. _tripulaciones:

=============
Tripulaciones
=============

.. figure:: ../_static/crews_app.png
   :alt: Modelos de la aplicación crews
   :align: center

   Modelos de la aplicación ``crews``

Aplicación Tripulaciones
========================

.. automodule:: crews


Modelos Tripulaciones
---------------------

.. autosummary::
   :recursive:

   crews.models.Base
   crews.models.Crew
   crews.models.MobileUnit
   crews.models.Practitioner
   crews.models.PractitionerRole

.. automodule:: crews.models
   :members:
