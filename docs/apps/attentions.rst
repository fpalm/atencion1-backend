.. _atenciones:

==========
Atenciones
==========

.. figure:: ../_static/attentions_app.png
   :alt: Modelos de la aplicación attentions
   :align: center

   Modelos de la aplicación ``attentions``


Aplicación Atenciones
=====================

.. automodule:: attentions


Modelos Atenciones
------------------

.. autosummary::
   :recursive:

   attentions.models.Attention
   attentions.models.AttentionPhoneNumber

.. automodule:: attentions.models
   :members:
