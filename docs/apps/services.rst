.. _servicios:

=========
Servicios
=========

.. figure:: ../_static/services_app.png
   :alt: Modelos de la aplicación services
   :align: center

   Modelos de la aplicación ``services``

Aplicación Servicios
====================

.. automodule:: services


Modelos Servicios
-----------------

.. autosummary::
   :recursive:

   services.models.ServiceType
   services.models.ServiceSource
   services.models.ServiceAddress
   services.models.CancellationReason
   services.models.LabTest
   services.models.Service
   services.models.TMG
   services.models.ExternalService
   services.models.Transfer
   services.models.HHC
   services.models.LAB
   services.models.HHP
   services.models.DeliveryItem
   services.models.DeliveryNote


.. automodule:: services.models
   :members:

