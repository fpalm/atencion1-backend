.. _terminologias:

=============
Terminologías
=============

.. figure:: ../_static/terminologies_app.png
   :alt: Modelos de la aplicación terminologies
   :align: center

   Modelos de la aplicación ``terminologies``

Aplicación Terminologías
========================

.. automodule:: terminologies


Modelos Terminologías
---------------------

.. autosummary::
   :recursive:

   terminologies.models.ICD10Chapter
   terminologies.models.ICD10Block
   terminologies.models.ICD10Category
   terminologies.models.ICD10Symptom
   terminologies.models.ICD10Diagnose
   terminologies.models.Condition
   terminologies.models.ActiveIngredient
   terminologies.models.MedicationForm
   terminologies.models.IngredientInProduct
   terminologies.models.DeliveryUnit
   terminologies.models.MedicinalProduct
   terminologies.models.PrescriptionItem
   terminologies.models.MedicalPrescription
   terminologies.models.TriageQuestion
   terminologies.models.OptionalTriageQuestion
   terminologies.models.CallingQuestionnaire
   terminologies.models.MedicalPrescription
   terminologies.models.TriageQuestionAnswer
   terminologies.models.OptionalTriageQuestionAnswer
   terminologies.models.QuestionnaireMessage


.. automodule:: terminologies.models
   :members:
