.. _afiliados:

=========
Afiliados
=========

.. figure:: ../_static/affiliates_app.png
   :alt: Modelos de la aplicación affiliates
   :align: center

   Modelos de la aplicación ``affiliates``

Aplicación Afiliados
====================

.. automodule:: affiliates


Modelos Afiliados
-----------------

.. autosummary::
   :recursive:

   affiliates.models.Affiliate
   affiliates.models.AffiliateAddress
   affiliates.models.AffiliateEmail
   affiliates.models.AffiliateKinship
   affiliates.models.AffiliatePhoneNumber

.. automodule:: affiliates.models
   :members:







