.. _polizas:

=========
Pólizas
=========

.. figure:: ../_static/policies_app.png
   :alt: Modelos de la aplicación policies
   :align: center

   Modelos de la aplicación ``policies``

Aplicación Pólizas
==================

.. automodule:: policies


Modelos Pólizas
---------------

.. autosummary::
   :recursive:

   policies.models.Plan
   policies.models.Policy
   policies.models.ValidatorPlanMapping

.. automodule:: policies.models
   :members:
