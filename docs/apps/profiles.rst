.. _perfiles:

========
Perfiles
========

.. figure:: ../_static/profiles_app.png
   :alt: Modelos de la aplicación profiles
   :align: center

   Modelos de la aplicación ``profiles``

Aplicación Perfiles
===================

.. automodule:: profiles


Modelos Perfiles
----------------

.. autosummary::
   :recursive:

   profiles.models.Profile

.. automodule:: profiles.models
   :members:
