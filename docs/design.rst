.. _diseno:

====================
Diseño de Atención-1
====================

Atención-1, por "Atención Primaria", es una aplicación diseñada para gestionar
las solicitudes de atención a una central de operaciones de servicios de salud.
Desde el punto de vista técnico está implementada como aplicación desacoplada
que está constituida por un backend desarrollado en Django y un frontend
desarrollado en Vue.js que se comunican a través de un API Restful.

Los **usuarios** del sistema son básicamente integrantes del equipo de
operaciones y de otras unidades de gestión relacionadas. Estos usuarios
pertenecen a un rol, a saber: operador, médico, despachador, coordinador,
asistente y gerente. Para una explicación detallada de estos roles y el uso del
sistema, por favor lea el "Manual de Usuarios Atención-1".

Las personas que reciben los **servicios** son **afiliados** de un seguro o
particulares. Los afiliados a los seguros pueden recibir los servicios
especificados en un **plan**, bajo el cual contratan una **póliza**. Se considera
que los afiliados que reciben servicios como particulares tienen una póliza
"autofinanciada".

Cada nueva solicitud de servicio inicia una **atención** bajo la cual se agrupan
un conjunto de servicios sucesivos. Los servicios son atendidos por **médicos**
y por **tripulaciones**. Las tripulaciones están constituidas por un conjunto de
profesionales de la salud a los que se les asigna un vehículo (unidad móvil)
para prestar algunos de los servicios durante un período de tiempo determinado.

Buena parte de los servicios requiere el traslado de una tripulación de médicos
y/o paramédicos a la dirección actual del afiliado, y dependiendo de las
características del caso se atenderá en el lugar o será trasladado a un centro
de salud.

En algunos de los servicios se registran síntomas y se generan diagnósticos, con
sus respectivos récipes e indicaciones, que se envían automáticamente por correo
electrónico una vez finaliza el servicios. Según se considere necesario se
adjuntan documentos a las atenciones para validar servicios como la entrega de
medicamentos y los planes de hospitalización domiciliaria.

Se exhorta nuevamente a revisar el "Manual del Usuario Atención-1" para obtener
una descripción detallada del funcionamiento del sistema.

Uso de Estándares
=================

Atención-1 desde un principio fue diseñada y pensada para cumplir con estándares
internacionales que le permitan adaptarse a distintos entornos de despliegue, a
continuación mencionamos los aspectos más resaltantes.

Localización de términos
------------------------

Los términos utilizados en la interfaz gráfica tanto en frontend (Vue) y backend
(Django) cuentan con una capa de internacionalización i18n y localización l10n
que permita utilizar distintos idiomas y  variantes de idioma. En la actualidad,
el sistema puede utilizarse tanto en inglés como en español, y también podrían
generarse localizaciones de la interfaz de ser necesario utilizar variantes
distintas del español para cada uno de los países donde se implemente.

Divisiones Administrativas y direcciones
----------------------------------------

La estructura de las divisiones administrativas está organizada en niveles y
para todos los países del planeta, respetando los términos que se utilizan en
cada uno utilizando el estándar ISO 3166-1 para los nombres de los países, e ISO
3166-2 para sus subdivisiones. 

Números Telefónicos
-------------------

Los números telefónicos en el sistema utilizan la biblioteca phonenumbers de
python que ofrece utilidades para manejar los números telefónicos del planeta
entero en formato internacional o local según sea necesario, facilitando
validaciones de los códigos autorizados.

Medicamentos y Equipos Médicos
------------------------------

Si bien los principios activos son universales, las presentaciones comerciales
de los medicamentos varían de país en país. Es necesario elaborar una lista de
los medicamentos esenciales en sus presentaciones comerciales para cada país
como base, para luego actualizarlo según los inventarios que se manejen.

Fechas y Horas
--------------

En Atención-1 las fechas y horas se registran utilizando el formato ISO 8601 con
base en la zona horaria UTC, de esta manera se pueden procesar sin
complicaciones servicios que ocurran simultáneamente en distintas zonas
horarias, y esto también es clave para integrar la información de los distintos
nodos.

Servicios
---------

Este probablemente sea el punto que haya que revisar con mayor cuidado ya que en
todos los nodos no solamente se van a realizar los mismos servicios y de la
misma manera. Es importante señalar que aparte de tener total control para la
personalización del sistema se deben diseñar mecanismos para personalizar los
flujos de trabajo.

Terminología Médica
-------------------

El sistema utiliza terminología basada en la clasificación internacional de
enfermedades CIE-10, si bien los valores posibles aparecen en la interfaz
gráfica en términos simplificados, filtrando las enfermedades más comunes, lo
que se almacenan en la base de datos son sus códigos. Hace falta considerar que
la morbilidad más frecuente cambia de país en país y deben existir mecanismos
para ajustarlo de forma periódica.

Interfaz de Programación de Aplicaciones
----------------------------------------

El desarrollo de  Atención-1 es completamente desacoplado, el backend está
desarrollado en Django y este ofrece una interfaz API-Restful que interactúa con
el frontend desarrollado en VueJS. Por esta razón, existen endpoints para
realizar cualquier tipo interacción de integración de los sistemas. Además, ya
que todas las instancias se encuentran en AWS, es posible escribir scripts que
de forma regular realicen operaciones de actualización y/o mantenimiento de las
distintas bases de datos.
