Introducción
============

El presente documento contiene una versión esquemática de la estructura de
directorios y puntos finales (endpoints) de la versión 0.7 en el backend de
Atención-1.

De acuerdo a la terminología de Django, al sistema completo se le denomina
*Proyecto* y a cada uno de los módulos funcionales se les denomina
*Aplicaciones*. Este documento consiste en una revisión de cada de las
aplicaciones de Atención-1 haciendo énfasis en los modelos de datos y sus
relaciones.


El conjunto de endpoints del API pueden consultarse en cualquier despliegue de
Atención-1 por medio de Swagger en ``{{dominio}}/swagger-docs/``

De igual manera, los modelos pueden consultarse a través de la interfaz
administrativa de la aplicación en ``{{dominio}}/admin/doc/models/``

