.. Atención-1 Backend documentation master file, created by
   sphinx-quickstart on Wed Aug  5 12:17:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Atención-1 Backend
==================

.. toctree::
   :maxdepth: 2
   :caption: Contenidos:

   intro.rst
   design.rst
   structure.rst
   install.rst
   conf.rst
   deployment.rst
   file_structure.rst
   apps/affiliates.rst
   apps/attentions.rst
   apps/crews.rst
   apps/documents.rst
   apps/locations.rst
   apps/policies.rst
   apps/profiles.rst
   apps/services.rst
   apps/terminologies.rst






Índices y tablas
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
