"""
Script de migración del historial de atenciones desde Emergensys

@author: lurdaneta y fpalm

"""

import os
import re
import subprocess
from datetime import timedelta
from glob import glob
from pathlib import Path

import numpy as np
import pandas as pd

BASE_PATH = Path(os.path.dirname(os.path.realpath(__file__)))
DATA_DIR = os.path.join(BASE_PATH.parent, "emergensys-history/source_data")
# ATENCION1_DIR = "/home/fpalm/projects/grupov/atencion1-backend"
WORKON_HOME = "/home/lurdaneta/PersonalProjects/qu4nt"
ATENCION1_ENV = os.path.join(WORKON_HOME, "atencion1-backend")

sino2bool = {"SI": True, "NO": False}
rename_gender = {"M": "MALE", "F": "FEMALE"}

report_columns = [
    "CLIENTE",
    "TIPO_SERVICIO",
    "SERVICIO",
    "ANUL",
    "ABOR",
    "IMPUTABLE",
    "FECHA",
    "DIASEM",
    "NUMERO",
    "COD",
    "NOMBRE_PACIENTE",
    "CEDULA",
    "EDAD",
    "SEXO",
    "TELEFONOS",
    "ORIGEN",
    "DESTINO",
    "MEDICO",
    "PARAMEDICO",
    "CONDUCTOR",
    "SALE_BASE",
    "PROGM",
    "DIA_NCH",
    "LLAM",
    "DESP",
    "SAL",
    "LLEG",
    "FINAL",
    "LLEG_BAS",
    "TMP_DESP",
    "TMP_SAL",
    "TMP_DESZ",
    "TMP_ATN",
    "TMP_RESP",
    "TMP_LLEG.BAS",
    "DIAGNOSTICO",
    "CARNET",
    "F_U",
    "CAUSAL_ANUL_ABOR",
    "OBSERVACIONES",
    "MUNICIPIO",
    "CAPTL",
    "PROV",
    "NOMBRE_PROV",
    "MONTO_SERV",
]

report_converters = {
    "CLIENTE": str,
    "TIPO SERVICIO": str,
    "SERVICIO": str,
    "ANUL": str,
    "ABOR": str,
    "IMPUTABLE": str,
    "FECHA": pd.to_datetime,
    "DIASEM": int,
    "NUMERO": int,
    "COD": str,
    "NOMBRE PACIENTE": str,
    "CEDULA": str,
    "EDAD": int,
    "SEXO": str,
    "TELEFONOS": str,
    "ORIGEN": str,
    "DESTINO": str,
    "MEDICO": str,
    "PARAMEDICO": str,
    "CONDUCTOR": str,
    "SALE BASE": str,
    "PROGM": str,
    "DIA/NCH": str,
    "LLAM": str,
    "DESP": str,
    "SAL": str,
    "LLEG": str,
    "FINAL": str,
    "LLEG BAS": str,
    "TMP DESP": str,
    "TMP SAL": str,
    "TMP DESZ": str,
    "TMP ATN": str,
    "TMP RESP": str,
    "TMP LLEG.BAS": str,
    "DIAGNOSTICO": str,
    "CARNET": str,
    "F/U": str,
    "CAUSAL ANUL/ABOR": str,
    "OBSERVACIONES": str,
    "MUNICIPIO": str,
    "CAPTL": str,
    "PROV": str,
    "NOMBRE PROV": str,
    "MONTO SERV": float,
}

attention_rename = {
    "tLLAM": "created",
    "tLLEG_BAS": "last_modified",
    "OBSERVACIONES": "observations",
    "tFINAL": "end",
}

# TODO revisar id relacionados con las pólizas

plan = 46
sponsor = 1306
user = 72
organization = 1305
attendant = 73

FIELDS_ATTENTION = [
    "user",
    "affiliate",
    "reason_for_care",
    "created",
    "last_modified",
    "status",
    "operation_state",
    "observations",
    "follow_up",
    "closing_reason",
    "end",
]

FIELDS_POLICY = [
    "plan",
    "sponsor",
    "code",
    "authorized_by",
    "policy_type",
]

FIELDS_AFFILIATE = [
    "policies",
    "first_name",
    "last_name",
    "gender",
    "birthdate",
    "is_active",
    "date_joined",
    "dni",
    "from_validator",
    "registered_by",
]

trn_rename = {
    "bIMPUTABLE": "imputable",
    "tLLAM": "start",
    "tFINAL": "end",
    "bPROGM": "scheduled",
    "tDESP": "dispatch_datetime",
    "tSAL": "base_departure_datetime",
    "tLLEG": "destination_arrival_datetime",
    "tLLEG_BAS": "base_arrival_datetime",
    "OBSERVACIONES": "comments",
    "ATENCION": "attention",
}

lab_rename = {
    "bIMPUTABLE": "imputable",
    "tLLAM": "start",
    "tFINAL": "end",
    "bPROGM": "scheduled",
    "tDESP": "dispatch_datetime",
    "tSAL": "base_departure_datetime",
    "tLLEG": "destination_arrival_datetime",
    "tLLEG_BAS": "base_arrival_datetime",
    "OBSERVACIONES": "comments",
    "ATENCION": "attention",
}

FIELDS_TRN = [
    "user",
    "attendant",
    "attention",
    "policy",
    "sucessive_requester",
    "prior_service",
    "prior_service_type",
    "start",
    "end",
    "status",
    "cancellation_reason",
    "operation_state",
    "scheduled",
    "scheduled_date_time",
    "imputable",
    "symptoms",
    "other_symptoms",
    "diagnoses",
    "other_diagnoses",
    "closing_reason",
    "comments",
    "send_email",
    "destination_address",
    "crew",
    "dispatch_datetime",
    "base_departure_datetime",
    "destination_arrival_datetime",
    "base_arrival_datetime",
    "transfer_reason",
    "origin_address",
    "origin_arrival_datetime",
    "origin_departure_datetime",
    "origin_second_arrival_datetime",
    "origin_second_departure_datetime",
    "emergency",
    "simple_trip",
    "interinstitutional",
    "interstate",
    "icu",
]


FIELDS_LAB = [
    "user",
    "attendant",
    "attention",
    "policy",
    "sucessive_requester",
    "prior_service",
    "prior_service_type",
    "start",
    "end",
    "status",
    "cancellation_reason",
    "operation_state",
    "scheduled",
    "scheduled_date_time",
    "imputable",
    "symptoms",
    "other_symptoms",
    "diagnoses",
    "other_diagnoses",
    "closing_reason",
    "comments",
    "send_email",
    "destination_address",
    "crew",
    "dispatch_datetime",
    "base_departure_datetime",
    "destination_arrival_datetime",
    "destination_departure_datetime",
    "base_arrival_datetime",
    "transfer_reason",
    "lab_tests",
]

hmd_rename = {
    "bIMPUTABLE": "imputable",
    "tLLAM": "start",
    "tFINAL": "end",
    "bPROGM": "scheduled",
    "tDESP": "dispatch_datetime",
    "tSAL": "base_departure_datetime",
    "tLLEG": "destination_arrival_datetime",
    "tLLEG_BAS": "base_arrival_datetime",
    "OBSERVACIONES": "comments",
    "ATENCION": "attention",
}

FIELDS_HMD = [
    "user",
    "attendant",
    "attention",
    "policy",
    "sucessive_requester",
    "prior_service",
    "prior_service_type",
    "start",
    "end",
    "status",
    "cancellation_reason",
    "operation_state",
    "scheduled",
    "scheduled_date_time",
    "imputable",
    "symptoms",
    "other_symptoms",
    "diagnoses",
    "other_diagnoses",
    "closing_reason",
    "comments",
    "send_email",
    "destination_address",
    "crew",
    "dispatch_datetime",
    "base_departure_datetime",
    "destination_arrival_datetime",
    "destination_departure_datetime",
    "base_arrival_datetime",
    "transfer_reason",
    "delivery_note",
    "critical_inventory",
    "acute_inventory",
    "medical_equipment_delivery",
    "lab_examination",
    "source",
]

hhp_rename = {
    "bIMPUTABLE": "imputable",
    "tLLAM": "start",
    "tFINAL": "end",
    "bPROGM": "scheduled",
    "tDESP": "dispatch_datetime",
    "tSAL": "base_departure_datetime",
    "tLLEG": "destination_arrival_datetime",
    "tLLEG_BAS": "base_arrival_datetime",
    "OBSERVACIONES": "comments",
    "ATENCION": "attention",
}


FIELDS_HHP = [
    "user",
    "attendant",
    "attention",
    "policy",
    "sucessive_requester",
    "prior_service",
    "prior_service_type",
    "start",
    "end",
    "status",
    "cancellation_reason",
    "operation_state",
    "scheduled",
    "scheduled_date_time",
    "imputable",
    "symptoms",
    "other_symptoms",
    "diagnoses",
    "other_diagnoses",
    "closing_reason",
    "comments",
    "send_email",
    "destination_address",
    "crew",
    "dispatch_datetime",
    "base_departure_datetime",
    "destination_arrival_datetime",
    "destination_departure_datetime",
    "base_arrival_datetime",
    "transfer_reason",
    "source",
]


hhc_rename = {
    "bIMPUTABLE": "imputable",
    "tLLAM": "start",
    "tFINAL": "end",
    "bPROGM": "scheduled",
    "tDESP": "dispatch_datetime",
    "tSAL": "base_departure_datetime",
    "tLLEG": "destination_arrival_datetime",
    "tLLEG_BAS": "base_arrival_datetime",
    "OBSERVACIONES": "comments",
    "ATENCION": "attention",
}


FIELDS_HHC = [
    "user",
    "attendant",
    "attention",
    "policy",
    "sucessive_requester",
    "prior_service",
    "prior_service_type",
    "start",
    "end",
    "status",
    "cancellation_reason",
    "operation_state",
    "scheduled",
    "scheduled_date_time",
    "imputable",
    "symptoms",
    "other_symptoms",
    "diagnoses",
    "other_diagnoses",
    "closing_reason",
    "comments",
    "send_email",
    "destination_address",
    "crew",
    "dispatch_datetime",
    "base_departure_datetime",
    "destination_arrival_datetime",
    "destination_departure_datetime",
    "base_arrival_datetime",
    "transfer_reason",
    "request_medicine",
    "request_external_service",
    "request_medical_specialist",
    "request_advice",
    "request_lab_exam",
    "request_medical_indication",
    "lab_examination",
    "lab_tests",
]

tmg_rename = {
    "bIMPUTABLE": "imputable",
    "tLLAM": "start",
    "tFINAL": "end",
    "bPROGM": "scheduled",
    "tDESP": "dispatch_datetime",
    "tSAL": "base_departure_datetime",
    "tLLEG": "destination_arrival_datetime",
    "tLLEG_BAS": "base_arrival_datetime",
    "OBSERVACIONES": "comments",
    "ATENCION": "attention",
}

FIELDS_TMG = [
    "user",
    "attendant",
    "attention",
    "policy",
    "sucessive_requester",
    "prior_service",
    "prior_service_type",
    "start",
    "end",
    "status",
    "cancellation_reason",
    "operation_state",
    "scheduled",
    "scheduled_date_time",
    "imputable",
    "symptoms",
    "other_symptoms",
    "diagnoses",
    "other_diagnoses",
    "closing_reason",
    "comments",
    "send_email",
    "send_email",
    "call_start",
    "call_end",
    "request_medicine",
    "request_external_service",
    "request_medical_specialist",
    "request_advice",
    "request_lab_exam",
    "request_medical_indication",
]

# TODO: Modificar esto para obtener los archivos del directorio "data"

excel_files = glob(DATA_DIR + "/" + "*.xlsx")
excel_files.sort()
print(f"Los archivos de origen son: {excel_files}")


def get_reports():
    reports = glob("*.xlsx")
    reports.sort()
    return reports


def convert2datetime(tiempos):
    """Convert time columns to datetime format.

    Args:
        tiempos (pd.DataFrame): Report columns with time recording in str format.

    Returns:
        pd.DataFrame: Time columns in datetime format.
    """
    for tiempo in tiempos[:-1]:
        tiempos[tiempo] = pd.to_datetime(tiempos[tiempo], format="%H:%M:%S")
    for i in tiempos.index:
        f = tiempos.at[i, "FECHA"]  # fecha de la atencion
        tl = tiempos.at[i, "LLAM"]  # tiempo de la llamada
        for j in tiempos[:-1]:
            tm = tiempos.at[i, j]
            delta = timedelta(days=1) if tm < tl else timedelta(days=0)
            fa = f + delta  # fecha ajustada
            tiempos.at[i, j] = tm.replace(day=fa.day, month=fa.month, year=fa.year)
    return tiempos[["LLAM", "DESP", "SAL", "LLEG", "FINAL", "LLEG_BAS"]]


def run_import_export(command, filename, resource_class):
    print(f"Procesando {filename}...")
    return subprocess.run(
        [
            # os.path.join(ATENCION1_ENV, ""),
            "python",
            os.path.join(ATENCION1_ENV, "manage.py"),
            f"{command}",
            f"{os.path.join(BASE_PATH, filename)}",
            "--resource-class",
            f"{resource_class}",
        ],
        # shell=True,
        check=True,
    )


def load_and_get_ids(resource, resource_class):
    run_import_export("import_file", f"{resource}_input.csv", resource_class)
    run_import_export("export_file", f"{resource}_export.csv", resource_class)
    return pd.read_csv(f"{resource}_export.csv")


def read_reports(reports_filename):
    if os.path.isfile(reports_filename):
        reports = pd.read_pickle(reports_filename)
    else:
        reports = pd.DataFrame(columns=report_columns)
        for filename in excel_files:
            df = pd.read_excel(filename, skiprows=3, converters=report_converters)
            df.columns = report_columns
            reports = reports.append(df, ignore_index=True)

        reports.TIPO_SERVICIO = reports.TIPO_SERVICIO.astype("category")
        reports.SERVICIO = reports.SERVICIO.astype("category")
        reports.TIPO_SERVICIO = reports.TIPO_SERVICIO.astype("category")
        reports.ANUL = reports.ANUL.astype("category")
        reports.ABOR = reports.ABOR.astype("category")
        reports.IMPUTABLE = reports.IMPUTABLE.astype("category")
        reports.ORIGEN = reports.ORIGEN.astype("category")
        reports.DESTINO = reports.DESTINO.astype("category")
        reports.SALE_BASE = reports.SALE_BASE.astype("category")
        reports.PROGM = reports.PROGM.astype("category")
        reports.DIA_NCH = reports.DIA_NCH.astype("category")
        reports["bANUL"] = reports.ANUL.map(sino2bool)
        reports["bABOR"] = reports.ABOR.map(sino2bool)
        reports["bIMPUTABLE"] = reports.IMPUTABLE.map(sino2bool)
        reports["bSALE_BASE"] = reports.SALE_BASE.map(sino2bool)
        reports["bPROGM"] = reports.PROGM.map(sino2bool)
        tiempos = reports[["LLAM", "DESP", "SAL", "LLEG", "FINAL", "LLEG_BAS", "FECHA"]]
        ttiempos = convert2datetime(tiempos)
        tcolumns = ["tLLAM", "tDESP", "tSAL", "tLLEG", "tFINAL", "tLLEG_BAS"]
        ttiempos.columns = tcolumns
        reports = reports.assign(**ttiempos)
        reports.DIAGNOSTICO = reports.DIAGNOSTICO.astype("category")
        reports.MUNICIPIO = reports.MUNICIPIO.astype("category")
        reports["bCAPTL"] = reports.CAPTL.map(sino2bool)
        reports["bPROV"] = reports.PROV.map(sino2bool)
        reports.NOMBRE_PROV = reports.NOMBRE_PROV.astype("category")
        # Serializar el objeto para posteriores consultas
        reports.to_pickle(reports_filename)
    return reports


def group_attentions(sorted_reports_filename):
    """Get attentions as sequence of services per patient within timeframe.

    Args:
        sorted_reports_filename (pd.DataFrame): Sorted reports by time and patient.

    Returns:
        pd.DataFrame: Attentions
    """

    if os.path.isfile(sorted_reports_filename):
        sorted_reports = pd.read_pickle(sorted_reports_filename)
    else:
        sorted_reports = reports.sort_values(["NOMBRE_PACIENTE", "tLLAM"])
        sorted_reports["tLLAM_diff"] = sorted_reports.tLLAM.diff()
        sorted_reports["ATENCION"] = np.NAN
        counter = 0
        prev = ""
        for index, row in sorted_reports.iterrows():
            if row["NOMBRE_PACIENTE"] == prev and row["tLLAM_diff"] < np.timedelta64(
                12, "h"
            ):
                counter = counter
            else:
                counter += 1
            prev = row["NOMBRE_PACIENTE"]
            sorted_reports.loc[index, "ATENCION"] = counter
        sorted_reports["ATENCION_ORIGINAL"] = sorted_reports["ATENCION"]
        sorted_reports["ATENCION"] = sorted_reports["ATENCION"].astype(int)
        sorted_reports.to_pickle(sorted_reports_filename)
    return sorted_reports


def get_attentions(grouped_reports_filename):
    if os.path.isfile("attention_df.pkl"):
        attention_df = pd.read_pickle("attention_df.pkl")
    else:
        attention_df = grouped_reports_filename.groupby("ATENCION").first()
        attention_df.to_pickle("attention_df.pkl")
    attentions = attention_df.rename(columns=attention_rename, inplace=False).copy()
    attentions.index.names = ["id"]  # ??
    attentions.columns.names = ["id"]  # ??
    return attentions


def build_attentions(attentions):
    attentions["user"] = "MigrationUser"
    attentions["affiliate"] = (
        "V"
        + attentions["CEDULA"].str.extract(r"^([A-Z]*-?)?(\d*)( #\d*)?", expand=False)[
            1
        ]
    )
    attentions.loc[
        attentions["DIAGNOSTICO"] != "SERVICIO ABORTADO", "reason_for_care"
    ] = attentions["DIAGNOSTICO"]
    attentions["status"] = "CLOSED"
    attentions["operation_state"] = "AWAKE"
    attentions["follow_up"] = False
    attentions["closing_reason"] = attentions["ATENCION_ORIGINAL"]
    # Drop affiliates without dni
    attentions.dropna(subset=["affiliate"], inplace=True)
    attentions = attentions[FIELDS_ATTENTION].copy()
    return attentions


def build_policies(attentions):
    policies = attentions.copy()
    policies["code"] = "MigrationPolicy-" + policies["affiliate"]
    policies["plan"] = plan
    policies["sponsor"] = sponsor
    policies["authorized_by"] = "MigrationUser"
    policies["policy_type"] = "INDIVIDUAL"
    policies = policies[FIELDS_POLICY].copy()
    policies = policies.drop_duplicates()
    return policies


def build_affiliates(attentions):
    affiliates = attentions[["NOMBRE_PACIENTE", "SEXO", "CEDULA"]].copy()
    affiliates["CEDULA"] = affiliates["CEDULA"].str.strip()
    affiliates["dni"] = (
        "V"
        + affiliates["CEDULA"].str.extract(r"^([A-Z]*-?)?(\d*)( #\d*)?", expand=False)[
            1
        ]
    )
    affiliates["dni"].replace("", np.nan, inplace=True)
    affiliates.dropna(subset=["dni"], inplace=True)
    affiliates = affiliates.drop_duplicates("dni")
    affiliates["policies"] = "MigrationPolicy-" + affiliates["dni"]
    affiliates["policies"] = affiliates["policies"].map(code2id)
    affiliates["first_name"] = affiliates["NOMBRE_PACIENTE"].str.strip()
    affiliates["last_name"] = "MigrationLastName"
    affiliates["gender"] = affiliates["SEXO"].map(rename_gender)
    affiliates["birthdate"] = "1980-01-01"
    affiliates["is_active"] = 1
    affiliates["date_joined"] = ""
    affiliates["from_validator"] = 1
    affiliates["registered_by"] = user
    affiliates = affiliates[FIELDS_AFFILIATE]
    affiliates["policies"] = pd.to_numeric(affiliates["policies"]).astype("Int64")
    return affiliates


def build_attentions_affiliates(affiliates_id):
    dni2id = affiliates_id.set_index("dni").to_dict()["id"]
    attentions_affiliates = attentions.copy()
    attentions_affiliates["affiliate"] = attentions_affiliates["affiliate"].map(dni2id)
    attentions_affiliates["affiliate"].replace("", np.nan, inplace=True)
    attentions_affiliates.dropna(subset=["affiliate"], inplace=True)
    attentions_affiliates["user"] = user
    attentions_affiliates["closing_reason"] = pd.to_numeric(
        attentions_affiliates["closing_reason"]
    ).astype("Int64")
    return attentions_affiliates


def get_TLD_records(service_type, rep_sort):
    trns = list()
    for i in service_type:
        match = re.search("^TRASLADO", i)
        if match:
            trns.append(i)
    return rep_sort.loc[rep_sort["TIPO_SERVICIO"].isin(set(trns))]


def build_transfers(transfer_df):
    transfers = transfer_df.rename(columns=trn_rename, inplace=False)
    transfers.index.names = ["id"]
    transfers.columns.names = ["id"]
    transfers["user"] = user
    transfers["attendant"] = attendant
    transfers[["sucessive_requester", "prior_service"]] = np.nan
    transfers[["prior_service_type"]] = None
    transfers[["operation_state"]] = "ASLEEP"
    transfers[["symptoms"]] = ""
    transfers[["other_symptoms"]] = ""
    transfers[["diagnoses"]] = ""
    transfers[["other_diagnoses"]] = ""
    transfers[["closing_reason"]] = "Completado"
    transfers[["send_email"]] = False
    transfers[["destination_address"]] = ""
    transfers[["crew"]] = ""
    transfers[["origin_address"]] = ""
    transfers[["origin_second_arrival_datetime"]] = np.nan
    transfers[["origin_second_departure_datetime"]] = np.nan
    transfers[["emergency"]] = False
    transfers[["simple_trip"]] = True
    transfers[["interinstitutional"]] = False
    transfers[["interstate"]] = False
    transfers[["icu"]] = False
    transfers["destination_departure_datetime"] = transfers["end"]
    # transfers['cancellation_reason'] = transfers['CAUSAL_ANUL_ABOR'].str.title()
    transfers["cancellation_reason"] = ""
    transfers.loc[transfers["bANUL"] == True, "status"] = "CANCELLED"
    transfers.loc[transfers["bABOR"] == True, "status"] = "CANCELLED"
    transfers.loc[transfers["status"] != "CANCELLED", "status"] = "CLOSED"
    # transfers.loc[transfers['MEDICO'] != '', 'attendant'] = transfers['MEDICO']
    transfers.loc[transfers["attendant"].isnull(), "attendant"] = attendant
    transfers["policy"] = (
        "MigrationPolicy-V"
        + transfers["CEDULA"].str.extract(r"^([A-Z]*-?)?(\d*)( #\d*)?", expand=False)[
            1
        ]
    )
    transfers.loc[transfers["scheduled"] == True, "scheduled_date_time"] = transfers[
        "start"
    ]
    transfers.loc[
        transfers["DIAGNOSTICO"] != "SERVICIO ABORTADO", "transfer_reason"
    ] = transfers["DIAGNOSTICO"]
    transfers["origin_arrival_datetime"] = (
        (
            transfers["destination_arrival_datetime"]
            - transfers["base_departure_datetime"]
        )
        / 2
    ) + transfers["base_departure_datetime"]
    transfers["origin_departure_datetime"] = transfers["origin_arrival_datetime"]
    transfers = transfers[FIELDS_TRN]
    transfers["attention"] = transfers["attention"].map(original_attention2id)
    transfers["attention"] = pd.to_numeric(transfers["attention"]).astype("Int64")
    # TODO Incorporar póliza
    # TODO Incorporar IDs de síntomas y diagnósticos
    transfers["policy"] = transfers["policy"].map(code2id)
    transfers["policy"] = pd.to_numeric(transfers["policy"]).astype("Int64")
    # Eliminar servicios sin atención definida
    transfers["attention"].replace("", np.nan, inplace=True)
    transfers.dropna(subset=["attention"], inplace=True)
    return transfers


def get_LAB_records(service_type, rep_sort):
    labs = list()
    for i in service_type:
        match = re.search("^LABORATORIO", i)
        if match:
            labs.append(i)
    return rep_sort.loc[rep_sort["TIPO_SERVICIO"].isin(set(labs))]


def build_LAB(lab_df):
    labs = lab_df.rename(columns=lab_rename, inplace=False)
    labs.index.names = ["id"]
    labs.columns.names = ["id"]
    labs["user"] = user
    labs["attendant"] = attendant
    labs[["sucessive_requester", "prior_service"]] = np.nan
    labs[["prior_service_type"]] = None
    labs[["operation_state"]] = "ASLEEP"
    labs[["symptoms"]] = ""
    labs[["other_symptoms"]] = ""
    labs[["diagnoses"]] = ""
    labs[["other_diagnoses"]] = ""
    labs[["closing_reason"]] = "Completado"
    labs[["send_email"]] = False
    labs[["destination_address"]] = ""
    labs[["crew"]] = ""
    labs[["emergency"]] = False
    labs[["lab_tests"]] = ""
    labs["destination_departure_datetime"] = labs["end"]
    labs["cancellation_reason"] = labs["CAUSAL_ANUL_ABOR"].str.title()
    labs["cancellation_reason"] = ""
    labs.loc[labs["bANUL"] == True, "status"] = "CANCELLED"
    labs.loc[labs["bABOR"] == True, "status"] = "CANCELLED"
    labs.loc[labs["status"] != "CANCELLED", "status"] = "CLOSED"
    # labs.loc[labs['MEDICO'] != '', 'attendant'] = labs['MEDICO']
    labs.loc[labs["attendant"].isnull(), "attendant"] = "MigrationAttendant"
    labs["policy"] = (
        "MigrationPolicy-V"
        + labs["CEDULA"].str.extract(r"^([A-Z]*-?)?(\d*)( #\d*)?", expand=False)[
            1
        ]
    )
    labs.loc[labs["scheduled"] == True, "scheduled_date_time"] = labs["start"]
    labs.loc[labs["DIAGNOSTICO"] != "SERVICIO ABORTADO", "transfer_reason"] = labs[
        "DIAGNOSTICO"
    ]
    labs["origin_arrival_datetime"] = (
        (labs["destination_arrival_datetime"] - labs["base_departure_datetime"]) / 2
    ) + labs["base_departure_datetime"]
    labs["origin_departure_datetime"] = labs["origin_arrival_datetime"]
    labs = labs[FIELDS_LAB]
    labs["attention"] = labs["attention"].map(original_attention2id)
    labs["attention"] = pd.to_numeric(labs["attention"]).astype("Int64")
    labs["policy"] = labs["policy"].map(code2id)
    labs["policy"] = pd.to_numeric(labs["policy"]).astype("Int64")
    # Eliminar servicios sin atención definida
    labs["attention"].replace("", np.nan, inplace=True)
    labs.dropna(subset=["attention"], inplace=True)
    return labs


def get_HMD_records(service_type, rep_sort):
    hmds = list()
    for i in service_type:
        match = re.search("^ENTREGA", i)
        if match:
            hmds.append(i)
    return rep_sort.loc[rep_sort["TIPO_SERVICIO"].isin(set(hmds))]


def build_HMD(hmd_df):
    hmds = hmd_df.rename(columns=hmd_rename, inplace=False)
    hmds.index.names = ["id"]
    hmds.columns.names = ["id"]
    hmds["user"] = user
    hmds["attendant"] = attendant
    hmds[["sucessive_requester", "prior_service"]] = np.nan
    hmds[["prior_service_type"]] = None
    hmds[["operation_state"]] = "ASLEEP"
    hmds[["symptoms"]] = ""
    hmds[["other_symptoms"]] = ""
    hmds[["diagnoses"]] = ""
    hmds[["other_diagnoses"]] = ""
    hmds[["closing_reason"]] = ""
    hmds[["send_email"]] = False
    hmds[["destination_address"]] = ""
    hmds[["crew"]] = ""
    hmds[["delivery_note"]] = ""
    hmds[["critical_inventory"]] = False
    hmds[["acute_inventory"]] = False
    hmds[["medical_equipment_delivery"]] = False
    hmds[["lab_examination"]] = False
    hmds[["source"]] = ""
    hmds["destination_departure_datetime"] = hmds["end"]
    hmds["cancellation_reason"] = ""
    hmds.loc[hmds["bANUL"] == True, "status"] = "CANCELLED"
    hmds.loc[hmds["bABOR"] == True, "status"] = "CANCELLED"
    hmds.loc[hmds["status"] != "CANCELLED", "status"] = "CLOSED"
    # hmds.loc[hmds['MEDICO'] != '', 'attendant'] = hmds['MEDICO']
    # hmds.loc[hmds['attendant'].isnull(), 'attendant'] = 'MigrationAttendant'
    hmds["policy"] = (
        "MigrationPolicy-V"
        + hmds["CEDULA"].str.extract(r"^([A-Z]*-?)?(\d*)( #\d*)?", expand=False)[
            1
        ]
    )
    hmds.loc[hmds["scheduled"] == True, "scheduled_date_time"] = hmds["start"]
    hmds.loc[hmds["DIAGNOSTICO"] != "SERVICIO ABORTADO", "transfer_reason"] = hmds[
        "DIAGNOSTICO"
    ]
    hmds = hmds[FIELDS_HMD]
    hmds["attention"] = hmds["attention"].map(original_attention2id)
    hmds["attention"] = pd.to_numeric(hmds["attention"]).astype("Int64")
    hmds["policy"] = hmds["policy"].map(code2id)
    hmds["policy"] = pd.to_numeric(hmds["policy"]).astype("Int64")
    # Eliminar servicios sin atención definida
    hmds["attention"].replace("", np.nan, inplace=True)
    hmds.dropna(subset=["attention"], inplace=True)
    return hmds


def get_PHD_records(service_type, rep_sort):
    phds = list()
    for i in service_type:
        match = re.search("^AT. PHD", i)
        if match:
            phds.append(i)
    return rep_sort.loc[rep_sort["TIPO_SERVICIO"].isin(set(phds))]


def build_PHD(hhp_df):
    hhps = hhp_df.rename(columns=hhp_rename, inplace=False)
    hhps.index.names = ["id"]
    hhps.columns.names = ["id"]
    hhps["user"] = user
    hhps["attendant"] = attendant
    hhps[["sucessive_requester", "prior_service"]] = np.nan
    hhps[["prior_service_type"]] = None
    hhps[["operation_state"]] = "ASLEEP"
    hhps[["symptoms"]] = ""
    hhps[["other_symptoms"]] = ""
    hhps[["diagnoses"]] = ""
    hhps[["other_diagnoses"]] = ""
    hhps[["closing_reason"]] = "Completado"
    hhps[["send_email"]] = False
    hhps[["destination_address"]] = ""
    hhps[["crew"]] = ""
    hhps[["delivery_note"]] = ""
    hhps[["critical_inventory"]] = False
    hhps[["acute_inventory"]] = False
    hhps[["medical_equipment_delivery"]] = False
    hhps[["source"]] = ""
    hhps["destination_departure_datetime"] = hhps["end"]
    hhps["cancellation_reason"] = ""
    hhps.loc[hhps["bANUL"] == True, "status"] = "CANCELLED"
    hhps.loc[hhps["bABOR"] == True, "status"] = "CANCELLED"
    hhps.loc[hhps["status"] != "CANCELLED", "status"] = "CLOSED"
    # hhps.loc[hhps['MEDICO'] != '', 'attendant'] = hhps['MEDICO']
    # hhps.loc[hhps['attendant'].isnull(), 'attendant'] = 'MigrationAttendant'
    hhps["policy"] = (
        "MigrationPolicy-V"
        + hhps["CEDULA"].str.extract(r"^([A-Z]*-?)?(\d*)( #\d*)?", expand=False)[
            1
        ]
    )
    hhps.loc[hhps["scheduled"] == True, "scheduled_date_time"] = hhps["start"]
    hhps.loc[hhps["DIAGNOSTICO"] != "SERVICIO ABORTADO", "transfer_reason"] = hhps[
        "DIAGNOSTICO"
    ]
    hhps = hhps[FIELDS_HHP]
    hhps["attention"] = hhps["attention"].map(original_attention2id)
    hhps["attention"] = pd.to_numeric(hhps["attention"]).astype("Int64")
    hhps["policy"] = hhps["policy"].map(code2id)
    hhps["policy"] = pd.to_numeric(hhps["policy"]).astype("Int64")
    # Eliminar servicios sin atención definida
    hhps["attention"].replace("", np.nan, inplace=True)
    hhps.dropna(subset=["attention"], inplace=True)
    return hhps


def get_AMD_records(service_type, rep_sort):
    hhcs = list()
    for i in service_type:
        match = re.search("^AMD", i)
        if match:
            hhcs.append(i)
    return rep_sort.loc[rep_sort["TIPO_SERVICIO"].isin(set(hhcs))]


def build_AMD(hhc_df):
    hhcs = hhc_df.rename(columns=hhc_rename, inplace=False)
    hhcs.index.names = ["id"]
    hhcs.columns.names = ["id"]
    hhcs["user"] = user
    hhcs["attendant"] = attendant
    hhcs[["sucessive_requester", "prior_service"]] = np.nan
    hhcs[["prior_service_type"]] = None
    hhcs[["operation_state"]] = "ASLEEP"
    hhcs[["symptoms"]] = ""
    hhcs[["other_symptoms"]] = ""
    hhcs[["diagnoses"]] = ""
    hhcs[["other_diagnoses"]] = ""
    hhcs[["closing_reason"]] = "Completado"
    hhcs[["send_email"]] = False
    hhcs[["destination_address"]] = ""
    hhcs[["crew"]] = ""
    hhcs[["request_medicine"]] = ""
    hhcs[["request_advice"]] = ""
    hhcs[["request_medical_indication"]] = ""
    hhcs[["lab_tests"]] = ""
    hhcs[["request_external_service"]] = ""
    hhcs[["request_lab_exam"]] = ""
    hhcs[["request_medical_specialist"]] = ""
    hhcs[["lab_examination"]] = False
    hhcs["destination_departure_datetime"] = hhcs["end"]
    hhcs["cancellation_reason"] = ""
    hhcs.loc[hhcs["bANUL"] == True, "status"] = "CANCELLED"
    hhcs.loc[hhcs["bABOR"] == True, "status"] = "CANCELLED"
    hhcs.loc[hhcs["status"] != "CANCELLED", "status"] = "CLOSED"
    # hhcs.loc[hhcs['MEDICO'] != '', 'attendant'] = hhcs['MEDICO']
    # hhcs.loc[hhcs['attendant'].isnull(), 'attendant'] = 'MigrationAttendant'
    hhcs["policy"] = (
        "MigrationPolicy-V"
        + hhcs["CEDULA"].str.extract(r"^([A-Z]*-?)?(\d*)( #\d*)?", expand=False)[
            1
        ]
    )
    hhcs.loc[hhcs["scheduled"] == True, "scheduled_date_time"] = hhcs["start"]
    hhcs.loc[hhcs["DIAGNOSTICO"] != "SERVICIO ABORTADO", "transfer_reason"] = hhcs[
        "DIAGNOSTICO"
    ]
    hhcs = hhcs[FIELDS_HHC]
    hhcs["attention"] = hhcs["attention"].map(original_attention2id)
    hhcs["attention"] = pd.to_numeric(hhcs["attention"]).astype("Int64")
    hhcs["policy"] = hhcs["policy"].map(code2id)
    hhcs["policy"] = pd.to_numeric(hhcs["policy"]).astype("Int64")
    hhcs["attention"].replace("", np.nan, inplace=True)
    hhcs.dropna(subset=["attention"], inplace=True)
    return hhcs


def get_OMT_records(service_type, rep_sort):
    tmgs = list()
    for i in service_type:
        match = re.search("^(OMT|ORIENTACION)", i)
        if match:
            tmgs.append(i)
    return rep_sort.loc[rep_sort["TIPO_SERVICIO"].isin(set(tmgs))]


def build_OMT(tmg_df):
    tmgs = tmg_df.rename(columns=hhc_rename, inplace=False)
    tmgs.index.names = ["id"]
    tmgs.columns.names = ["id"]
    tmgs["user"] = user
    tmgs["attendant"] = attendant
    tmgs[["sucessive_requester", "prior_service"]] = np.nan
    tmgs[["prior_service_type"]] = None
    tmgs[["operation_state"]] = "ASLEEP"
    tmgs[["symptoms"]] = ""
    tmgs[["other_symptoms"]] = ""
    tmgs[["diagnoses"]] = ""
    tmgs[["other_diagnoses"]] = ""
    tmgs[["closing_reason"]] = "Completado"
    tmgs[["send_email"]] = False
    tmgs[["request_medicine"]] = ""
    tmgs[["request_advice"]] = ""
    tmgs[["request_medical_indication"]] = ""
    tmgs[["lab_tests"]] = ""
    tmgs[["request_external_service"]] = ""
    tmgs[["request_lab_exam"]] = ""
    tmgs[["request_medical_specialist"]] = ""
    tmgs[["lab_examination"]] = False
    tmgs[["call_end"]] = np.nan
    tmgs["cancellation_reason"] = tmgs["CAUSAL_ANUL_ABOR"].str.title()
    tmgs.loc[tmgs["bANUL"] == True, "status"] = "CANCELLED"
    tmgs.loc[tmgs["bABOR"] == True, "status"] = "CANCELLED"
    tmgs.loc[tmgs["status"] != "CANCELLED", "status"] = "CLOSED"
    # tmgs.loc[tmgs['MEDICO'] != '', 'attendant'] = tmgs['MEDICO']
    # tmgs.loc[tmgs['attendant'].isnull(), 'attendant'] = 'MigrationAttendant'
    tmgs["policy"] = (
        "MigrationPolicy-V"
        + tmgs["CEDULA"].str.extract(r"^([A-Z]*-?)?(\d*)( #\d*)?", expand=False)[
            1
        ]
    )
    tmgs.loc[tmgs["scheduled"] == True, "scheduled_date_time"] = tmgs["start"]
    tmgs.loc[tmgs["DIAGNOSTICO"] != "SERVICIO ABORTADO", "transfer_reason"] = tmgs[
        "DIAGNOSTICO"
    ]
    tmgs[["call_start"]] = tmgs["start"]
    tmgs = tmgs[FIELDS_TMG]
    tmgs["attention"] = tmgs["attention"].map(original_attention2id)
    tmgs["attention"] = pd.to_numeric(tmgs["attention"]).astype("Int64")
    tmgs["policy"] = tmgs["policy"].map(code2id)
    tmgs["policy"] = pd.to_numeric(tmgs["policy"]).astype("Int64")
    # Eliminar servicios sin atención definida
    tmgs["attention"].replace("", np.nan, inplace=True)
    tmgs.dropna(subset=["attention"], inplace=True)
    return tmgs


if __name__ == "__main__":
    # Missing: Read reports from EXCEL.
    reports = read_reports("reports.pkl")
    # Missing: Sort reports.
    reports_sorted = group_attentions("rep_sort.pkl")
    # Prepare for import.
    attentions = get_attentions(reports_sorted)
    attentions = build_attentions(attentions)
    attentions.to_csv("attentions_input.csv", index=False)
    policies = build_policies(attentions)
    policies.to_csv("policies_input.csv", index=False)
    policies_id = load_and_get_ids("policies", "policies.admin.PolicyResource")
    code2id = policies_id.set_index("code").to_dict()["id"]
    affiliates = build_affiliates(reports_sorted)
    affiliates.to_csv("affiliates_input.csv", index=False)
    affiliates_id = load_and_get_ids("affiliates", "affiliates.admin.AffiliateResource")
    attentions_affiliates = build_attentions_affiliates(affiliates_id)
    attentions_affiliates.to_csv("attentions_input.csv", index=False)
    attentions_id = load_and_get_ids("attentions", "attentions.admin.AttentionResource")
    original_attention2id = attentions_id.set_index("closing_reason").to_dict()["id"]
    service_type = reports.TIPO_SERVICIO.unique()
    transfer_df = get_TLD_records(service_type, reports_sorted)
    transfers = build_transfers(transfer_df)
    transfers.to_csv("transfers_input.csv", index=False)
    run_import_export(
        "import_file", "transfers_input.csv", "services.admin.TransferResource"
    )
    lab_df = get_LAB_records(service_type, reports_sorted)
    labs = build_LAB(lab_df)
    labs.to_csv("labs_input.csv", index=False)
    run_import_export("import_file", "labs_input.csv", "services.admin.LABResource")
    hmd_df = get_HMD_records(service_type, reports_sorted)
    hmds = build_HMD(hmd_df)
    hmds.to_csv("hmds_input.csv", index=False)
    run_import_export("import_file", "hmds_input.csv", "services.admin.HMDResource")
    hhp_df = get_PHD_records(service_type, reports_sorted)
    hhps = build_PHD(hhp_df)
    hhps.to_csv("hhps_input.csv", index=False)
    run_import_export("import_file", "hhps_input.csv", "services.admin.HHPResource")
    hhc_df = get_AMD_records(service_type, reports_sorted)
    hhcs = build_AMD(hhc_df)
    hhcs.to_csv("hhcs_input.csv", index=False)
    run_import_export("import_file", "hhcs_input.csv", "services.admin.HHCResource")
    tmg_df = get_OMT_records(service_type, reports_sorted)
    tmgs = build_OMT(tmg_df)
    tmgs.to_csv("tmgs_input.csv", index=False)
    run_import_export("import_file", "tmgs_input.csv", "services.admin.TMGResource")
