from rest_framework import serializers

from documents.models import DocumentType, MedicalDocument, LabDocument, HHCLabDocument


class DocumentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentType
        fields = "__all__"


class MedicalDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicalDocument
        fields = "__all__"


class LabDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = LabDocument
        fields = "__all__"


class HHCLabDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = HHCLabDocument
        fields = "__all__"
