from rest_framework import generics
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from documents.api.serializers import (
    DocumentTypeSerializer,
    MedicalDocumentSerializer,
    LabDocumentSerializer,
    HHCLabDocumentSerializer,
)
from documents.models import (
    DocumentType,
    MedicalDocument,
    LabDocument,
    HHCLabDocument,
)


class DocumentTypeListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = DocumentType.objects.all()
    serializer_class = DocumentTypeSerializer


class DocumentTypeDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = DocumentType.objects.all()
    serializer_class = DocumentTypeSerializer


class MedicalDocumentListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = MedicalDocument.objects.all()
    serializer_class = MedicalDocumentSerializer

    filter_fields = (
        "document_type",
        "attention",
        "attention__status",
        "attention__follow_up",
    )
    ordering_fields = ("uploaded_at",)


class MedicalDocumentDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = MedicalDocument.objects.all()
    serializer_class = MedicalDocumentSerializer


class MedicalDocumentUpload(APIView):
    parser_classes = [MultiPartParser, FormParser]
    permission_classes = [
        IsAuthenticated,
    ]

    def post(self, request, format=None):
        serializer = MedicalDocumentSerializer(data=request.data,)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LabDocumentListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = LabDocument.objects.all()
    serializer_class = LabDocumentSerializer

    filter_fields = ("lab",)
    search_fields = ("description",)
    ordering_fields = ("uploaded_at",)


class LabDocumentDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = LabDocument.objects.all()
    serializer_class = LabDocumentSerializer


class LabDocumentUpload(APIView):
    parser_classes = [MultiPartParser, FormParser]
    permission_classes = [
        IsAuthenticated,
    ]

    def post(self, request, format=None):
        serializer = LabDocumentSerializer(data=request.data,)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class HHCLabDocumentListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHCLabDocument.objects.all()
    serializer_class = HHCLabDocumentSerializer

    filter_fields = ("hhc",)
    search_fields = ("description",)
    ordering_fields = ("uploaded_at",)


class HHCLabDocumentDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHCLabDocument.objects.all()
    serializer_class = HHCLabDocumentSerializer


class HHCLabDocumentUpload(APIView):
    parser_classes = [MultiPartParser, FormParser]
    permission_classes = [
        IsAuthenticated,
    ]

    def post(self, request, format=None):
        serializer = HHCLabDocumentSerializer(data=request.data,)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)