from django.urls import path

from . import views

app_name = 'documents'

urlpatterns = [
    path(
        "document_types",
        views.DocumentTypeListView.as_view(),
        name="document_type_list"
    ),
    path(
        "document_types/<pk>",
        views.DocumentTypeDetailView.as_view(),
        name="document_type_detail"
    ),
    path(
        "medical_documents",
        views.MedicalDocumentListView.as_view(),
        name="medical_document_list"
    ),
    path(
        "medical_documents/<pk>",
        views.MedicalDocumentDetailView.as_view(),
        name="medical_document_detail"
    ),
    path(
        "medical_document_upload",
        views.MedicalDocumentUpload.as_view(),
        name="medical_document_upload"),
    path(
        "lab_documents",
        views.LabDocumentListView.as_view(),
        name="laboratory_document_list"
    ),
    path(
        "lab_documents/<pk>",
        views.LabDocumentDetailView.as_view(),
        name="laboratory_document_detail"
    ),
    path(
        "lab_document_upload",
        views.LabDocumentUpload.as_view(),
        name="laboratory_document_upload"
    ),
    path(
        "hhc_lab_documents",
        views.HHCLabDocumentListView.as_view(),
        name="HHC_laboratory_document_list"
    ),
    path(
        "hhc_lab_documents/<pk>",
        views.HHCLabDocumentDetailView.as_view(),
        name="HHC_laboratory_document_detail"
    ),
    path(
        "hhc_lab_document_upload",
        views.HHCLabDocumentUpload.as_view(),
        name="HHC_laboratory_document_upload"
    ),
]
