"""
Modelos, vistas y serializadores de documentos médicos que pueden cargarse en la
aplicación por medio del API.
"""

default_app_config = 'documents.apps.DocumentsConfig'