"""
Modelos relacionados con los documentos que se suben a la aplicación como
soporte a las atenciones. 
"""

import os

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from attentions.models import Attention
from services.models import LAB, HHC


def upload_to(instance, filename):
    now = timezone.now()
    base, extension = os.path.splitext(filename.lower())
    return f"medical_documents/{now:%Y-%m}/ATT_{instance.attention.id}-{instance.document_type}-{now:%Y%m%dT%H%M%S-%f}{extension}"


def lab_upload_to(instance, filename):
    now = timezone.now()
    base, extension = os.path.splitext(filename.lower())
    return f"lab_documents/{now:%Y-%m}/LAB_{instance.lab.id}-{now:%Y%m%dT%H%M%S-%f}{extension}"


def hhc_lab_upload_to(instance, filename):
    now = timezone.now()
    base, extension = os.path.splitext(filename.lower())
    return f"lab_documents/{now:%Y-%m}/HHC_{instance.hhc.id}-{now:%Y%m%dT%H%M%S-%f}{extension}"


class DocumentType(models.Model):
    """
    Tipo de Documento.
    """

    name = models.CharField(_("nombre"), max_length=255,)
    code = models.CharField(_("código"), max_length=15, blank=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = _("Tipo de Documento")
        verbose_name_plural = _("Tipos de Documento")


class MedicalDocument(models.Model):
    """
    Documento Médico.
    """

    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to=upload_to)
    document_type = models.ForeignKey(
        DocumentType,
        on_delete=models.CASCADE,
        verbose_name="tipo de documento",
        blank=True,
        null=True,
    )
    attention = models.ForeignKey(
        Attention,
        on_delete=models.CASCADE,
        related_name="medical_documents",
        verbose_name="atención",
        blank=True,
        null=True,
    )
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def delete(self, *args, **kwargs):
        os.remove(os.path.join(settings.MEDIA_ROOT, self.document.name))
        super(MedicalDocument, self).delete(*args, **kwargs)

    def __str__(self):
        return f"({self.document_type.name}-{self.uploaded_at})"

    class Meta:
        verbose_name = _("Documento Médico")
        verbose_name_plural = _("Documentos Médicos")


class LabDocument(models.Model):
    """
    Documento de Laboratorio.
    """

    description = models.CharField(_("descripción"),max_length=511, blank=True)
    document = models.FileField(_("documento"), upload_to=lab_upload_to)
    lab = models.ForeignKey(
        LAB,
        on_delete=models.CASCADE,
        related_name="lab_documents",
        verbose_name="LAB",
        blank=True,
        null=True,
    )
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def delete(self, *args, **kwargs):
        os.remove(os.path.join(settings.MEDIA_ROOT, self.document.name))
        super(LabDocument, self).delete(*args, **kwargs)

    def __str__(self):
        return f"(LAB-doc-{self.lab.id}-{self.uploaded_at})"

    class Meta:
        verbose_name = _("Documento de Laboratorio")
        verbose_name_plural = _("Documentos de Laboratorio")


class HHCLabDocument(models.Model):
    """
    Documento de laboratorio asociado a un AMD.
    """

    description = models.CharField(_("descripción"),max_length=511, blank=True)
    document = models.FileField(_("documento"), upload_to=hhc_lab_upload_to)
    hhc = models.ForeignKey(
        HHC,
        on_delete=models.CASCADE,
        related_name="hhc_lab_documents",
        verbose_name="HHC",
        blank=True,
        null=True,
    )
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def delete(self, *args, **kwargs):
        os.remove(os.path.join(settings.MEDIA_ROOT, self.document.name))
        super(HHCLabDocument, self).delete(*args, **kwargs)

    def __str__(self):
        return f"(HHC-Lab-doc-{self.hhc.id}-{self.uploaded_at})"

    class Meta:
        verbose_name = _("Documento de Laboratorio de AMD")
        verbose_name_plural = _("Documentos de Laboratorio de AMD")
