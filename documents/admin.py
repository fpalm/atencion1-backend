from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin


from .models import (
    DocumentType,
    MedicalDocument,
    LabDocument,
)


class DocumentTypeAdmin(ImportExportModelAdmin):
    class Meta:
        model = DocumentType
        fields = ("code", "name")


class DocumentTypeResource(resources.ModelResource):
    class Meta:
        model = DocumentType


class MedicalDocumentAdmin(ImportExportModelAdmin):
    class Meta:
        model = MedicalDocument
        raw_id_fields = ("attention",)
        fields = ("document_type", "attention", "uploaded_at", "description", "document")


class LabDocumentAdmin(ImportExportModelAdmin):
    class Meta:
        model = LabDocument
        raw_id_fields = ("lab",)
        fields = ("document_type", "lab", "uploaded_at", "description", "document")


admin.site.register(DocumentType, DocumentTypeAdmin)
admin.site.register(MedicalDocument, MedicalDocumentAdmin)
admin.site.register(LabDocument, LabDocumentAdmin)
