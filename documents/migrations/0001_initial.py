# Generated by Django 2.2.7 on 2020-09-23 22:40

from django.db import migrations, models
import django.db.models.deletion
import documents.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('attentions', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DocumentType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='nombre')),
                ('code', models.CharField(blank=True, max_length=15, verbose_name='código')),
            ],
            options={
                'verbose_name': 'Tipo de Documento',
                'verbose_name_plural': 'Tipos de Documento',
            },
        ),
        migrations.CreateModel(
            name='HHCLabDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(blank=True, max_length=511, verbose_name='descripción')),
                ('document', models.FileField(upload_to=documents.models.hhc_lab_upload_to, verbose_name='documento')),
                ('uploaded_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Documento de Laboratorio de AMD',
                'verbose_name_plural': 'Documentos de Laboratorio de AMD',
            },
        ),
        migrations.CreateModel(
            name='LabDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(blank=True, max_length=511, verbose_name='descripción')),
                ('document', models.FileField(upload_to=documents.models.lab_upload_to, verbose_name='documento')),
                ('uploaded_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Documento de Laboratorio',
                'verbose_name_plural': 'Documentos de Laboratorio',
            },
        ),
        migrations.CreateModel(
            name='MedicalDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(blank=True, max_length=255)),
                ('document', models.FileField(upload_to=documents.models.upload_to)),
                ('uploaded_at', models.DateTimeField(auto_now_add=True)),
                ('attention', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='medical_documents', to='attentions.Attention', verbose_name='atención')),
                ('document_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='documents.DocumentType', verbose_name='tipo de documento')),
            ],
            options={
                'verbose_name': 'Documento Médico',
                'verbose_name_plural': 'Documentos Médicos',
            },
        ),
    ]
