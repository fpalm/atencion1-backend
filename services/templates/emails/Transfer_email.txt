Venemergencia
¡Te cuidamos mejor!

Resumen del servicio de Traslado de Paciente


Paciente: {{ affiliate.first_name }} {{ affiliate.last_name}}.
Documento de identidad: {{ affiliate.dni}}.
Plan de seguros: {{ transfer.policy.plan.name }} de {{ transfer.policy.plan.organization.name }}.
Atendido por: {{ transfer.attendant.first_name }} {{ transfer.attendant.last_name}}.
ID de atención: {{ attention.id}}.
ID de servicio: {{ transfer.id}}.
Fecha y hora de inicio del servicio: {{ transfer.start}}.
Fecha y hora de finalización del servicio: {{ transfer.end}}.



Venemergencia AG C.A. Calle Cecilio Acosta, entre Calle Páez y Sucre, Edificio BTU, Chacao, Caracas. 0800-VENEMED (8363633)
¿Quieres ponerte en contacto con nosotros?
http://www.grupov.com.ve
