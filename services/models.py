"""
Modelos relacionados con los servicios. Incluye los tipos de servicios, y los
modelos para almacenar los datos de los servicios prestados. Incluyendo modelos
relacionados como el origen de servicio, la dirección de servicio, razón de
cancelación, artículo de entrega y nota de entrega.

Se utiliza un modelo abstracto, `Service`, que incluye los campos comunes a todos
los servicios, y otro modelo abstracto `ExternalService` heredado del anterior
para los servicios externos que abarca los campos comunes a los servicios que
requieren de unidades móviles.
"""

import datetime

import pytz
from babel.dates import format_datetime
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _

# from terminologies.models import Condition
from locations.models import Address
from services.managers import (
    HHCManager,
    TMGManager,
    TransferManager,
    ServiceAddressManager,
    HMDManager,
    LABManager,
    HHPManager,
)


class ServiceType(models.Model):
    """
    Tipo de servicio.
    """

    name = models.CharField(
        _("nombre"),
        max_length=255,
    )
    code = models.CharField(_("código"), max_length=8, blank=True)

    def __str__(self):
        return f"{self.name or self.code}"

    class Meta:
        verbose_name = _("Tipo de Servicio")
        verbose_name_plural = _("Tipos de Servicios")


class ServiceSource(models.Model):
    """
    Orígen del servicio. Medio por el que se recibe la solicitud del servicio.
    """

    name = models.CharField(
        _("nombre"),
        max_length=255,
    )
    code = models.CharField(_("código"), max_length=8, blank=True)

    def __str__(self):
        return f"{self.name or self.code}"

    class Meta:
        verbose_name = _("Origen de Servicio")
        verbose_name_plural = _("Orígenes de Servicios")


class ServiceAddress(Address):
    """
    Dirección de Servicio.
    """

    objects = ServiceAddressManager()

    def __str__(self):
        return "{0}, {1}".format(self.address_line1, self.address_line2)

    class Meta:
        verbose_name = _("Dirección de Servicio")
        verbose_name_plural = _("Direcciones de Servicios")


class CancellationReason(models.Model):
    """
    Razón de cancelación de servicio.
    """

    name = models.CharField(_("nombre"), max_length=65)
    code = models.CharField(_("código"), max_length=4)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = _("Motivo de cancelación del servicio")
        verbose_name_plural = _("Motivos de cancelación de los servicios")


class LabTest(models.Model):
    """
    Examen de laboratorio.
    """

    name = models.CharField(
        _("nombre"),
        max_length=255,
    )
    code = models.CharField(_("código"), max_length=8, blank=True)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = _("Examen de laboratorio")
        verbose_name_plural = _("Exámenes de laboratorio")


class Service(models.Model):
    """
    Servicio.
    """

    SERVICE_STATUS = (
        ("IN_PROGRESS", _("En progreso")),
        ("COMPLETED", _("Completado")),
        ("CLOSED", _("Cerrado")),
        ("DELAYED", _("Retrasado")),
        ("OPEN", _("Abierto")),
        ("CANCELED", _("Cancelado")),
    )
    SERVICE_OPERATION_STATE = (
        ("ASLEEP", _("Dormido")),
        ("AWAKE", _("Despierto")),
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_user",
        verbose_name=_("usuario"),
    )
    attendant = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_attendant",
        verbose_name=_("encargado"),
        help_text=_("persona que atiende el servicio"),
        null=True,
    )
    attention = models.ForeignKey(
        "attentions.Attention",
        on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_attention",
        verbose_name=_("atención"),
    )
    policy = models.ForeignKey(
        "policies.Policy",
        on_delete=models.PROTECT,
        related_name="%(app_label)s_%(class)s_policy",
        verbose_name=_("póliza"),
        help_text=_("póliza a través de la cual se presta el servicio"),
        null=True,
    )
    successive_requester = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="%(app_label)s_%(class)s_successive_requester",
        verbose_name=_("solicitante del sucesivo"),
        help_text=_("usuario que solicitó este servicio como sucesivo"),
        blank=True,
        null=True,
    )
    prior_service = models.IntegerField(
        _("servicio previo"),
        help_text=_("servicio desde el que se solicitó este servicio"),
        blank=True,
        null=True,
    )
    prior_service_type = models.ForeignKey(
        ServiceType,
        on_delete=models.PROTECT,
        related_name="%(app_label)s_%(class)s_prior_service_type",
        verbose_name=_("tipo de servicio previo"),
        help_text=_("tipo de servicio desde que el que se solicitó este servicio"),
        blank=True,
        null=True,
    )
    start = models.DateTimeField(_("inicio del servicio"), blank=True, null=True)
    end = models.DateTimeField(_("finalización del servicio"), blank=True, null=True)
    status = models.CharField(
        _("estado"), max_length=25, choices=SERVICE_STATUS, default="OPEN"
    )
    cancellation_reason = models.ForeignKey(
        CancellationReason,
        on_delete=models.PROTECT,
        verbose_name=_("motivo de cancelación"),
        blank=True,
        null=True,
    )
    operation_state = models.CharField(
        _("estado de la operación"),
        max_length=25,
        choices=SERVICE_OPERATION_STATE,
        default="AWAKE",
    )
    scheduled = models.BooleanField(_("programado"))
    scheduled_date_time = models.DateTimeField(
        _("fecha y hora de programación"), blank=True, null=True
    )
    imputable = models.BooleanField(_("imputable"), blank=True, null=True)
    symptoms = models.ManyToManyField(
        "terminologies.ICD10Symptom",
        related_name="%(app_label)s_%(class)s_symptoms",
        verbose_name=_("síntomas"),
        blank=True,
    )
    other_symptoms = models.CharField(
        _("otros síntomas"), max_length=511, blank=True, null=True
    )
    diagnoses = models.ManyToManyField(
        "terminologies.ICD10Diagnose",
        related_name="%(app_label)s_%(class)s_diagnosis",
        verbose_name=_("diagnósticos"),
        blank=True,
    )
    other_diagnoses = models.CharField(
        _("otros diagnósticos"), max_length=511, blank=True, null=True
    )
    closing_reason = models.TextField(_("motivo de cierre"), blank=True, null=True)
    comments = models.TextField(_("observaciones"), blank=True)
    send_email = models.BooleanField(_("enviar email"), default=False)

    def __str__(self):
        return f"AT-{self.attention.id}: {self.attention.affiliate}; {self.get_status_display().upper()}; {format_datetime(self.start, 'EEE dd MMM YYYY : HH:mm', locale='es')}"


    class Meta:
        verbose_name = _("Servicio")
        verbose_name_plural = _("Servicios")
        abstract = True
        ordering = ("start",)


class TMG(Service):
    """
    Orientación Médica Telefónica.
    """

    call_start = models.DateTimeField(
        _("inicio de la llamada telefónica"), blank=True, null=True
    )
    call_end = models.DateTimeField(
        _("finalización de la llamada telefónica"), blank=True, null=True
    )

    request_medicine = models.TextField(
        _("solicitud de medicamentos"), blank=True, default=""
    )
    request_external_service = models.TextField(
        _("solicitud de servicios externos"), blank=True, default=""
    )
    request_medical_specialist = models.TextField(
        _("referencia a especialista médico"), blank=True, default=""
    )
    request_advice = models.TextField(_("recomendaciones"), blank=True, default="")
    request_lab_exam = models.TextField(
        _("exámenes de laboratorio"), blank=True, default=""
    )
    request_medical_indication = models.TextField(
        _("indicaciones médicas"), blank=True, default=""
    )
    is_pediatric = models.BooleanField(_("es pediátrico"), default=False)

    objects = TMGManager()

    def save(self, *args, **kwargs):
        if not self.start:
            self.start = datetime.datetime.now()
        super(TMG, self).save(*args, **kwargs)

    def __str__(self):
        return f"AT-{self.attention.id}: OMT-{self.id}: {self.attention.affiliate}; {self.get_status_display().upper()}; {format_datetime(self.start, 'EEE dd MMM YYYY : HH:mm', locale='es')}"


    class Meta:
        verbose_name = _("Orientación Médica Telefónica / OMT")
        verbose_name_plural = _("OMTs")


class ExternalService(Service):
    """
    Servicio externo.
    """

    destination_address = models.ForeignKey(
        ServiceAddress,
        on_delete=models.PROTECT,
        related_name="%(app_label)s_%(class)s_destinations",
        verbose_name=_("dirección destino"),
        blank=True,
        null=True,
    )
    crew = models.ForeignKey(
        "crews.Crew",
        on_delete=models.PROTECT,
        related_name="%(app_label)s_%(class)s_crews",
        verbose_name=_("tripulación"),
        default=None,
        blank=True,
        null=True,
    )
    dispatch_datetime = models.DateTimeField(_("despacho"), blank=True, null=True)
    base_departure_datetime = models.DateTimeField(
        _("salida de la base"), blank=True, null=True
    )
    destination_arrival_datetime = models.DateTimeField(
        _("llegada al destino"), blank=True, null=True
    )
    destination_departure_datetime = models.DateTimeField(
        _("salida del destino / disponible"), blank=True, null=True
    )
    base_arrival_datetime = models.DateTimeField(
        _("llegada a la base"), blank=True, null=True
    )
    transfer_reason = models.TextField(_("motivo de traslado"), blank=True, null=True)

    class Meta:
        verbose_name = _("servicio externo")
        verbose_name_plural = _("servicios externos")
        abstract = True


class Transfer(ExternalService):
    """
    Traslado de Emergencia.
    """

    origin_address = models.ForeignKey(
        ServiceAddress,
        on_delete=models.PROTECT,
        related_name="%(app_label)s_%(class)s_origins",
        verbose_name=_("dirección origen"),
        blank=True,
        null=True,
    )
    origin_arrival_datetime = models.DateTimeField(
        _("llegada a origen"), blank=True, null=True
    )
    origin_departure_datetime = models.DateTimeField(
        _("salida de origen"), blank=True, null=True
    )
    origin_second_arrival_datetime = models.DateTimeField(
        _("segunda llegada a origen"), blank=True, null=True
    )
    origin_second_departure_datetime = models.DateTimeField(
        _("segunda salida de origen"), blank=True, null=True
    )
    emergency = models.BooleanField(_("emergencia"), default=False)
    simple_trip = models.BooleanField(_("viaje simple"), default=True)
    interinstitutional = models.BooleanField(_("interinstitucional"), default=False)
    interstate = models.BooleanField(_("interestatal"), default=False)
    icu = models.BooleanField(_("UCI"), default=False)
    covered = models.BooleanField(_("cobertura activada"), default=False)

    objects = TransferManager()

    def save(self, *args, **kwargs):
        if not self.start:
            self.start = datetime.datetime.now()
        super(Transfer, self).save(*args, **kwargs)

    def __str__(self):
        return f"AT-{self.attention.id}: TLD-{self.id}: {self.attention.affiliate}; {self.get_status_display().upper()}; {format_datetime(self.start, 'EEE dd MMM YYYY : HH:mm', locale='es')}"

    class Meta:
        verbose_name = _("Traslado")
        verbose_name_plural = _("Traslados")


class HHC(ExternalService):
    """
    Atención Médica Domiciliaria.
    """

    request_medicine = models.TextField(
        _("solicitud de medicamentos"), blank=True, default=""
    )
    request_external_service = models.TextField(
        _("solicitud de servicios externos"), blank=True, default=""
    )
    request_medical_specialist = models.TextField(_("referencia a especialista médico"), blank=True, default="")
    request_advice = models.TextField(_("recomendaciones"), blank=True, default="")
    request_lab_exam = models.TextField(
        _("exámenes médicos"), blank=True, default=""
    )
    request_medical_indication = models.TextField(
        _("indicaciones médicas"), blank=True, default=""
    )
    lab_examination = models.BooleanField(_("examen de laboratorio"), default=False)
    lab_tests = models.ManyToManyField(
        LabTest,
        related_name="%(app_label)s_%(class)s_hhcs",
        verbose_name=_("exámenes de laboratorio"),
        blank=True,
    )

    objects = HHCManager()

    def save(self, *args, **kwargs):
        if not self.start:
            self.start = datetime.datetime.now()
        super(HHC, self).save(*args, **kwargs)

    def __str__(self):
        return f"AT-{self.attention.id}: AMD-{self.id}: {self.attention.affiliate}; {self.get_status_display().upper()}; {format_datetime(self.start, 'EEE dd MMM YYYY : HH:mm', locale='es')}"

    class Meta:
        verbose_name = _("Atención Médica Domiciliaria / AMD")
        verbose_name_plural = _("AMDs")


class LAB(ExternalService):
    """
    Examen de laboratorio domiciliario.
    """

    lab_tests = models.ManyToManyField(
        LabTest,
        related_name="%(app_label)s_%(class)s_labs",
        verbose_name=_("exámenes de laboratorio"),
    )

    objects = LABManager()

    def save(self, *args, **kwargs):
        if not self.start:
            self.start = datetime.datetime.now()
        super(LAB, self).save(*args, **kwargs)

    def __str__(self):
        return f"AT-{self.attention.id}: LAB-{self.id}: {self.attention.affiliate}; {self.get_status_display().upper()}; {format_datetime(self.start, 'EEE dd MMM YYYY : HH:mm', locale='es')}"

    class Meta:
        verbose_name = _("Laboratorio / LAB")
        verbose_name_plural = _("LABs")


class HHP(ExternalService):
    """
    Programa de hospitalización domiciliaria.
    """

    source = models.ForeignKey(
        ServiceSource,
        on_delete=models.CASCADE,
        verbose_name=_("origen del servicio"),
        default=None,
        blank=True,
        null=True,
    )

    objects = HHPManager()

    def save(self, *args, **kwargs):
        if not self.start:
            self.start = datetime.datetime.now()
        super(HHP, self).save(*args, **kwargs)

    def __str__(self):
        return f"AT-{self.attention.id}: PHD-{self.id}: {self.attention.affiliate}; {self.get_status_display().upper()}; {format_datetime(self.start, 'EEE dd MMM YYYY : HH:mm', locale='es')}"

    class Meta:
        verbose_name = _("Plan de Hospitalización Domiciliaria / PHD")
        verbose_name_plural = _("PHDs")


class DeliveryItem(models.Model):
    """
    Artículo de entrega.
    """

    medicinal_product = models.ForeignKey(
        "terminologies.MedicinalProduct",
        on_delete=models.CASCADE,
        verbose_name=_("producto medicinal"),
    )
    delivery_note = models.ForeignKey(
        "DeliveryNote",
        on_delete=models.CASCADE,
        default=None,
        verbose_name=_("nota de entrega"),
    )
    quantity = models.DecimalField(_("cantidad"), max_digits=8, decimal_places=3)
    quantity_unit = models.CharField(_("unidad de medida"), max_length=32)

    class Meta:
        verbose_name = _("Medicamento Solicitado")
        verbose_name_plural = _("Medicamentos Solicitados")


class DeliveryNote(models.Model):
    """
    Nota de entrega.
    """

    description = models.TextField(_("descripción"))
    medicinal_products = models.ManyToManyField(
        "terminologies.MedicinalProduct",
        through=DeliveryItem,
        verbose_name=_("medicamentos"),
    )

    class Meta:
        verbose_name = _("nota de entrega")
        verbose_name_plural = _("notas de entrega")


class HMD(ExternalService):
    """
    Entrega de medicamentos.
    """

    delivery_note = models.ForeignKey(
        DeliveryNote,
        on_delete=models.CASCADE,
        verbose_name=_("nota de entrega"),
        default=None,
        blank=True,
        null=True,
    )

    critical_inventory = models.BooleanField(_("inventario crítico"), default=False)
    acute_inventory = models.BooleanField(_("inventario agudo"), default=False)
    medical_equipment_delivery = models.BooleanField(
        _("entrega de equipos médicos"), default=False
    )
    lab_examination = models.BooleanField(_("examen de laboratorio"), default=False)
    source = models.ForeignKey(
        ServiceSource,
        on_delete=models.CASCADE,
        verbose_name=_("origen del servicio"),
        default=None,
        blank=True,
        null=True,
    )
    from_private_practitioner = models.BooleanField(
        _("de médico particular"), default=False
    )

    objects = HMDManager()

    def save(self, *args, **kwargs):
        if not self.start:
            self.start = datetime.datetime.now()
        super(HMD, self).save(*args, **kwargs)

    def __str__(self):
        return f"AT-{self.attention.id}: EMD-{self.id}: {self.attention.affiliate}; {self.get_status_display().upper()}; {format_datetime(self.start, 'EEE dd MMM YYYY : HH:mm', locale='es')}"

    class Meta:
        verbose_name = _("Entrega de Medicamentos a Domicilio / EMD")
        verbose_name_plural = _("EMDs")
