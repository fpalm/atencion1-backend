import pytz
from rest_framework import serializers

from attentions.models import Attention
from crews.api.serializers import CrewSerializer, CrewMinimalSerializer
from locations.api.serializers import (
    AdminThirdLevelSerializer,
    PlaceSerializer,
)
from policies.models import Policy
from profiles.api.serializers import UserSerializer
from services.models import (
    TMG,
    HHC,
    LAB,
    HHP,
    HMD,
    Transfer,
    ServiceType,
    ExternalService,
    Service,
    ServiceAddress,
    DeliveryItem,
    DeliveryNote,
    ServiceSource,
    CancellationReason,
    LabTest,
)
from terminologies.api.serializers import (
    ICD10SymptomSerializer,
    ICD10DiagnoseSerializer,
    CallingQuestionnaireSerializer,
)
from terminologies.models import MedicinalProduct


class PolicyServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Policy
        fields = "__all__"
        depth = 2


class LabTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = LabTest
        fields = "__all__"


class ServiceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceType
        fields = "__all__"


class ServiceSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceSource
        fields = "__all__"


class ServiceAddressSerializer(serializers.ModelSerializer):
    place = PlaceSerializer()
    region = AdminThirdLevelSerializer(read_only=True)

    class Meta:
        model = ServiceAddress
        fields = "__all__"


class ServiceAddressSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceAddress
        fields = "__all__"


class ServiceAddressByUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceAddress
        exclude = ("last_update_user",)
        depth = 5


class CancellationReasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = CancellationReason
        fields = "__all__"


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = "__all__"
        abstract = True


class ExternalServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExternalService
        fields = "__all__"
        abstract = True


class DeliveryItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryItem
        fields = (
            "id",
            "quantity",
            "quantity_unit",
            "medicinal_product",
        )
        depth = 2


class DeliveryNoteSerializer(serializers.ModelSerializer):
    medicinal_products = DeliveryItemSerializer(
        source="deliveryitem_set", many=True
    )

    class Meta:
        model = DeliveryNote
        fields = "__all__"
        depth = 1


class TMGSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    status = serializers.CharField(source="get_status_display")
    symptoms = ICD10SymptomSerializer(many=True)
    diagnoses = ICD10DiagnoseSerializer(many=True)
    policy = PolicyServiceSerializer()
    attendant = UserSerializer()
    successive_requester = UserSerializer()
    calling_questionnaires = CallingQuestionnaireSerializer(
        many=True, read_only=True
    )

    class Meta:
        model = TMG
        fields = "__all__"


class HHCSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    status = serializers.CharField(source="get_status_display")
    crew = CrewSerializer()
    destination_address = ServiceAddressSerializer()
    symptoms = ICD10SymptomSerializer(many=True)
    diagnoses = ICD10DiagnoseSerializer(many=True)
    policy = PolicyServiceSerializer()
    attendant = UserSerializer()
    successive_requester = UserSerializer()
    lab_tests = LabTestSerializer(many=True, read_only=True)

    class Meta:
        model = HHC
        fields = "__all__"


class LABSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    status = serializers.CharField(source="get_status_display")
    crew = CrewSerializer()
    destination_address = ServiceAddressSerializer()
    symptoms = ICD10SymptomSerializer(many=True)
    diagnoses = ICD10DiagnoseSerializer(many=True)
    policy = PolicyServiceSerializer()
    attendant = UserSerializer()
    successive_requester = UserSerializer()
    lab_tests = LabTestSerializer(many=True, read_only=True)

    class Meta:
        model = LAB
        fields = "__all__"


class HHPSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    status = serializers.CharField(source="get_status_display")
    crew = CrewSerializer()
    destination_address = ServiceAddressSerializer()
    symptoms = ICD10SymptomSerializer(many=True)
    diagnoses = ICD10DiagnoseSerializer(many=True)
    policy = PolicyServiceSerializer()
    attendant = UserSerializer()
    successive_requester = UserSerializer()

    class Meta:
        model = HHP
        fields = "__all__"


class TransferSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    status = serializers.CharField(source="get_status_display")
    crew = CrewSerializer()
    destination_address = ServiceAddressSerializer()
    origin_address = ServiceAddressSerializer()
    symptoms = ICD10SymptomSerializer(many=True)
    diagnoses = ICD10DiagnoseSerializer(many=True)
    policy = PolicyServiceSerializer()
    attendant = UserSerializer()
    successive_requester = UserSerializer()

    class Meta:
        model = Transfer
        fields = "__all__"


class HMDSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    status = serializers.CharField(source="get_status_display")
    crew = CrewSerializer()
    destination_address = ServiceAddressSerializer()
    symptoms = ICD10SymptomSerializer(many=True)
    diagnoses = ICD10DiagnoseSerializer(many=True)
    policy = PolicyServiceSerializer()
    delivery_note = DeliveryNoteSerializer()
    attendant = UserSerializer()
    successive_requester = UserSerializer()

    class Meta:
        model = HMD
        fields = "__all__"


# ----- Simple Serializers -----


class DeliveryItemSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryItem
        fields = (
            "id",
            "quantity",
            "quantity_unit",
            "medicinal_product",
        )


class DeliveryNoteSimpleSerializer(serializers.ModelSerializer):
    medicinal_products = DeliveryItemSimpleSerializer(
        source="deliveryitem_set", many=True, read_only=True
    )

    class Meta:
        model = DeliveryNote
        fields = "__all__"

    def create(self, validated_data):
        delivery_note = DeliveryNote.objects.create(**validated_data)
        if "medicinal_products" in self.initial_data:
            medicinal_products = self.initial_data.get("medicinal_products")
            for product in medicinal_products:
                product_id = product["medicinal_product"]
                quantity = product.get("quantity")
                quantity_unit = product.get("quantity_unit")
                product_instance = MedicinalProduct.objects.get(pk=product_id)
                DeliveryItem(
                    delivery_note=delivery_note,
                    medicinal_product=product_instance,
                    quantity=quantity,
                    quantity_unit=quantity_unit,
                ).save()
        delivery_note.save()
        return delivery_note

    def update(self, instance, validated_data):
        instance.description = validated_data.get(
            "description", instance.description
        )
        if "medicinal_products" in self.initial_data:
            medicinal_products = self.initial_data.get("medicinal_products")
            instance.medicinal_products.clear()
            for product in medicinal_products:
                product_id = product["medicinal_product"]
                quantity = product.get("quantity")
                quantity_unit = product.get("quantity_unit")
                product_instance = MedicinalProduct.objects.get(pk=product_id)
                DeliveryItem(
                    delivery_note=instance,
                    medicinal_product=product_instance,
                    quantity=quantity,
                    quantity_unit=quantity_unit,
                ).save()
        instance.save()
        return instance


class HHCSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = HHC
        fields = "__all__"

    def create(self, validated_data):
        symptoms = validated_data.pop("symptoms")
        diagnoses = validated_data.pop("diagnoses")
        lab_tests = validated_data.pop("lab_tests")
        hhc = HHC.objects.create(**validated_data)
        hhc.symptoms.set(symptoms)
        hhc.diagnoses.set(diagnoses)
        hhc.lab_tests.set(lab_tests)
        return hhc

    def update(self, instance, validated_data):

        if "symptoms" in validated_data:
            symptoms = validated_data.pop("symptoms")
            instance.symptoms.set(symptoms)
        if "diagnoses" in validated_data:
            diagnoses = validated_data.pop("diagnoses")
            instance.diagnoses.set(diagnoses)
        if "lab_tests" in validated_data:
            lab_tests = validated_data.pop("lab_tests")
            instance.lab_tests.set(lab_tests)

        instance.save()
        hhc = super().update(instance, validated_data)
        Attention.objects.get(pk=instance.attention.id).save()
        return hhc


class LABSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = LAB
        fields = "__all__"

    def create(self, validated_data):
        symptoms = validated_data.pop("symptoms")
        diagnoses = validated_data.pop("diagnoses")
        lab_tests = validated_data.pop("lab_tests")

        lab = LAB.objects.create(**validated_data)
        lab.symptoms.set(symptoms)
        lab.diagnoses.set(diagnoses)
        lab.lab_tests.set(lab_tests)
        return lab

    def update(self, instance, validated_data):

        if "symptoms" in validated_data:
            symptoms = validated_data.pop("symptoms")
            instance.symptoms.set(symptoms)
        if "diagnoses" in validated_data:
            diagnoses = validated_data.pop("diagnoses")
            instance.diagnoses.set(diagnoses)
        if "lab_tests" in validated_data:
            lab_tests = validated_data.pop("lab_tests")
            instance.lab_tests.set(lab_tests)

        instance.save()
        lab = super().update(instance, validated_data)
        Attention.objects.get(pk=instance.attention.id).save()
        return lab


class TMGSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = TMG
        fields = "__all__"

    def create(self, validated_data):
        return super().create(validated_data)

    def update(self, instance, validated_data):
        tmg = super().update(instance, validated_data)
        Attention.objects.get(pk=instance.attention.id).save()
        return tmg


class TransferSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transfer
        fields = "__all__"

    def create(self, validated_data):
        symptoms = validated_data.pop("symptoms")
        diagnoses = validated_data.pop("diagnoses")
        transfer = Transfer.objects.create(**validated_data)
        transfer.symptoms.set(symptoms)
        transfer.diagnoses.set(diagnoses)
        return transfer

    def update(self, instance, validated_data):

        if "symptoms" in validated_data:
            symptoms = validated_data.pop("symptoms")
            instance.symptoms.set(symptoms)
        if "diagnoses" in validated_data:
            diagnoses = validated_data.pop("diagnoses")
            instance.diagnoses.set(diagnoses)

        instance.save()
        transfer = super().update(instance, validated_data)
        Attention.objects.get(pk=instance.attention.id).save()
        return transfer


class HHPSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = HHP
        fields = "__all__"

    def create(self, validated_data):
        symptoms = validated_data.pop("symptoms")
        diagnoses = validated_data.pop("diagnoses")
        hhp = HHP.objects.create(**validated_data)
        hhp.symptoms.set(symptoms)
        hhp.diagnoses.set(diagnoses)
        return hhp

    def update(self, instance, validated_data):

        if "symptoms" in validated_data:
            symptoms = validated_data.pop("symptoms")
            instance.symptoms.set(symptoms)
        if "diagnoses" in validated_data:
            diagnoses = validated_data.pop("diagnoses")
            instance.diagnoses.set(diagnoses)

        instance.save()
        hhp = super().update(instance, validated_data)
        Attention.objects.get(pk=instance.attention.id).save()
        return hhp


class HMDSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = HMD
        fields = "__all__"

    def create(self, validated_data):
        symptoms = validated_data.pop("symptoms")
        diagnoses = validated_data.pop("diagnoses")
        hmd = HMD.objects.create(**validated_data)
        hmd.symptoms.set(symptoms)
        hmd.diagnoses.set(diagnoses)
        return hmd

    def update(self, instance, validated_data):

        if "symptoms" in validated_data:
            symptoms = validated_data.pop("symptoms")
            instance.symptoms.set(symptoms)
        if "diagnoses" in validated_data:
            diagnoses = validated_data.pop("diagnoses")
            instance.diagnoses.set(diagnoses)

        instance.save()
        hmd = super().update(instance, validated_data)
        Attention.objects.get(pk=instance.attention.id).save()
        return hmd


class TMGMinimalSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source="get_status_display")
    policy_org_code = serializers.SerializerMethodField()

    class Meta:
        model = TMG
        fields = (
            "id",
            "status",
            "policy_org_code",
            "scheduled",
            "scheduled_date_time",
        )

    def get_policy_org_code(self, obj):
        return obj.policy.plan.organization.code


class ExternalServiceMinimalSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source="get_status_display")
    policy_org_code = serializers.SerializerMethodField()
    crew = CrewMinimalSerializer()
    destination_first_level = serializers.SerializerMethodField()

    class Meta:
        model = TMG
        fields = (
            "id",
            "status",
            "crew",
            "destination_first_level",
            "policy_org_code",
            "scheduled",
            "scheduled_date_time",
        )

    def get_policy_org_code(self, obj):
        return obj.policy.plan.organization.code
    def get_destination_first_level(self, obj):
        if obj.destination_address:
            return obj.destination_address.region.second_level.first_level.name
        else:
            return None


# ----- Service Reports -----


class ServiceReportSerializer(serializers.HyperlinkedModelSerializer):
    start = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    end = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    status = serializers.CharField(source="get_status_display")
    client = serializers.SerializerMethodField()
    sponsor = serializers.SerializerMethodField()
    in_charge = serializers.SerializerMethodField()
    attendant = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    dni = serializers.SerializerMethodField()
    birthdate = serializers.SerializerMethodField()
    gender = serializers.SerializerMethodField()
    scheduled = serializers.SerializerMethodField()
    scheduled_date_time = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    imputable = serializers.SerializerMethodField()
    symptoms = serializers.SerializerMethodField()
    diagnoses = serializers.SerializerMethodField()
    cancellation_reason = serializers.SerializerMethodField()

    def get_client(self, obj):
        if obj.policy:
            return obj.policy.plan.organization.name
        else:
            return None

    def get_sponsor(self, obj):
        if obj.policy:
            return obj.policy.sponsor.name
        else:
            return None

    def get_in_charge(self, obj):
        return "{} {}".format(obj.user.first_name, obj.user.last_name)

    def get_attendant(self, obj):
        if obj.attendant:
            return "{} {}".format(
                obj.attendant.first_name, obj.attendant.last_name
            )
        else:
            return ""

    def get_first_name(self, obj):
        return obj.attention.affiliate.first_name

    def get_last_name(self, obj):
        return obj.attention.affiliate.last_name

    def get_dni(self, obj):
        return obj.attention.affiliate.dni

    def get_gender(self, obj):
        return obj.attention.affiliate.get_gender_display()

    def get_birthdate(self, obj):
        return f"{obj.attention.affiliate.birthdate:%Y-%m-%d}"

    # def get_affiliate(self, obj):
    #     return "{} {}".format(
    #         obj.attention.affiliate.first_name,
    #         obj.attention.affiliate.last_name,
    #     )

    def get_scheduled(self, obj):
        return "Si" if obj.scheduled else "No"

    def get_symptoms(self, obj):
        if obj.symptoms:
            return ", ".join([item.term_es for item in obj.symptoms.all()])
        else:
            return ""

    def get_imputable(self, obj):
        return "Si" if obj.scheduled else "No"

    def get_diagnoses(self, obj):
        if obj.diagnoses:
            return ", ".join([item.term_es for item in obj.diagnoses.all()])
        else:
            return ""

    def get_cancellation_reason(self, obj):
        if obj.cancellation_reason:
            return obj.cancellation_reason.name
        else:
            return ""

    class Meta:
        abstract = True
        fields = [
            "id",
            "client",
            "sponsor",
            "start",
            "end",
            "status",
            "in_charge",
            "attendant",
            "first_name",
            "last_name",
            "dni",
            "birthdate",
            "gender",
            "scheduled",
            "scheduled_date_time",
            "imputable",
            "symptoms",
            "other_symptoms",
            "diagnoses",
            "other_diagnoses",
            "cancellation_reason",
            "closing_reason",
            "comments",
        ]


tmg_report_fields_order = [
    0, 1, 2, 3, 23, 24, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 25, 26, 27, 28, 29, 30
]

tmg_report_fields = ServiceReportSerializer.Meta.fields + [
    "call_start",
    "call_end",
    "request_medicine",
    "request_medical_indication",
    "request_external_service",
    "request_medical_specialist",
    "request_advice",
    "request_lab_exam",
]


class TMGReportSerializer(ServiceReportSerializer):
    call_start = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    call_end = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )

    class Meta:
        model = TMG
        fields = [
            tmg_report_fields[tmg_report_fields_order[i]]
            for i in range(len(tmg_report_fields))
        ]


class ExternalServiceReportSerializer(ServiceReportSerializer):
    destination_address = serializers.SerializerMethodField()
    destination_region = serializers.SerializerMethodField()
    crew = serializers.SerializerMethodField()
    dispatch_datetime = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    base_departure_datetime = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    destination_arrival_datetime = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    destination_departure_datetime = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    base_arrival_datetime = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )

    def get_destination_address(self, obj):
        if obj.destination_address:
            return (
                obj.destination_address.address_line1
            )  # TODO pendiente de mejorar
        else:
            return None

    def get_destination_region(self, obj):
        if obj.destination_address:
            return str(obj.destination_address.region)
        else:
            return None

    def get_crew(self, obj):
        if obj.crew:
            return obj.crew.name
        else:
            return "No asignada"

    class Meta:
        abstract = True
        fields = ServiceReportSerializer.Meta.fields + [
            "destination_address",
            "destination_region",
            "crew",
            "dispatch_datetime",
            "base_departure_datetime",
            "destination_arrival_datetime",
            "destination_departure_datetime",
            "base_arrival_datetime",
            "transfer_reason",
        ]


hhc_report_fields_order = [
    0, 1, 2, 3, 26, 27, 28, 29, 30, 4, 5, 6, 7, 8, 9, 10, 11,
    12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41
]

hhc_report_fields = ExternalServiceReportSerializer.Meta.fields + [
    "request_medicine",
    "request_external_service",
    "request_medical_specialist",
    "request_advice",
    "request_lab_exam",
    "request_medical_indication",
    "lab_examination",
    "lab_tests"
]

class HHCReportSerializer(ExternalServiceReportSerializer):
    lab_examination = serializers.SerializerMethodField()
    lab_tests = serializers.SerializerMethodField()

    def get_lab_examination(self, obj):
        return "Si" if obj.lab_examination else "No"

    def get_lab_tests(self, obj):
        if obj.lab_tests:
            return ", ".join([item.code for item in obj.lab_tests.all()])
        else:
            return ""

    class Meta:
        model = HHC
        fields = [
            hhc_report_fields[hhc_report_fields_order[i]]
            for i in range(len(hhc_report_fields))
        ]


transfer_report_fields_order = [
    0, 1, 2, 3, 26, 27, 34, 35, 28, 29, 36, 37, 4, 5, 6, 7, 8, 9, 10,
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 32, 33, 23, 24, 25,
    30, 31, 38, 39, 40, 41, 42, 43
]

transfer_report_fields = ExternalServiceReportSerializer.Meta.fields + [
    "origin_address",
    "origin_region",
    "origin_arrival_datetime",
    "origin_departure_datetime",
    "origin_second_arrival_datetime",
    "origin_second_departure_datetime",
    "emergency",
    "simple_trip",
    "interinstitutional",
    "interstate",
    "icu",
    "covered"
]


class TransferReportSerializer(ExternalServiceReportSerializer):
    origin_address = serializers.SerializerMethodField()
    origin_region = serializers.SerializerMethodField()
    origin_arrival_datetime = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    origin_departure_datetime = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    origin_second_arrival_datetime = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    origin_second_departure_datetime = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    emergency = serializers.SerializerMethodField()
    simple_trip = serializers.SerializerMethodField()
    interinstitutional = serializers.SerializerMethodField()
    interstate = serializers.SerializerMethodField()
    icu = serializers.SerializerMethodField()
    covered = serializers.SerializerMethodField()

    def get_origin_address(self, obj):
        if obj.origin_address:
            return obj.origin_address.address_line1  # TODO pendiente de mejorar
        else:
            return None

    def get_origin_region(self, obj):
        if obj.origin_address:
            return str(obj.origin_address.region)
        else:
            return None

    def get_emergency(self, obj):
        return "Si" if obj.emergency else "No"

    def get_simple_trip(self, obj):
        return "Si" if obj.simple_trip else "No"

    def get_interinstitutional(self, obj):
        return "Si" if obj.interinstitutional else "No"

    def get_interstate(self, obj):
        return "Si" if obj.interstate else "No"

    def get_icu(self, obj):
        return "Si" if obj.icu else "No"

    def get_covered(self, obj):
        return "Si" if obj.covered else "No"

    class Meta:
        model = Transfer
        fields = [
            transfer_report_fields[transfer_report_fields_order[i]]
            for i in range(len(transfer_report_fields))
        ]


lab_report_fields_order = [
    0, 1, 2, 3, 26, 27, 28, 29, 30, 4, 5, 6, 7, 8, 9, 10, 11,
    12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    31, 32
]

lab_report_fields = ExternalServiceReportSerializer.Meta.fields + [
    "lab_tests"
]


class LABReportSerializer(ExternalServiceReportSerializer):
    lab_tests = serializers.SerializerMethodField()

    def get_lab_tests(self, obj):
        if obj.lab_tests:
            return ", ".join([item.code for item in obj.lab_tests.all()])
        else:
            return ""

    class Meta:
        model = LAB
        fields = [
            lab_report_fields[lab_report_fields_order[i]]
            for i in range(len(lab_report_fields))
        ]


hhp_report_fields_order = [
    0, 1, 2, 3, 26, 27, 28, 29, 30, 4, 5, 6, 7, 8, 9, 10, 11,
    12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    31, 32
]

hhp_report_fields = ExternalServiceReportSerializer.Meta.fields + [
    "source"
]


class HHPReportSerializer(ExternalServiceReportSerializer):
    source = serializers.SerializerMethodField()

    def get_source(self, obj):
        return obj.source.name

    class Meta:
        model = HHP
        fields = [
            hhp_report_fields[hhp_report_fields_order[i]]
            for i in range(len(hhp_report_fields))
        ]


hmd_report_fields_order = [
    0, 1, 2, 3, 26, 27, 28, 29, 30, 4, 5, 6, 7, 8, 9, 10, 11,
    12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    31, 32, 33, 34, 35, 36, 37, 38
]

hmd_report_fields = ExternalServiceReportSerializer.Meta.fields + [
        "delivery_note",
        "critical_inventory",
        "acute_inventory",
        "medical_equipment",
        "lab_examination",
        "from_private_practitioner",
        "source",
]


class HMDReportSerializer(ExternalServiceReportSerializer):
    delivery_note = serializers.SerializerMethodField()
    critical_inventory = serializers.SerializerMethodField()
    acute_inventory = serializers.SerializerMethodField()
    medical_equipment = serializers.SerializerMethodField()
    lab_examination = serializers.SerializerMethodField()
    from_private_practitioner = serializers.SerializerMethodField()
    source = serializers.SerializerMethodField()

    def get_delivery_note(self, obj):
        if obj.delivery_note:
            return ", ".join(
                [
                    item.name
                    for item in obj.delivery_note.medicinal_products.all()
                ]
            )
        else:
            return ""

    def get_critical_inventory(self, obj):
        return "Si" if obj.critical_inventory else "No"

    def get_acute_inventory(self, obj):
        return "Si" if obj.acute_inventory else "No"

    def get_medical_equipment(self, obj):
        return "Si" if obj.medical_equipment_delivery else "No"

    def get_lab_examination(self, obj):
        return "Si" if obj.lab_examination else "No"

    def get_from_private_practitioner(self, obj):
        return "Si" if obj.from_private_practitioner else "No"

    def get_source(self, obj):
        if obj.source:
            return obj.source.name
        else:
            return None

    class Meta:
        model = HMD
        fields = [
            hmd_report_fields[hmd_report_fields_order[i]]
            for i in range(len(hmd_report_fields))
        ]
