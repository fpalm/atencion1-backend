from django.utils.translation import gettext
from django_filters import rest_framework as filters
from drf_renderer_xlsx.mixins import XLSXFileMixin
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ReadOnlyModelViewSet

from atencion1.report_styles import header_style, body_style
from services.models import (
    ServiceType,
    ServiceSource,
    ServiceAddress,
    CancellationReason,
    LabTest,
    TMG,
    HHC,
    LAB,
    Transfer,
    HHP,
    HMD,
    DeliveryNote,
)
from .serializers import (
    ServiceTypeSerializer,
    ServiceSourceSerializer,
    ServiceAddressSerializer,
    ServiceAddressSimpleSerializer,
    CancellationReasonSerializer,
    LabTestSerializer,
    TMGSerializer,
    HHCSerializer,
    LABSerializer,
    TransferSerializer,
    HHPSerializer,
    DeliveryNoteSerializer,
    HHCSimpleSerializer,
    LABSimpleSerializer,
    TMGSimpleSerializer,
    TransferSimpleSerializer,
    HHPSimpleSerializer,
    DeliveryNoteSimpleSerializer,
    HMDSerializer,
    HMDSimpleSerializer,
    TMGReportSerializer,
    tmg_report_fields_order,
    HHCReportSerializer,
    hhc_report_fields_order,
    TransferReportSerializer,
    transfer_report_fields_order,
    LABReportSerializer,
    lab_report_fields_order,
    HHPReportSerializer,
    hhp_report_fields_order,
    HMDReportSerializer,
    hmd_report_fields_order,
    ServiceAddressByUserSerializer,
)


# from profiles.custom_permissions import (IsAnyAuthenticatedUser,
#                                          IsOperatorUser,
#                                          IsCoordinatorUser,
#                                          IsManagerUser
#                                          )


FILTER_FIELDS_SERVICE = (
    "user",
    "attendant",
    "attention",
    "policy",
    "status",
    "operation_state",
    "scheduled",
    "imputable",
    "send_email",
)

FILTER_FIELDS_EXTERNAL = (
    "crew",
    "destination_address",
    "destination_address__place",
    "destination_address__region",
)

ORDERING_FIELDS_SERVICE = (
    "start",
    "end",
    "scheduled_date_time",
)

ORDERING_FIELDS_EXTERNAL = (
    "dispatch_datetime",
    "base_departure_datetime",
    "destination_arrival_datetime",
    "destination_departure_datetime",
    "base_arrival_datetime",
)


class ServiceTypeListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ServiceType.objects.all()
    serializer_class = ServiceTypeSerializer


class ServiceTypeDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ServiceType.objects.all()
    serializer_class = ServiceTypeSerializer


class ServiceSourceListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ServiceSource.objects.all()
    serializer_class = ServiceSourceSerializer


class ServiceSourceDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ServiceSource.objects.all()
    serializer_class = ServiceSourceSerializer


class ServiceAddressListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ServiceAddress.objects.all()
    serializer_class = ServiceAddressSerializer

    filter_fields = (
        "address_line2",
        "place",
        "region",
        "zip_code",
        "last_update_user",
        "unknown_region",
    )
    ordering_fields = (
        "id",
        "last_update_time",
    )
    search_fields = (
        "address_line1",
        "address_line2",
        "^zip_code",
    )


class ServiceAddressSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ServiceAddress.objects.all()
    serializer_class = ServiceAddressSimpleSerializer


class ServiceAddressDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ServiceAddress.objects.all()
    serializer_class = ServiceAddressSerializer


class ServiceAddressSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ServiceAddress.objects.all()
    serializer_class = ServiceAddressSimpleSerializer


class CancellationReasonListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = CancellationReason.objects.all()
    serializer_class = CancellationReasonSerializer


class CancellationReasonDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = CancellationReason.objects.all()
    serializer_class = CancellationReasonSerializer


class LabTestListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = LabTest.objects.all()
    serializer_class = LabTestSerializer


class LabTestDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = LabTest.objects.all()
    serializer_class = LabTestSerializer


class TMGListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = TMG.objects.all()
    serializer_class = TMGSerializer

    filter_fields = FILTER_FIELDS_SERVICE + (
        "calling_questionnaires__main_condition",
        "calling_questionnaires__emergency_evaluation",
    )
    ordering_fields = ORDERING_FIELDS_SERVICE + ("call_start", "call_end",)


class TMGSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = TMG.objects.all()
    serializer_class = TMGSimpleSerializer


class TMGDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = TMG.objects.all()
    serializer_class = TMGSerializer


class TMGSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = TMG.objects.all()
    serializer_class = TMGSimpleSerializer


class TMGByAffiliateListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    serializer_class = TMGSerializer

    def get_queryset(self):
        affiliate_id = self.kwargs.get("pk")
        return TMG.objects.service_by_affiliate(affiliate_id)


class HHCListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHC.objects.all()
    serializer_class = HHCSerializer

    filter_fields = (
        FILTER_FIELDS_SERVICE + FILTER_FIELDS_EXTERNAL + ("lab_examination",)
    )
    ordering_fields = ORDERING_FIELDS_SERVICE + ORDERING_FIELDS_EXTERNAL


class HHCSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHC.objects.all()
    serializer_class = HHCSimpleSerializer


class HHCDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHC.objects.all()
    serializer_class = HHCSerializer


class HHCSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHC.objects.all()
    serializer_class = HHCSimpleSerializer


class HHCByAffiliateListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    serializer_class = HHCSerializer

    def get_queryset(self):
        affiliate_id = self.kwargs.get("pk")
        return HHC.objects.service_by_affiliate(affiliate_id)


class LABListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = LAB.objects.all()
    serializer_class = LABSerializer

    filter_fields = FILTER_FIELDS_SERVICE + FILTER_FIELDS_EXTERNAL
    ordering_fields = ORDERING_FIELDS_SERVICE + ORDERING_FIELDS_EXTERNAL


class LABSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = LAB.objects.all()
    serializer_class = LABSimpleSerializer


class LABDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = LAB.objects.all()
    serializer_class = LABSerializer


class LABSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = LAB.objects.all()
    serializer_class = LABSimpleSerializer


class LABByAffiliateListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    serializer_class = LABSerializer

    def get_queryset(self):
        affiliate_id = self.kwargs.get("pk")
        return LAB.objects.service_by_affiliate(affiliate_id)


class TransferListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer

    filter_fields = (
        FILTER_FIELDS_SERVICE
        + FILTER_FIELDS_EXTERNAL
        + (
            "emergency",
            "simple_trip",
            "interinstitutional",
            "interstate",
            "icu",
            "covered",
        )
    )
    ordering_fields = (
        ORDERING_FIELDS_SERVICE
        + ORDERING_FIELDS_EXTERNAL
        + (
            "origin_arrival_datetime",
            "origin_departure_datetime",
            "origin_second_arrival_datetime",
            "origin_second_departure_datetime",
        )
    )


class TransferSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Transfer.objects.all()
    serializer_class = TransferSimpleSerializer


class TransferDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer


class TransferSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Transfer.objects.all()
    serializer_class = TransferSimpleSerializer


class TransferByAffiliateListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    serializer_class = TransferSerializer

    def get_queryset(self):
        affiliate_id = self.kwargs.get("pk")
        return Transfer.objects.service_by_affiliate(affiliate_id)


class HHPListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHP.objects.all()
    serializer_class = HHPSerializer

    filter_fields = FILTER_FIELDS_SERVICE + FILTER_FIELDS_EXTERNAL
    ordering_fields = ORDERING_FIELDS_SERVICE + ORDERING_FIELDS_EXTERNAL


class HHPSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHP.objects.all()
    serializer_class = HHPSimpleSerializer


class HHPDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHP.objects.all()
    serializer_class = HHPSerializer


class HHPSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HHP.objects.all()
    serializer_class = HHPSimpleSerializer


class HHPByAffiliateListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    serializer_class = HHPSerializer

    def get_queryset(self):
        affiliate_id = self.kwargs.get("pk")
        return HHP.objects.service_by_affiliate(affiliate_id)


class HMDListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HMD.objects.all()
    serializer_class = HMDSerializer

    filter_fields = (
        FILTER_FIELDS_SERVICE
        + FILTER_FIELDS_EXTERNAL
        + (
            "delivery_note__medicinal_products__id",
            "critical_inventory",
            "acute_inventory",
            "medical_equipment_delivery",
            "lab_examination",
            "from_private_practitioner",
        )
    )
    ordering_fields = ORDERING_FIELDS_SERVICE + ORDERING_FIELDS_EXTERNAL


class HMDSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HMD.objects.all()
    serializer_class = HMDSimpleSerializer


class HMDDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HMD.objects.all()
    serializer_class = HMDSerializer


class HMDSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = HMD.objects.all()
    serializer_class = HMDSimpleSerializer


class HMDByAffiliateListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    serializer_class = HMDSerializer

    def get_queryset(self):
        affiliate_id = self.kwargs.get("pk")
        return HMD.objects.service_by_affiliate(affiliate_id)


class DeliveryNoteListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = DeliveryNote.objects.all()
    serializer_class = DeliveryNoteSerializer

    filter_fields = (
        "id",
        "medicinal_products__id",
        "medicinal_products__name",
        "medicinal_products__form__id",
        "medicinal_products__form__code",
        "medicinal_products__manufacturer__id",
        "medicinal_products__manufacturer__code",
        "medicinal_products__active_ingredients__id",
        "medicinal_products__active_ingredients__name",
    )


class DeliveryNoteSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = DeliveryNote.objects.all()
    serializer_class = DeliveryNoteSimpleSerializer


class DeliveryNoteDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = DeliveryNote.objects.all()
    serializer_class = DeliveryNoteSerializer


class DeliveryNoteSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = DeliveryNote.objects.all()
    serializer_class = DeliveryNoteSimpleSerializer


class ServiceAddressByAffiliateListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    serializer_class = ServiceAddressByUserSerializer

    filter_fields = (
        "address_line2",
        "place",
        "region",
        "zip_code",
        "last_update_user",
        "unknown_region",
    )
    ordering_fields = (
        "id",
        "last_update_time",
    )
    search_fields = (
        "address_line1",
        "address_line2",
        "^zip_code",
    )

    def get_queryset(self):
        affiliate_id = self.kwargs.get("pk")
        return ServiceAddress.objects.address_by_affiliate(affiliate_id)


# ----- Service Reports -----

services_common_head = [
    (gettext("num"), 10),
    (gettext("Cliente"), 20),
    (gettext("Contratante"), 20),
    (gettext("Inicio"), 20),
    (gettext("Cierre"), 20),
    (gettext("Status"), 10),
    (gettext("Solicitante"), 20),
    (gettext("Encargado"), 20),
    (gettext("Nombres"), 20),
    (gettext("Apellidos"), 20),
    (gettext("Cédula"), 15),
    (gettext("Fecha de Nacimiento"), 15),
    (gettext("Sexo"), 10),
    (gettext("¿Programado?"), 15),
    (gettext("Programación"), 20),
    (gettext("¿Imputable?"), 15),
    (gettext("Síntomas"), 20),
    (gettext("Otros síntomas"), 20),
    (gettext("Diagnósticos"), 20),
    (gettext("Otros diagnósticos"), 20),
    (gettext("Razón de cancelación"), 20),
    (gettext("Razón de cierre"), 20),
    (gettext("Observaciones"), 40),
]

services_common_titles = [title for title, width in services_common_head]

services_common_widths = [width for title, width in services_common_head]

externals_common_head = [
    (gettext("Dirección destino"), 40),
    (gettext("Región destino"), 30),
    (gettext("Tripulación"), 20),
    (gettext("Despacho"), 20),
    (gettext("Salida de Base"), 20),
    (gettext("LLegada a Destino"), 20),
    (gettext("Salida de Destino"), 20),
    (gettext("Disponible"), 20),
    (gettext("Razón de traslado"), 40),
]

external_common_titles = [title for title, width in externals_common_head]
external_common_widths = [width for title, width in externals_common_head]


class TMGFilters(filters.FilterSet):
    start = filters.DateFromToRangeFilter()
    end = filters.DateFromToRangeFilter()
    client = filters.CharFilter(
        field_name="policy__plan__organization__name",
        lookup_expr="icontains",
        label="cliente",
    )

    class Meta:
        model = TMG
        fields = ["start", "client"]


TMG_head = services_common_head + [
    (gettext("Inicio llamada"), 20),
    (gettext("Fin Llamada"), 20),
    (gettext("Medicamentos"), 40),
    (gettext("Indicaciones"), 40),
    (gettext("Servicios Externos"), 40),
    (gettext("Referencia Médica a Especialista"), 40),
    (gettext("Recomendaciones"), 40),
    (gettext("Exámenes de Laboratorio"), 40),
]

TMG_titles = [TMG_head[tmg_report_fields_order[i]][0] for i in range(len(TMG_head))]
TMG_widths = [TMG_head[tmg_report_fields_order[i]][1] for i in range(len(TMG_head))]


class TMGListReport(XLSXFileMixin, ReadOnlyModelViewSet):
    queryset = TMG.objects.all()
    serializer_class = TMGReportSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TMGFilters

    column_header = {
        "titles": TMG_titles,
        "column_width": TMG_widths,
        "height": 20,
        "style": header_style,
    }
    body = {
        "height": 20,
        "style": body_style,
    }


class HHCFilters(filters.FilterSet):
    start = filters.DateFromToRangeFilter()
    end = filters.DateFromToRangeFilter()
    client = filters.CharFilter(
        field_name="policy__plan__organization__name",
        lookup_expr="icontains",
        label="cliente",
    )

    class Meta:
        model = HHC
        fields = ["start", "client"]


HHC_head = (
    services_common_head
    + externals_common_head
    + [
        (gettext("Medicamentos"), 40),
        (gettext("Servicios Externos"), 40),
        (gettext("Referencia Médica a Especialista"), 40),
        (gettext("Recomendaciones"), 40),
        (gettext("Exámenes de Laboratorio"), 40),
        (gettext("Indicaciones"), 40),
    ]
)

HHC_titles = [HHC_head[hhc_report_fields_order[i]][0] for i in range(len(HHC_head))]
HHC_widths = [HHC_head[hhc_report_fields_order[i]][1] for i in range(len(HHC_head))]


class HHCListReport(XLSXFileMixin, ReadOnlyModelViewSet):
    queryset = HHC.objects.all()
    serializer_class = HHCReportSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = HHCFilters

    column_header = {
        "titles": HHC_titles,
        "column_width": HHC_widths,
        "height": 20,
        "style": header_style,
    }
    body = {
        "height": 20,
        "style": body_style,
    }


class TransferFilters(filters.FilterSet):
    start = filters.DateFromToRangeFilter()
    end = filters.DateFromToRangeFilter()
    client = filters.CharFilter(
        field_name="policy__plan__organization__name",
        lookup_expr="icontains",
        label="cliente",
    )

    class Meta:
        model = Transfer
        fields = ["start", "client"]


Transfer_head = (
    services_common_head
    + externals_common_head
    + [
        (gettext("Dirección origen"), 40),
        (gettext("Región origen"), 30),
        (gettext("Llegada a origen"), 20),
        (gettext("Salida de origen"), 20),
        (gettext("2da llegada a origen"), 20),
        (gettext("2da salida de origen"), 20),
        (gettext("¿Emergencia?"), 15),
        (gettext("¿Simple?"), 15),
        (gettext("¿Interinstitucional?"), 15),
        (gettext("¿Interestatal?"), 15),
        (gettext("¿UCI?"), 15),
        (gettext("¿Activó cobertura?"), 15),
    ]
)

Transfer_titles = [
    Transfer_head[transfer_report_fields_order[i]][0] for i in range(len(Transfer_head))
]
Transfer_widths = [
    Transfer_head[transfer_report_fields_order[i]][1] for i in range(len(Transfer_head))
]


class TransferListReport(XLSXFileMixin, ReadOnlyModelViewSet):
    queryset = Transfer.objects.all()
    serializer_class = TransferReportSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TransferFilters
    column_header = {
        "titles": Transfer_titles,
        "column_width": Transfer_widths,
        "height": 20,
        "style": header_style,
    }
    body = {
        "height": 20,
        "style": body_style,
    }


class LABFilters(filters.FilterSet):
    start = filters.DateFromToRangeFilter()
    end = filters.DateFromToRangeFilter()
    client = filters.CharFilter(
        field_name="policy__plan__organization__name",
        lookup_expr="icontains",
        label="cliente",
    )

    class Meta:
        model = LAB
        fields = ["start", "client"]


LAB_head = (
    services_common_head
    + externals_common_head
    + [(gettext("Exámenes de laboratorio"), 60),]
)

LAB_titles = [LAB_head[lab_report_fields_order[i]][0] for i in range(len(LAB_head))]
LAB_widths = [LAB_head[lab_report_fields_order[i]][1] for i in range(len(LAB_head))]


class LABListReport(XLSXFileMixin, ReadOnlyModelViewSet):
    queryset = LAB.objects.all()
    serializer_class = LABReportSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = LABFilters

    column_header = {
        "titles": LAB_titles,
        "column_width": LAB_widths,
        "height": 20,
        "style": header_style,
    }
    body = {
        "height": 20,
        "style": body_style,
    }


class HHPFilters(filters.FilterSet):
    start = filters.DateFromToRangeFilter()
    end = filters.DateFromToRangeFilter()
    client = filters.CharFilter(
        field_name="policy__plan__organization__name",
        lookup_expr="icontains",
        label="cliente",
    )

    class Meta:
        model = HHP
        fields = ["start", "client"]


HHP_head = (
    services_common_head
    + externals_common_head
    + [(gettext("Origen del servicio"), 40),]
)

HHP_titles = [HHP_head[hhp_report_fields_order[i]][0] for i in range(len(HHP_head))]
HHP_widths = [HHP_head[hhp_report_fields_order[i]][1] for i in range(len(HHP_head))]


class HHPListReport(XLSXFileMixin, ReadOnlyModelViewSet):
    queryset = HHP.objects.all()
    serializer_class = HHPReportSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = HHPFilters

    column_header = {
        "titles": HHP_titles,
        "column_width": HHP_widths,
        "height": 20,
        "style": header_style,
    }
    body = {
        "height": 20,
        "style": body_style,
    }


class HMDFilters(filters.FilterSet):
    start = filters.DateFromToRangeFilter()
    end = filters.DateFromToRangeFilter()
    client = filters.CharFilter(
        field_name="policy__plan__organization__name",
        lookup_expr="icontains",
        label="cliente",
    )

    class Meta:
        model = HMD
        fields = ["start", "client"]


HMD_head = (
    services_common_head
    + externals_common_head
    + [
        (gettext("Nota de Entrega"), 60),
        (gettext("Inventario Crítico"), 20),
        (gettext("Inventario Agudo"), 20),
        (gettext("Equipos Médicos"), 20),
        (gettext("Laboratorio"), 20),
        (gettext("De médico privado"), 20),
        (gettext("Origen del servicio"), 40),
    ]
)

HMD_titles = [HMD_head[hmd_report_fields_order[i]][0] for i in range(len(HMD_head))]
HMD_widths = [HMD_head[hmd_report_fields_order[i]][1] for i in range(len(HMD_head))]


class HMDListReport(XLSXFileMixin, ReadOnlyModelViewSet):
    queryset = HMD.objects.all()
    serializer_class = HMDReportSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = HMDFilters

    column_header = {
        "titles": HMD_titles,
        "column_width": HMD_widths,
        "height": 20,
        "style": header_style,
    }
    body = {
        "height": 20,
        "style": body_style,
    }
