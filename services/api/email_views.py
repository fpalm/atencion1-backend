from datetime import date
from datetime import datetime
import logging

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound
from weasyprint import HTML, CSS

from affiliates.models import Affiliate, AffiliateEmail
from attentions.models import Attention
from policies.models import Policy, Plan
from profiles.models import Profile
from services.models import (
    TMG,
    HHC,
    LAB,
    Transfer,
    HHP,
    HMD,
    DeliveryNote,
    DeliveryItem,
)
from terminologies.models import MedicinalProduct


def datetime_to_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


def check_service_application(service_type: list, affiliate: Affiliate):
    if Policy.objects.filter(
        affiliates__id=affiliate.id, plan__service_types__code__in=service_type
    ):
        return True
    else:
        return False


def check_request_medicine(service):
    return len(service.request_medicine) > 3


def is_int(num):
    if float(num).is_integer():
        return int(num)
    else:
        return float(num)


# Create a logger for this file
logger = logging.getLogger(__file__)


# EMAIL_TO_BCC = ["servicios@venedigital.com"]
# EMAIL_TO_OPMEDICO = ["opmedico@grupov.com.ve"]


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def TMG_send_email(request, pk):
    tmg = TMG.objects.get(pk=pk)
    attention = Attention.objects.get(services_tmg_attention=pk)
    affiliate_id = attention.affiliate.id
    affiliate = Affiliate.objects.get(pk=affiliate_id)
    affiliate_age = datetime_to_age(affiliate.birthdate)
    policy = Policy.objects.get(pk=tmg.policy_id)
    plan = Plan.objects.get(pk=policy.plan.id)
    plan_org = plan.organization
    primary_emails = AffiliateEmail.objects.filter(
        affiliate=affiliate_id, primary=True
    ).distinct()
    time_now = datetime.now()
    if not primary_emails:
        raise NotFound(detail="Sin correo electrónico primario.")
    profile = Profile.objects.get(user=tmg.user.id)
    attendant = Profile.objects.get(user=tmg.attendant.id)
    diagnoses = [str(i)[str(i).rfind(":") + 2:] for i in tmg.diagnoses.all()]
    css = CSS(
        string="""
            @page {
                size: Letter; /* Change from the default size of A4 */
                margin: 1.5cm; /* Set margin on each page */
                @bottom-center{
                            content: "Venemergencia AG C.A. Calle Cecilio Acosta, entre Calle Páez y Sucre, Edificio BTU, Chacao, Caracas. 0800-VENEMED (8363633).";
                            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                            font-size: 10px;
                        }
                    }
            .center {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
            }""",
    )

    # Send service ticket
    subject, from_email, to = (
        "Resumen del servicio de Orientación Médica Teléfonica de Venemergencia (documentos adjuntos)",
        settings.EMAIL_HOST_USER,
        [email.email for email in primary_emails],
    )
    text_content = render(
        request,
        "emails/TMG_email.txt",
        {"tmg": tmg, "affiliate": affiliate, "attention": attention},
        content_type="text",
    )
    html_content = render(
        request,
        "emails/TMG_email.html",
        {"tmg": tmg, "affiliate": affiliate, "attention": attention},
    )
    msg = EmailMultiAlternatives(
        subject, text_content.content.decode("utf-8"), from_email, to, bcc=settings.EMAIL_TO_BCC,
    )
    msg.attach_alternative(html_content.content.decode("utf-8"), "text/html")

    # Send TMG report
    subject = "Informe de servicio de Orientación Médica Telefónica de Venemergencia"
    text_content = render(
        request,
        "emails/TMG_report.txt",
        {
            "tmg": tmg,
            "affiliate": affiliate,
            "attention": attention,
            "attendant": attendant,
        },
        content_type="text",
    )
    report = render(
        request,
        "emails/TMG_report.html",
        {
            "tmg": tmg,
            "affiliate": affiliate,
            "attention": attention,
            "attendant": attendant,
            "diagnoses": diagnoses,
            "time_now": time_now,
            "other_diagnoses": tmg.other_diagnoses,
            "plan_org": plan_org,
            "age": affiliate_age,
        },
    )
    report_pdf = HTML(
        file_obj=report.content, base_url=request.build_absolute_uri()
    ).write_pdf(stylesheets=[css])
    msg.attach("Informe.pdf", report_pdf)

    # Send TMG report to opmedico:
    msg_opmedico = EmailMultiAlternatives(
        f"Copia de solicitud de servicio número {tmg.id}",
        f"Solicitud de servicio número {tmg.id}, proveniente de la atención {attention.id}.",
        from_email,
        settings.EMAIL_TO_OPMEDICO,
    )
    if check_service_application(["EMD"], affiliate) and check_request_medicine(tmg):
        msg_opmedico.attach("Informe.pdf", report_pdf)
    # msg.send()

    # Send TMG medical record
    subject = "Constancia Médica Venemergencia"
    text_content = render(
        request,
        "emails/TMG_medical_record.txt",
        {
            "tmg": tmg,
            "affiliate": affiliate,
            "attention": attention,
            "attendant": attendant,
        },
        content_type="text",
    )
    medical_record = render(
        request,
        "emails/TMG_medical_record.html",
        {
            "tmg": tmg,
            "affiliate": affiliate,
            "attention": attention,
            "diagnoses": diagnoses,
            "age": affiliate_age,
            "profile": profile,
            "attendant": attendant,
            "time_now": time_now,
        },
    )
    medical_record_pdf = HTML(
        file_obj=medical_record.content, base_url=request.build_absolute_uri()
    ).write_pdf(stylesheets=[css])
    msg.attach("Constancia.pdf", medical_record_pdf)

    if (tmg.request_external_service != "") or (tmg.request_lab_exam != ""):
        paraclinical = render(
            request,
            "emails/TMG_paraclinical.html",
            {
                "tmg": tmg,
                "affiliate": affiliate,
                "attention": attention,
                "diagnoses": diagnoses,
                "age": affiliate_age,
                "profile": profile,
                "attendant": attendant,
                "time_now": time_now,
            },
        )
        paraclinical_pdf = HTML(
            file_obj=paraclinical.content, base_url=request.build_absolute_uri()
        ).write_pdf(stylesheets=[css])

        msg.attach("Paraclínicos.pdf", paraclinical_pdf)

    if tmg.request_medical_indication != "":
        medical_indications = render(
            request,
            "emails/TMG_indications.html",
            {
                "tmg": tmg,
                "affiliate": affiliate,
                "attention": attention,
                "diagnoses": diagnoses,
                "age": affiliate_age,
                "profile": profile,
                "attendant": attendant,
                "time_now": time_now,
            },
        )
        medical_indications_pdf = HTML(
            file_obj=medical_indications.content, base_url=request.build_absolute_uri()
        ).write_pdf(stylesheets=[css])
        msg.attach("Indicaciones médicas.pdf", medical_indications_pdf)

        # Send TMG_medical_products to opmedico:
        if check_service_application(["EMD"], affiliate) and check_request_medicine(
            tmg
        ):
            msg_opmedico.attach("Indicaciones médicas.pdf", medical_indications_pdf)

    if tmg.request_medicine != "":
        medicines = render(
            request,
            "emails/TMG_medical_products.html",
            {
                "tmg": tmg,
                "affiliate": affiliate,
                "attention": attention,
                "diagnoses": diagnoses,
                "age": affiliate_age,
                "profile": profile,
                "attendant": attendant,
                "time_now": time_now,
            },
        )
        medicines_pdf = HTML(
            file_obj=medicines.content, base_url=request.build_absolute_uri()
        ).write_pdf(stylesheets=[css])
        msg.attach("Récipe.pdf", medicines_pdf)

        if tmg.request_medical_specialist != "":
            specialist = render(
                request,
                "emails/TMG_specialist.html",
                {
                    "tmg": tmg,
                    "affiliate": affiliate,
                    "attention": attention,
                    "diagnoses": diagnoses,
                    "age": affiliate_age,
                    "profile": profile,
                    "attendant": attendant,
                    "time_now": time_now,
                },
            )
            specialist_pdf = HTML(
                file_obj=specialist.content, base_url=request.build_absolute_uri()
            ).write_pdf(stylesheets=[css])
            msg.attach("Referencia a especialista.pdf", specialist_pdf)

        # Send TMG_medical_products to opmedico:
        if check_service_application(["EMD"], affiliate):
            msg_opmedico.attach("Récipe médico.pdf", medicines_pdf)

    msg.send()
    logger.info("TMG email sent!")
    if check_service_application(["EMD"], affiliate) and check_request_medicine(tmg):
        msg_opmedico.send()
        logger.info("TMG copy to opmedico sent!")
    return html_content


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def Transfer_send_email(request, pk):
    transfer = Transfer.objects.get(pk=pk)
    attention = Attention.objects.get(services_transfer_attention=pk)
    affiliate_id = attention.affiliate.id
    affiliate = Affiliate.objects.get(pk=affiliate_id)
    primary_emails = AffiliateEmail.objects.filter(
        affiliate=affiliate_id, primary=True
    ).distinct()
    if not primary_emails:
        raise NotFound(detail="Sin correo electrónico primario.")
    text_content = render(
        request,
        "emails/Transfer_email.txt",
        {"transfer": transfer, "affiliate": affiliate, "attention": attention},
        content_type="text",
    )
    html_content = render(
        request,
        "emails/Transfer_email.html",
        {"transfer": transfer, "affiliate": affiliate, "attention": attention},
    )
    subject, from_email, to = (
        "Resumen del servicio de Traslados de Venemergencia",
        settings.EMAIL_HOST_USER,
        [email.email for email in primary_emails],
    )
    msg = EmailMultiAlternatives(
        subject, text_content.content.decode("utf-8"), from_email, to, bcc=settings.EMAIL_TO_BCC,
    )
    msg.attach_alternative(html_content.content.decode("utf-8"), "text/html")
    msg.send()
    logger.info("Transfer email sent!")
    return html_content


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def HHC_send_email(request, pk):
    hhc = HHC.objects.get(pk=pk)
    attention = Attention.objects.get(services_hhc_attention=pk)
    affiliate_id = attention.affiliate.id
    affiliate = Affiliate.objects.get(pk=affiliate_id)
    primary_emails = AffiliateEmail.objects.filter(
        affiliate=affiliate_id, primary=True
    ).distinct()
    if not primary_emails:
        raise NotFound(detail="Sin correo electrónico primario.")
    text_content = render(
        request,
        "emails/HHC_email.txt",
        {"hhc": hhc, "affiliate": affiliate, "attention": attention},
        content_type="text",
    )
    html_content = render(
        request,
        "emails/HHC_email.html",
        {"hhc": hhc, "affiliate": affiliate, "attention": attention},
    )
    subject, from_email, to = (
        "Resumen del servicio de Atención Médica Domiciliaria de Venemergencia",
        settings.EMAIL_HOST_USER,
        [email.email for email in primary_emails],
    )
    msg = EmailMultiAlternatives(
        subject, text_content.content.decode("utf-8"), from_email, to, bcc=settings.EMAIL_TO_BCC,
    )
    msg.attach_alternative(html_content.content.decode("utf-8"), "text/html")
    msg.send()
    logger.info("HHC email sent!")
    return html_content


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def LAB_send_email(request, pk):
    lab = LAB.objects.get(pk=pk)
    attention = Attention.objects.get(services_lab_attention=pk)
    affiliate_id = attention.affiliate.id
    affiliate = Affiliate.objects.get(pk=affiliate_id)
    primary_emails = AffiliateEmail.objects.filter(
        affiliate=affiliate_id, primary=True
    ).distinct()
    tests = [str(i.name) for i in lab.lab_tests.all()]
    if not primary_emails:
        raise NotFound(detail="Sin correo electrónico primario.")
    text_content = render(
        request,
        "emails/LAB_email.txt",
        {"lab": lab, "affiliate": affiliate, "attention": attention, "tests": tests, },
        content_type="text",
    )
    html_content = render(
        request,
        "emails/LAB_email.html",
        {"lab": lab, "affiliate": affiliate, "attention": attention, "tests": tests, },
    )
    subject, from_email, to = (
        "Resumen del servicio de Laboratorio de Venemergencia",
        settings.EMAIL_HOST_USER,
        [email.email for email in primary_emails],
    )
    msg = EmailMultiAlternatives(
        subject, text_content.content.decode("utf-8"), from_email, to, bcc=settings.EMAIL_TO_BCC,
    )
    msg.attach_alternative(html_content.content.decode("utf-8"), "text/html")
    msg.send()
    logger.info("LAB email sent!")
    return html_content


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def HHP_send_email(request, pk):
    hhp = HHP.objects.get(pk=pk)
    attention = Attention.objects.get(services_hhp_attention=pk)
    affiliate_id = attention.affiliate.id
    affiliate = Affiliate.objects.get(pk=affiliate_id)
    primary_emails = AffiliateEmail.objects.filter(
        affiliate=affiliate_id, primary=True
    ).distinct()
    if not primary_emails:
        raise NotFound(detail="Sin correo electrónico primario.")
    text_content = render(
        request,
        "emails/HHP_email.txt",
        {"hhp": hhp, "affiliate": affiliate, "attention": attention},
        content_type="text",
    )
    html_content = render(
        request,
        "emails/HHP_email.html",
        {"hhp": hhp, "affiliate": affiliate, "attention": attention},
    )
    subject, from_email, to = (
        "Resumen del servicio de Programa de Hospitalización Domiciliaria de Venemergencia",
        settings.EMAIL_HOST_USER,
        [email.email for email in primary_emails],
    )
    msg = EmailMultiAlternatives(
        subject, text_content.content.decode("utf-8"), from_email, to, bcc=settings.EMAIL_TO_BCC,
    )
    msg.attach_alternative(html_content.content.decode("utf-8"), "text/html")
    msg.send()
    logger.info("HHP email sent!")
    return html_content


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def HMD_send_email(request, pk):
    hmd = HMD.objects.get(pk=pk)
    attention = Attention.objects.get(services_hmd_attention=pk)
    affiliate_id = attention.affiliate.id
    affiliate = Affiliate.objects.get(pk=affiliate_id)
    primary_emails = AffiliateEmail.objects.filter(
        affiliate=affiliate_id, primary=True
    ).distinct()
    if not primary_emails:
        raise NotFound(detail="Sin correo electrónico primario.")
    delivery_note_id = DeliveryNote.objects.get(pk=hmd.delivery_note.id).id
    delivery_items = [
        (item.medicinal_product.id, item.quantity, item.quantity_unit)
        for item in DeliveryItem.objects.filter(delivery_note=delivery_note_id)
    ]
    medicinal_products = [
        (MedicinalProduct.objects.get(id=product).name, is_int(quantity), unit) for product, quantity, unit in delivery_items
    ]
    text_content = render(
        request,
        "emails/HMD_email.txt",
        {
            "hmd": hmd,
            "affiliate": affiliate,
            "attention": attention,
            "delivered": medicinal_products,
        },
        content_type="text",
    )
    html_content = render(
        request,
        "emails/HMD_email.html",
        {
            "hmd": hmd,
            "affiliate": affiliate,
            "attention": attention,
            "delivered": medicinal_products,
        },
    )
    subject, from_email, to = (
        "Resumen del servicio de Entrega de Medicamentos a Domicilio de Venemergencia",
        settings.EMAIL_HOST_USER,
        [email.email for email in primary_emails],
    )
    msg = EmailMultiAlternatives(
        subject, text_content.content.decode("utf-8"), from_email, to, bcc=settings.EMAIL_TO_BCC,
    )
    msg.attach_alternative(html_content.content.decode("utf-8"), "text/html")
    msg.send()
    logger.info("HMD email sent!")
    return html_content
