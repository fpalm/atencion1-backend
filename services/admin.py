from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import (
    ServiceType,
    ServiceSource,
    ServiceAddress,
    CancellationReason,
    LabTest,
    TMG,
    Transfer,
    HHC,
    LAB,
    HHP,
    DeliveryItem,
    DeliveryNote,
    HMD,
)

SERVICE_FIELDS = (
    ("user", "attendant"),
    ("attention"),
    ("status", "operation_state", "cancellation_reason"),
    ("policy", "imputable"),
    ("start", "end"),
    ("successive_requester", "prior_service", "prior_service_type"),
    ("scheduled", "scheduled_date_time"),
    ("symptoms", "other_symptoms"),
    ("diagnoses", "other_diagnoses"),
    ("closing_reason", "comments"),
)

EXTERNAL_FIELDS = (
    "crew",
    ("destination_address", "dispatch_datetime"),
    ("base_departure_datetime", "base_arrival_datetime"),
    ("destination_departure_datetime", "destination_arrival_datetime"),
    "transfer_reason",
)


class ServiceTypeResource(resources.ModelResource):
    class Meta:
        model = ServiceType


class ServiceTypeAdmin(ImportExportModelAdmin):
    model = ServiceType


class ServiceSourceAdmin(ImportExportModelAdmin):
    model = ServiceSource


class ServiceSourceResource(resources.ModelResource):
    class Meta:
        model = ServiceSource


class ServiceAddressResource(resources.ModelResource):
    class Meta:
        model = ServiceAddress


class ServiceAddressAdmin(ImportExportModelAdmin):
    model = ServiceAddress


class CancellationReasonResource(resources.ModelResource):
    class Meta:
        model = CancellationReason


class CancellationReasonAdmin(ImportExportModelAdmin):
    model = CancellationReason


class LabTestResource(resources.ModelResource):
    class Meta:
        model = LabTest


class LabTestAdmin(ImportExportModelAdmin):
    model = LabTest


class TMGResource(resources.ModelResource):
    class Meta:
        model = TMG


class TMGAdmin(ImportExportModelAdmin):
    model = TMG
    readonly_fields = ("start",)
    raw_id_fields = ("attention", "policy", "symptoms", "diagnoses")
    fields = SERVICE_FIELDS + (
        "call_start",
        "call_end",
        "request_medicine",
        "request_external_service",
        "request_medical_specialist",
        "request_advice",
        "request_lab_exam",
        "request_medical_indication",
        "is_pediatric"
    )
    list_display = ("service_attention_id", "service_id", "affiliate", "start", "end")
    list_filter = ("status", "operation_state", "is_pediatric", "start", "end")
    search_fields = [
        "id",
        "attention__id",
        "attention__affiliate__first_name",
        "attention__affiliate__last_name",
        "attention__affiliate__dni__exact",
        "policy__plan__name"
    ]

    def service_attention_id(self, obj):
        return obj.attention.id
    service_attention_id.short_description = "Atención"

    def service_id(self, obj):
        return obj.id
    service_id.short_description = "OMT"

    def affiliate(self, obj):
        return str(obj.attention.affiliate)
    affiliate.short_description = "Afiliado"


class HHCResource(resources.ModelResource):
    class Meta:
        model = HHC


class HHCAdmin(ImportExportModelAdmin):
    model = HHC
    readonly_fields = ("start",)
    raw_id_fields = (
        "attention",
        "policy",
        "symptoms",
        "diagnoses",
        "destination_address",
    )
    fields = (
        SERVICE_FIELDS
        + EXTERNAL_FIELDS
        + (
            "request_medicine",
            "request_external_service",
            "request_medical_specialist",
            "request_advice",
            "request_lab_exam",
            "request_medical_indication",
            "lab_examination",
            "lab_tests",
        )
    )
    list_display = ("service_attention_id", "service_id", "affiliate", "start", "end")
    list_filter = ("status", "operation_state", "start", "end")
    search_fields = [
        "id",
        "attention__id",
        "attention__affiliate__first_name",
        "attention__affiliate__last_name",
        "attention__affiliate__dni__exact",
        "policy__plan__name"
    ]

    def service_attention_id(self, obj):
        return obj.attention.id
    service_attention_id.short_description = "Atención"

    def service_id(self, obj):
        return obj.id
    service_id.short_description = "AMD"

    def affiliate(self, obj):
        return str(obj.attention.affiliate)
    affiliate.short_description = "Afiliado"


class LABResource(resources.ModelResource):
    class Meta:
        model = LAB


class LABAdmin(ImportExportModelAdmin):
    model = LAB
    readonly_fields = ("start",)
    raw_id_fields = (
        "attention",
        "policy",
        "symptoms",
        "diagnoses",
        "destination_address",
        "lab_tests",
    )
    fields = SERVICE_FIELDS + EXTERNAL_FIELDS + ("lab_tests",)
    list_display = ("service_attention_id", "service_id", "affiliate", "start", "end")
    list_filter = ("status", "operation_state", "start", "end")
    search_fields = [
        "id",
        "attention__id",
        "attention__affiliate__first_name",
        "attention__affiliate__last_name",
        "attention__affiliate__dni__exact",
        "policy__plan__name"
    ]

    def service_attention_id(self, obj):
        return obj.attention.id
    service_attention_id.short_description = "Atención"

    def service_id(self, obj):
        return obj.id
    service_id.short_description = "LAB"

    def affiliate(self, obj):
        return str(obj.attention.affiliate)
    affiliate.short_description = "Afiliado"


class TransferResource(resources.ModelResource):
    class Meta:
        model = Transfer


class TransferAdmin(ImportExportModelAdmin):
    model = Transfer
    readonly_fields = ("start",)
    raw_id_fields = (
        "attention",
        "policy",
        "symptoms",
        "diagnoses",
        "origin_address",
        "destination_address",
    )
    fields = (
        SERVICE_FIELDS
        + EXTERNAL_FIELDS
        + (
            ("origin_address"),
            ("origin_arrival_datetime", "origin_departure_datetime"),
            ("origin_second_arrival_datetime", "origin_second_departure_datetime"),
            ("emergency", "simple_trip", "interinstitutional", "interstate", "icu"),
        )
    )
    list_display = ("service_attention_id", "service_id", "affiliate", "start", "end")
    list_filter = ("status", "operation_state", "start", "end")
    search_fields = [
        "id",
        "attention__id",
        "attention__affiliate__first_name",
        "attention__affiliate__last_name",
        "attention__affiliate__dni__exact",
        "policy__plan__name"
    ]

    def service_attention_id(self, obj):
        return obj.attention.id
    service_attention_id.short_description = "Atención"

    def service_id(self, obj):
        return obj.id
    service_id.short_description = "TLD"

    def affiliate(self, obj):
        return str(obj.attention.affiliate)
    affiliate.short_description = "Afiliado"


class DeliveryItemResource(resources.ModelResource):
    class Meta:
        model = DeliveryItem


class DeliveryItemAdmin(ImportExportModelAdmin):
    model = DeliveryItem


class DeliveryItemInline(admin.TabularInline):
    model = DeliveryItem
    extra = 1


class DeliveryNoteResource(resources.ModelResource):
    class Meta:
        model = DeliveryNote


class DeliveryNoteAdmin(ImportExportModelAdmin):
    model = DeliveryNote
    inlines = (DeliveryItemInline,)


class HMDResource(resources.ModelResource):
    class Meta:
        model = HMD


class HMDAdmin(ImportExportModelAdmin):
    model = HMD
    readonly_fields = ("start",)
    raw_id_fields = (
        "attention",
        "policy",
        "symptoms",
        "diagnoses",
        "destination_address",
        "delivery_note",
    )
    fields = (
        SERVICE_FIELDS
        + ("source",)
        + EXTERNAL_FIELDS
        + (
            "delivery_note",
            (
                "critical_inventory",
                "acute_inventory",
                "medical_equipment_delivery",
                "lab_examination",
            ),
        )
    )
    list_display = ("service_attention_id", "service_id", "affiliate", "start", "end")
    list_filter = ("status", "operation_state", "start", "end")
    search_fields = [
        "id",
        "attention__id",
        "attention__affiliate__first_name",
        "attention__affiliate__last_name",
        "attention__affiliate__dni__exact",
        "policy__plan__name"
    ]

    def service_attention_id(self, obj):
        return obj.attention.id
    service_attention_id.short_description = "Atención"

    def service_id(self, obj):
        return obj.id
    service_id.short_description = "PHD"

    def affiliate(self, obj):
        return str(obj.attention.affiliate)
    affiliate.short_description = "Afiliado"


class HHPResource(resources.ModelResource):
    class Meta:
        model = HHP


class HHPAdmin(ImportExportModelAdmin):
    model = HHP
    readonly_fields = ("start",)
    raw_id_fields = (
        "attention",
        "policy",
        "symptoms",
        "diagnoses",
        "destination_address",
    )
    fields = SERVICE_FIELDS + ("source",) + EXTERNAL_FIELDS
    list_display = ("service_attention_id", "service_id", "affiliate", "start", "end")
    list_filter = ("status", "operation_state", "start", "end")
    search_fields = [
        "id",
        "attention__id",
        "attention__affiliate__first_name",
        "attention__affiliate__last_name",
        "attention__affiliate__dni__exact",
        "policy__plan__name"
    ]

    def service_attention_id(self, obj):
        return obj.attention.id
    service_attention_id.short_description = "Atención"

    def service_id(self, obj):
        return obj.id
    service_id.short_description = "PHD"

    def affiliate(self, obj):
        return str(obj.attention.affiliate)
    affiliate.short_description = "Afiliado"


admin.site.register(ServiceType, ServiceTypeAdmin)
admin.site.register(ServiceSource, ServiceSourceAdmin)
admin.site.register(ServiceAddress, ServiceAddressAdmin)
admin.site.register(CancellationReason, CancellationReasonAdmin)
admin.site.register(LabTest, LabTestAdmin)
admin.site.register(TMG, TMGAdmin)
admin.site.register(Transfer, TransferAdmin)
admin.site.register(HHC, HHCAdmin)
admin.site.register(LAB, LABAdmin)
admin.site.register(DeliveryItem, DeliveryItemAdmin)
admin.site.register(DeliveryNote, DeliveryNoteAdmin)
admin.site.register(HMD, HMDAdmin)
admin.site.register(HHP, HHPAdmin)
