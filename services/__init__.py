"""
Modelos, vistas y serializadores de los servicios: OMT, AMD, LAB, EMD, HDM y
TLD. Además de lo directamente relacionado con los modelos de servicios, se
incorporan vistas y serializadores para el envío de correos electrónicos a los
afiliados y la generación de reportes. En esta aplicación se incluye también
tareas en Celery para la actualización periódica de algunos valores de servicios
y atenciones.
"""

default_app_config = 'services.apps.ServicesConfig'
