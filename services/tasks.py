import os

from celery.decorators import periodic_task
from celery.task.schedules import crontab
from celery.utils.log import get_task_logger

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "atencion1.settings")
import django

django.setup()

from django.db.models import Q
from django.utils import timezone
from datetime import timedelta

from attentions.models import Attention
from services.models import HHP, HMD, TMG, Transfer, HHC, LAB

TIME_WINDOW_START = 0  # TODO define as system setting.
TIME_WINDOW_DURATION = 60  # TODO define as system setting.
TIME_DELAYED = 15 # TODO define as system setting.
SCHEDULED_DELAYED = 15 # TODO define as system setting.
SERVICES_LIST = [HHP, TMG, Transfer, HHC, LAB]
SERVICES_MAY_DELAYED = [HHP, TMG, Transfer, HHC, LAB]

def service_asleep_to_awake(start: int, duration: int, services: list):
    """Search for services in state ASLEEP in a time window and change state to AWAKE
    """
    now = timezone.now()
    start = timedelta(minutes=start)
    duration = timedelta(minutes=duration)

    for service in services:
        queryset = service.objects.filter(
            Q(operation_state="ASLEEP"),
            Q(scheduled_date_time__gte=now + start),
            Q(scheduled_date_time__lte=now + start + duration),
        )

        for asleep in queryset:
            asleep.operation_state = "AWAKE"
            attention = Attention.objects.get(pk=asleep.attention.id)
            attention.operation_state = "AWAKE"

            asleep.save()
            attention.save()

    return True


def no_scheduled_delayed(minutes: int, services: list):
    """Search for delayed no scheduled services in state OPEN change state to DELAYED
    """
    now = timezone.now()
    time_window = timedelta(minutes=minutes)

    for service in services:
        queryset = service.objects.filter(
            Q(status="OPEN"), Q(scheduled=False), Q(start__lt=now - time_window)
        )
        for delayed in queryset:
            service_instance = service.objects.get(id=delayed.id)
            service_instance.status = "DELAYED"
            service_instance.save()

    return True


def scheduled_delayed(minutes: int, services: list):
    """Search for delayed scheduled services in state OPEN change state to DELAYED
    """
    now = timezone.now()
    time_window = timedelta(minutes=minutes)

    for service in services:
        queryset = service.objects.filter(
            Q(status="OPEN"),
            Q(scheduled=True),
            Q(scheduled_date_time__lt=now - time_window),
        )
        for delayed in queryset:
            service_instance = service.objects.get(id=delayed.id)
            service_instance.status = "DELAYED"
            service_instance.save()

    return True


logger = get_task_logger(__name__)


@periodic_task(
    run_every=(crontab(minute="*/5")),
    name="task_service_asleep_to_awake",
    ignore_result=True,
)
def task_service_asleep_to_awake():
    """
    Change services operational status to AWAKE
    """
    service_asleep_to_awake(TIME_WINDOW_START, TIME_WINDOW_DURATION, SERVICES_LIST)
    logger.info("Changed service operational status to AWAKE")


@periodic_task(
    run_every=(crontab(minute="*/5")),
    name="task_scheduled_to_delayed",
    ignore_result=True,
)
def task_scheduled_to_delayed():
    """
    Change OPEN to DELAYED status in scheduled services after certain amount of time
    """
    scheduled_delayed(SCHEDULED_DELAYED, SERVICES_LIST)
    logger.info("Changed OPEN scheduled services status to DELAYED")


@periodic_task(
    run_every=(crontab(minute="*/5")),
    name="task_no_scheduled_to_delayed",
    ignore_result=True,
)
def task_no_scheduled_to_delayed():
    """
    Change OPEN to DELAYED status in no scheduled services after certain amount of time
    """
    no_scheduled_delayed(TIME_DELAYED, SERVICES_MAY_DELAYED)
    logger.info("Changed OPEN NO scheduled services status to DELAYED")
