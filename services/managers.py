from django.db import models
from django.db.models import Q


class HHCManager(models.Manager):
    """Manager para filtrar el modelo del servicio HHC.
    """

    def service_by_affiliate(self, affiliate_id: int):
        """HHCs (AMDs) por ID de afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de servicos HHCs (AMDs) relacionados con un afiliado.
        """
        return self.filter(Q(attention__affiliate__pk=affiliate_id))


class TMGManager(models.Manager):
    """Manager para filtrar el modelo del servicio TMG.
    """

    def service_by_affiliate(self, affiliate_id: int):
        """TMGs (OMTs) por ID de afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de servicos TMGs (OMTs) relacionados con un afiliado.
        """
        return self.filter(Q(attention__affiliate__pk=affiliate_id))


class TransferManager(models.Manager):
    """Manager para filtrar el modelo del servicio Transfer.
    """

    def service_by_affiliate(self, affiliate_id: int):
        """Transfers (TLDs) por ID de afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de servicos Transfers (TLDs) relacionados con un afiliado.
        """
        return self.filter(Q(attention__affiliate__pk=affiliate_id))


class HHPManager(models.Manager):
    """Manager para filtrar el modelo del servicio HHP.
    """

    def service_by_affiliate(self, affiliate_id: int):
        """HHPs (PHDs) por ID de afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de servicos HHPs (PHDs) relacionados con un afiliado.
        """
        return self.filter(Q(attention__affiliate__pk=affiliate_id))


class HMDManager(models.Manager):
    """Manager para filtrar el modelo del servicio HMD.
    """

    def service_by_affiliate(self, affiliate_id: int):
        """HMDs (EMDs) por ID de afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de servicos HMDs (EMDs) relacionados con un afiliado.
        """
        return self.filter(Q(attention__affiliate__pk=affiliate_id))


class LABManager(models.Manager):
    """Manager para filtrar el modelo del servicio LAB.
    """

    def service_by_affiliate(self, affiliate_id: int):
        """LABs (LABs) por ID de afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de servicos LABs (LABs) relacionados con un afiliado.
        """
        return self.filter(Q(attention__affiliate__pk=affiliate_id))


class ServiceAddressManager(models.Manager):
    """Manager para filtrar el modelo de ServiceAddressManager.
    """

    def address_by_affiliate(self, affiliate_id: int):
        """Direcciones de servicios filtrados por el ID del afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de direcciones relacionadas con un afiliado.
        """
        return self.filter(
            Q(services_transfer_destinations__attention__affiliate__pk=affiliate_id)
            | Q(services_transfer_origins__attention__affiliate__pk=affiliate_id)
            | Q(services_hhc_destinations__attention__affiliate__pk=affiliate_id)
            | Q(services_hhp_destinations__attention__affiliate__pk=affiliate_id)
            | Q(services_hmd_destinations__attention__affiliate__pk=affiliate_id)
            | Q(services_lab_destinations__attention__affiliate__pk=affiliate_id)
        ).distinct("address_line1", "address_line2")

    def HHC_address_by_affiliate(self, affiliate_id: int):
        """Direcciones de servicios HHC (AMD) por afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de direcciones en cada HHC (AMD) relacionadas con un
            afiliado.
        """
        return self.filter(
            Q(services_hhc_destinations__attention__affiliate__pk=affiliate_id)
        ).distinct("address_line1", "address_line2")

    def HHP_address_by_affiliate(self, affiliate_id: int):
        """Direcciones de servicios HHP (PHD) por afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de direcciones en cada HHP (PHD) relacionadas con un
            afiliado.
        """
        return self.filter(
            Q(services_hhp_destinations__attention__affiliate__pk=affiliate_id)
        ).distinct("address_line1", "address_line2")

    def HMD_address_by_affiliate(self, affiliate_id: int):
        """Direcciones de servicios HMD (EMD) por afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de direcciones en cada HMD (EMD) relacionadas con un
            afiliado.
        """
        return self.filter(
            Q(services_hmd_destinations__attention__affiliate__pk=affiliate_id)
        ).distinct("address_line1", "address_line2")

    def LAB_address_by_affiliate(self, affiliate_id: int):
        """Direcciones de servicios LAB (LAB) por afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de direcciones en cada LAB (LAB) relacionadas con un
            afiliado.
        """
        return self.filter(
            Q(services_lab_destinations__attention__affiliate__pk=affiliate_id)
        ).distinct("address_line1", "address_line2")

    def transfer_address_by_affiliate(self, affiliate_id: int):
        """Direcciones de servicios Transfer (TLD) por afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de direcciones en cada Transfer (TLD) relacionadas con un
            afiliado.
        """
        return self.filter(
            Q(services_transfer_destinations__attention__affiliate__pk=affiliate_id)
            | Q(services_transfer_origins__attention__affiliate__pk=affiliate_id)
        ).distinct("address_line1", "address_line2")
