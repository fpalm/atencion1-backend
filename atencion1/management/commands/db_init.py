import os
import subprocess
from pathlib import Path


BASE_PATH = Path(os.path.dirname(os.path.realpath(__file__)))
DATA_DIR = os.path.join(BASE_PATH.parent, "source_data")
ATENCION1_MANAGE = os.path.join(BASE_PATH.parent.parent.parent)


def run_import(filename, resource_class):
    print(f"Procesando {filename}...")
    return subprocess.run(
        [
            # os.path.join(ATENCION1_ENV, ""),
            "python",
            os.path.join(ATENCION1_MANAGE, "manage.py"),
            "import_file",
            f"{os.path.join(DATA_DIR, filename)}",
            "--resource-class",
            f"{resource_class}",
        ],
        # shell=True,
        check=True,
    )


if __name__ == "__main__":
    run_import("Country.csv", "locations.admin.CountryResource")
    run_import("AdminFirstLevel.csv", "locations.admin.AdminFirstLevelResource")
    run_import("AdminSecondLevel.csv", "locations.admin.AdminSecondLevelResource")
    run_import("AdminThirdLevel.csv", "locations.admin.AdminThirdLevelResource")
    run_import("OrganizationType.csv", "locations.admin.OrganizationTypeResource")
    run_import("Organization.csv", "locations.admin.OrganizationResource")

    run_import("ServiceType.csv", "services.admin.ServiceTypeResource")
    run_import("CancellationReason.csv", "services.admin.CancellationReasonResource")
    run_import("LabTest.csv", "services.admin.LabTestResource")
    run_import("ServiceSource.csv", "services.admin.ServiceSourceResource")
    run_import("ICD10Chapter.csv", "terminologies.admin.ICD10ChapterResource")
    run_import("ICD10Block.csv", "terminologies.admin.ICD10BlockResource")
    run_import("ICD10Category.csv", "terminologies.admin.ICD10CategoryResource")
    run_import("ICD10Diagnose.csv", "terminologies.admin.ICD10DiagnoseResource")
    run_import("ICD10Symptom.csv", "terminologies.admin.ICD10SymptomResource")
    run_import("TriageQuestion.csv", "terminologies.admin.TriageQuestionResource")
    run_import("OptionalTriageQuestion.csv", "terminologies.admin.OptionalTriageQuestionResource")
    run_import("QuestionnaireMessage.csv", "terminologies.admin.QuestionnaireMessageResource")
    run_import("ActiveIngredient.csv", "terminologies.admin.ActiveIngredientResource")
    run_import("MedicationForm.csv", "terminologies.admin.MedicationFormResource")
    run_import("DeliveryUnit.csv", "terminologies.admin.DeliveryUnitResource")
    run_import("MedicinalProduct.csv", "terminologies.admin.MedicinalProductResource")
    run_import("IngredientInProduct.csv", "terminologies.admin.IngredientInProductResource")

    run_import("Plan.csv", "policies.admin.PlanResource")
    run_import("ValidatorPlanMapping.csv", "policies.admin.ValidatorPlanMappingResource")

    run_import("DocumentType.csv", "documents.admin.DocumentTypeResource")

    run_import("Practitioner.csv", "crews.admin.PractitionerResource")
    run_import("Base.csv", "crews.admin.BaseResource")
    run_import("MobileUnit.csv", "crews.admin.MobileUnitResource")

    # Este hay que corregirlo: run_import("User.csv", "user.admin.UserResource")
    # Falta Profiles