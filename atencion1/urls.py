"""atencion1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.generic import TemplateView
from rest_framework_simplejwt import views as jwt_views
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Atención-1 API')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('api/v1/', include('attentions.api.urls', namespace='attentions_api')),
    path('api/v1/', include('policies.api.urls', namespace='policies_api')),
    path('api/v1/', include('affiliates.api.urls', namespace='affiliates_api')),
    path('api/v1/', include('services.api.urls', namespace='services_api')),
    path('api/v1/', include('profiles.api.urls', namespace='profiles_api')),
    path('api/v1/', include('crews.api.urls', namespace='crews_api')),
    path('api/v1/', include('terminologies.api.urls', namespace='terminologies_api')),
    path('api/v1/', include('locations.api.urls', namespace='locations_api')),
    path('api/v1/', include('documents.api.urls', namespace='documents_api')),
    path('api/v1/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/v1/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api-auth/', include('rest_framework.urls')),
    path('rest-auth/', include('rest_auth.urls')),
    path('swagger-docs/', schema_view),
]

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )
else:
    #VueJS Template Manager
    urlpatterns.append(re_path(".*",
        TemplateView.as_view(template_name="index.html"),
        name="atencion1-frontend",
    ))

admin.site.site_header = "Administración de Atención-1"
admin.site.site_title = "Sitio Administrativo de Atención-1"
admin.site.index_title = "Bienvenido al Sitio Administrativo de Atención-1"
