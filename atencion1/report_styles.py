header_style = {
    "fill": {"fill_type": "solid", "start_color": "FFCBEAED",},
    "alignment": {
        "horizontal": "center",
        "vertical": "center",
        "wrapText": True,
        "shrink_to_fit": True,
    },
    "border_side": {"border_style": "thick", "color": "FF333333",},
    "font": {"name": "Arial", "size": 12, "bold": True, "color": "FF006A71",},
}

body_style = {
    "fill": {"fill_type": "solid", "start_color": "FFDDDDDD",},
    "alignment": {
        "horizontal": "center",
        "vertical": "center",
        "wrapText": True,
        "shrink_to_fit": True,
    },
    "border_side": {"border_style": "thin", "color": "FF000000",},
    "font": {"name": "Arial", "size": 10, "bold": False, "color": "FF666666",},
}
