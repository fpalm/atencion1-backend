from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator

from terminologies.models import (
    ICD10Symptom,
    ICD10Diagnose,
    MedicinalProduct,
    TriageQuestionAnswer,
    OptionalTriageQuestionAnswer,
    CallingQuestionnaire,
    TriageQuestion,
    OptionalTriageQuestion,
    QuestionnaireMessage,
)
from .serializers import (
    ICD10SymptomSerializer,
    ICD10DiagnoseSerializer,
    MedicinalProductSerializer,
    TriageQuestionSerializer,
    OptionalTriageQuestionSerializer,
    CallingQuestionnaireSerializer,
    TriageQuestionAnswerSerializer,
    OptionalTriageQuestionAnswerSerializer,
    CallingQuestionnaireSimpleSerializer,
    QuestionnaireMessageSerializer,
)


@method_decorator(cache_page(60*20), name='dispatch')
class ICD10SymptomListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ICD10Symptom.objects.all()
    serializer_class = ICD10SymptomSerializer

    filter_fields = ("term_es",)
    search_fields = ("^term_es",)
    ordering_fields = ("term_es",)


class ICD10SymptomDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ICD10Symptom.objects.all()
    serializer_class = ICD10SymptomSerializer


@method_decorator(cache_page(60*20), name='dispatch')
class ICD10DiagnoseListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ICD10Diagnose.objects.all()
    serializer_class = ICD10DiagnoseSerializer

    filter_fields = ("term_es",)
    search_fields = ("^term_es",)
    ordering_fields = ("term_es",)


class ICD10DiagnoseDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = ICD10Diagnose.objects.all()
    serializer_class = ICD10DiagnoseSerializer


@method_decorator(cache_page(60*20), name='dispatch')
class MedicinalProductListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = MedicinalProduct.objects.all()
    serializer_class = MedicinalProductSerializer

    filter_fields = (
        "form",
        "active_ingredients",
        "manufacturer",
        "product_type",
        "critical_care",
        "acute_care",
        "available"
    )
    search_fields = (
        "^name",
        "active_ingredients__name",
    )


class MedicinalProductDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = MedicinalProduct.objects.all()
    serializer_class = MedicinalProductSerializer


@method_decorator(cache_page(60*20), name='dispatch')
class MedicineOrSupplyListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = MedicinalProduct.objects.is_medicine_or_supply()
    serializer_class = MedicinalProductSerializer

    filter_fields = (
        "form",
        "active_ingredients",
        "manufacturer",
        "product_type",
        "critical_care",
        "acute_care",
        "available"
    )
    search_fields = (
        "^name",
        "active_ingredients__name",
    )


class CallingQuestionnaireListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = CallingQuestionnaire.objects.all()
    serializer_class = CallingQuestionnaireSerializer

    filter_fields = (
        "tmg",
        "emergency_evaluation",
        "triage_questions__id",
        "triage_questions__question_type",
    )
    ordering_fields = (
        "id",
        "answer_date",
    )
    search_fields = (
        "calling_reason",
    )


class CallingQuestionnaireSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = CallingQuestionnaire.objects.all()
    serializer_class = CallingQuestionnaireSimpleSerializer


class CallingQuestionnaireDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = CallingQuestionnaire.objects.all()
    serializer_class = CallingQuestionnaireSerializer


class CallingQuestionnaireSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = CallingQuestionnaire.objects.all()
    serializer_class = CallingQuestionnaireSimpleSerializer


@method_decorator(cache_page(60*20), name='dispatch')
class TriageQuestionListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = TriageQuestion.objects.all()
    serializer_class = TriageQuestionSerializer

    filter_fields = (
        "question_type",
    )
    search_fields = (
        "question_text",
    )


class TriageQuestionDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = TriageQuestion.objects.all()
    serializer_class = TriageQuestionSerializer


@method_decorator(cache_page(60*20), name='dispatch')
class TriageQuestionAnswerListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = TriageQuestionAnswer.objects.all()
    serializer_class = TriageQuestionAnswerSerializer


class TriageQuestionAnswerDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = TriageQuestionAnswer.objects.all()
    serializer_class = TriageQuestionAnswerSerializer


@method_decorator(cache_page(60*20), name='dispatch')
class OptionalTriageQuestionListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = OptionalTriageQuestion.objects.all()
    serializer_class = OptionalTriageQuestionSerializer

    filter_fields = (
        "question_type",
    )
    search_fields = (
        "question_text",
    )


class OptionalTriageQuestionDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = OptionalTriageQuestion.objects.all()
    serializer_class = OptionalTriageQuestionSerializer


@method_decorator(cache_page(60*20), name='dispatch')
class OptionalTriageQuestionAnswerListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = OptionalTriageQuestionAnswer.objects.all()
    serializer_class = OptionalTriageQuestionAnswerSerializer


class OptionalTriageQuestionAnswerDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = OptionalTriageQuestionAnswer.objects.all()
    serializer_class = OptionalTriageQuestionAnswerSerializer


class QuestionnaireMessageListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = QuestionnaireMessage.objects.all()
    serializer_class = QuestionnaireMessageSerializer
