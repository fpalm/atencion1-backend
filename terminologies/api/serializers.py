from rest_framework import serializers

from ..models import (
    ICD10Chapter,
    ICD10Block,
    ICD10Category,
    ICD10Diagnose,
    ICD10Symptom,
    MedicinalProduct,
    TriageQuestion,
    TriageQuestionAnswer,
    OptionalTriageQuestion,
    OptionalTriageQuestionAnswer,
    CallingQuestionnaire,
    QuestionnaireMessage,
)


class MedicinalProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicinalProduct
        fields = "__all__"
        depth = 1


class ICD10ChapterSerializer(serializers.ModelSerializer):
    class Meta:
        model = ICD10Chapter
        fields = "__all__"
        depth = 1


class ICD10BlockSerializer(serializers.ModelSerializer):
    class Meta:
        model = ICD10Block
        fields = "__all__"
        depth = 1


class ICD10CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ICD10Category
        fields = "__all__"
        depth = 1


class ICD10SymptomSerializer(serializers.ModelSerializer):
    class Meta:
        model = ICD10Symptom
        fields = "__all__"
        depth = 1


class ICD10DiagnoseSerializer(serializers.ModelSerializer):
    class Meta:
        model = ICD10Diagnose
        fields = "__all__"
        depth = 1


class TriageQuestionSerializer(serializers.ModelSerializer):
    # question_type = serializers.CharField(source='get_question_type_display')

    class Meta:
        model = TriageQuestion
        fields = "__all__"


class TriageQuestionAnswerSerializer(serializers.ModelSerializer):
    triage_question = TriageQuestionSerializer(read_only=True)

    class Meta:
        model = TriageQuestionAnswer
        fields = "__all__"


class OptionalTriageQuestionSerializer(serializers.ModelSerializer):
    # question_type = serializers.CharField(source='get_question_type_display')

    class Meta:
        model = OptionalTriageQuestion
        fields = "__all__"


class OptionalTriageQuestionAnswerSerializer(serializers.ModelSerializer):
    triage_question = OptionalTriageQuestionSerializer(read_only=True)

    class Meta:
        model = OptionalTriageQuestionAnswer
        fields = "__all__"


class CallingQuestionnaireSerializer(serializers.ModelSerializer):
    triage_questions = TriageQuestionAnswerSerializer(source="triagequestionanswer_set", many=True)
    optional_triage_questions = OptionalTriageQuestionAnswerSerializer(source="optionaltriagequestionanswer_set",
                                                                       many=True)

    class Meta:
        model = CallingQuestionnaire
        fields = "__all__"


class QuestionnaireMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionnaireMessage
        fields = "__all__"


# ----- Simple Serializers -----


class CallingQuestionnaireSimpleSerializer(serializers.ModelSerializer):
    triage_questions = TriageQuestionAnswerSerializer(source="triagequestionanswer_set", many=True, read_only=True)
    optional_triage_questions = OptionalTriageQuestionAnswerSerializer(source="optionaltriagequestionanswer_set",
                                                                       many=True, read_only=True)

    class Meta:
        model = CallingQuestionnaire
        fields = "__all__"

    def create(self, validated_data):
        questionnaire = CallingQuestionnaire.objects.create(**validated_data)
        global_answer = False
        if "triage_questions" in self.initial_data:
            triage_questions = self.initial_data.get("triage_questions")
            for question in triage_questions:
                question_id = question["triage_question"].get("id")
                triage_question = TriageQuestion.objects.get(pk=question_id)
                affirmative_answer = question.get("affirmative_answer")
                if affirmative_answer is True:
                    global_answer = True
                TriageQuestionAnswer(calling_questionnaire=questionnaire, triage_question=triage_question,
                                     affirmative_answer=affirmative_answer).save()
        if "optional_triage_questions" in self.initial_data:
            optional_triage_questions = self.initial_data.get("optional_triage_questions")
            for question in optional_triage_questions:
                question_id = question["triage_question"].get("id")
                optional_triage_question = OptionalTriageQuestion.objects.get(pk=question_id)
                affirmative_answer = question.get("affirmative_answer")
                OptionalTriageQuestionAnswer(calling_questionnaire=questionnaire,
                                             triage_question=optional_triage_question,
                                             affirmative_answer=affirmative_answer).save()
        questionnaire.emergency_evaluation = global_answer
        questionnaire.save()
        return questionnaire

    def update(self, instance, validated_data):
        instance.answer_date = validated_data.get("answer_date", instance.answer_date)
        instance.emergency_evaluation = validated_data.get("emergency_evaluation", instance.emergency_evaluation)
        instance.tmg = validated_data.get("tmg", instance.tmg)
        instance.main_condition = validated_data.get("main_condition", instance.main_condition)
        global_answer = False
        if "triage_questions" in self.initial_data:
            triage_questions = self.initial_data.get("triage_questions")
            instance.triage_questions.clear()
            for question in triage_questions:
                question_id = question["triage_question"].get("id")
                triage_question = TriageQuestion.objects.get(pk=question_id)
                affirmative_answer = question.get("affirmative_answer")
                if affirmative_answer is True:
                    global_answer = True
                TriageQuestionAnswer(calling_questionnaire=instance, triage_question=triage_question,
                                     affirmative_answer=affirmative_answer).save()
        if "optional_triage_questions" in self.initial_data:
            optional_triage_questions = self.initial_data.get("optional_triage_questions")
            instance.optional_triage_questions.clear()
            for question in optional_triage_questions:
                question_id = question["triage_question"].get("id")
                optional_triage_question = OptionalTriageQuestion.objects.get(pk=question_id)
                affirmative_answer = question.get("affirmative_answer")
                OptionalTriageQuestionAnswer(calling_questionnaire=instance, triage_question=optional_triage_question,
                                             affirmative_answer=affirmative_answer).save()
        instance.emergency_evaluation = global_answer
        instance.save()
        return instance
