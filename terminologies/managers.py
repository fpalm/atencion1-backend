from django.db import models
from django.db.models import Q


class MedicinalProductManager(models.Manager):
    """Manager for filter data in Medicinal Product Model
    """

    def is_medicine_or_supply(self):
        """Find all medicinal products that are MEDICINE OR MEDICAL SUPPLY.
        
        Arguments:
            None.
        
        Returns:
            list -- List of medicinal products filtered.
        """
        return self.filter(
            Q(product_type="MEDICINE") | Q(product_type="MEDICAL_SUPPLY")
        )