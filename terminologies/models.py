"""
Modelos relacionados con la terminología médica. Principalmente lo relacionado
con categorización CIE-10, los medicamentos, las preguntas de triaje y los
récipes.
"""

import datetime

import pytz
from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

default_btime = datetime.datetime.strptime("1980-01-01", "%Y-%m-%d")
default_btime = default_btime.replace(tzinfo=pytz.timezone(settings.TIME_ZONE))

from services.models import TMG
from terminologies.managers import MedicinalProductManager



UM_CHOICES = [
    ("kg", _("kg")),
    ("g", _("g")),
    ("mg", _("mg")),
    ("mcg", _("mcg")),
    ("L", _("L")),
    ("mL", _("mL")),
    ("cc", _("cc")),
    ("mol", _("mol")),
    ("mmol", _("mmol")),
    ("iU", _("UI")),
    ("dose", _("dosis")),
]


class ICD10Chapter(models.Model):
    """
    Capítulo CIE-10.
    """

    code = models.CharField(_("código"), max_length=8)
    term_es = models.CharField(_("término"), max_length=64)
    desc_es = models.CharField(_("descripción"), max_length=255)
    block_range = models.CharField(_("rango de bloques"), max_length=8, default="")

    def __str__(self):
        return f"Cap. {self.code}: {self.term_es}"

    class Meta:
        verbose_name = _("Capítulo CIE-10")
        verbose_name_plural = _("Capítulos CIE-10")


class ICD10Block(models.Model):
    """
    Bloque CIE-10.
    """

    code = models.CharField(_("código"), max_length=8)
    term_es = models.CharField(_("término"), max_length=64)
    desc_es = models.CharField(_("descripción"), max_length=255)
    icd10_chapter = models.ForeignKey(
        ICD10Chapter, on_delete=models.CASCADE, verbose_name=_("capítulo")
    )

    def __str__(self):
        return f"{self.code}: {self.term_es}"

    class Meta:
        verbose_name = _("Bloque CIE-10")
        verbose_name_plural = _("Bloques CIE-10")


class ICD10Category(models.Model):
    """
    Categoría CIE-10.
    """

    code = models.CharField(_("código"), max_length=8)
    term_es = models.CharField(_("término"), max_length=64)
    desc_es = models.CharField(_("descripción"), max_length=255)
    icd10_block = models.ForeignKey(
        ICD10Block, on_delete=models.CASCADE, verbose_name=_("bloque")
    )

    def __str__(self):
        return f"{self.code}: {self.term_es}"

    class Meta:
        verbose_name = _("Categoría CIE-10")
        verbose_name_plural = _("Categorías CIE-10")


class ICD10Symptom(models.Model):
    """
    Síntoma CIE-10.
    """

    code = models.CharField(_("código"), max_length=8)
    term_es = models.CharField(_("término"), max_length=64)
    desc_es = models.CharField(_("descripción"), max_length=255)
    icd10_category = models.ForeignKey(
        ICD10Category,
        on_delete=models.CASCADE,
        verbose_name=_("categoría"),
        blank=True,
        null=True,
    )
    icd10_block = models.ForeignKey(
        ICD10Block, on_delete=models.CASCADE, verbose_name=_("bloque")
    )
    icd10_chapter = models.ForeignKey(
        ICD10Chapter, on_delete=models.CASCADE, verbose_name=_("capítulo")
    )

    def __str__(self):
        return f"{self.code}: {self.term_es}"

    class Meta:
        verbose_name = _("Síntoma CIE-10")
        verbose_name_plural = _("Sintomas CIE-10")


class ICD10Diagnose(models.Model):
    """
    Diagnóstico CIE-10.
    """

    code = models.CharField(_("código"), max_length=8)
    term_es = models.CharField(_("término"), max_length=64)
    desc_es = models.CharField(_("descripción"), max_length=255)
    icd10_category = models.ForeignKey(
        ICD10Category,
        on_delete=models.CASCADE,
        verbose_name=_("categoría"),
        blank=True,
        null=True,
    )
    icd10_block = models.ForeignKey(
        ICD10Block, on_delete=models.CASCADE, verbose_name=_("bloque")
    )
    icd10_chapter = models.ForeignKey(
        ICD10Chapter, on_delete=models.CASCADE, verbose_name=_("capítulo")
    )

    def __str__(self):
        return f"{self.code}: {self.term_es}"

    class Meta:
        verbose_name = _("Diagnóstico CIE-10")
        verbose_name_plural = _("Diagnósticos CIE-10")


class Condition(models.Model):
    """
    Condición Médica.
    """

    snomed = models.IntegerField(_("código SNOMED"), blank=True)
    icd10_category = models.ForeignKey(
        ICD10Category, on_delete=models.CASCADE, verbose_name=_("categoría CIE-10")
    )
    name = models.CharField(_("nombre"), max_length=255)
    description = models.TextField(_("descripción"))

    class Meta:
        verbose_name = _("Condición / Síntoma / Diagnóstico")
        verbose_name_plural = _("Condiciones / Síntomas / Diagnósticos")


class ActiveIngredient(models.Model):
    """
    Principio Activo.
    """

    name = models.CharField(_("nombre"), max_length=255)
    description = models.TextField(_("descripción"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Principio Activo")
        verbose_name_plural = _("Principios Activos")


class MedicationForm(models.Model):
    """
    Formas Farmacéuticas.
    """

    name = models.CharField(_("nombre"), max_length=255)
    code = models.CharField(_("código"), max_length=8, blank=True)
    description = models.TextField(_("descripción"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Presentación")
        verbose_name_plural = _("Presentaciones")


class IngredientInProduct(models.Model):
    """
    Ingrediente de Producto Farmacéutico.
    """

    active_ingredient = models.ForeignKey(
        ActiveIngredient, on_delete=models.CASCADE, verbose_name=_("principio activo")
    )
    medicinal_product = models.ForeignKey(
        "MedicinalProduct",
        on_delete=models.CASCADE,
        verbose_name=_("producto medicinal"),
    )

    numerator = models.DecimalField(
        _("numerador"), max_digits=8, decimal_places=3, blank=True, null=True
    )
    numerator_unit = models.CharField(
        _("unidad de medida del numerador"), max_length=8, blank=True, null=True, choices=UM_CHOICES
    )
    denominator = models.DecimalField(
        _("denominador"), max_digits=8, decimal_places=3, blank=True, null=True
    )
    denominator_unit = models.CharField(
        _("unidad de medida del denominador"), max_length=8, blank=True, null=True, choices=UM_CHOICES
    )

    def __str__(self):
        return f"{self.medicinal_product.name}: {self.active_ingredient.name}"

    class Meta:
        verbose_name = _("Principio Activo en Producto Medicinal")
        verbose_name_plural = _("Principios Activos en Productos Medicinales")


class DeliveryUnit(models.Model):
    """
    Unidad de Entrega.
    """

    name = models.CharField(
        _("nombre"),
        max_length=32,
        help_text=_(
            "Unidad mínima divisible en la que puede ser despachado el producto."
        ),
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Unidad de Entrega")
        verbose_name_plural = _("Unidades de Entrega")


class MedicinalProduct(models.Model):
    """
    Producto Farmacéutico.
    """

    PRODUCT_TYPE = (
        ("MEDICINE", _("Medicamento")),  # TODO check value to be set on the model
        ("MEDICAL_EQUIPMENT", _("Equipo médico")),
        ("MEDICAL_SUPPLY", _("Insumo médico")),
    )
    name = models.CharField(_("nombre"), max_length=255)
    description = models.TextField(_("descripción"))
    form = models.ForeignKey(
        MedicationForm,
        on_delete=models.CASCADE,
        related_name="medicinal_products",
        verbose_name=_("presentación"),
        blank=True,
        null=True,
    )
    quantity = models.DecimalField(_("cantidad"), max_digits=8, decimal_places=3)
    quantity_unit = models.CharField(_("unidad de medida"), max_length=32)
    active_ingredients = models.ManyToManyField(
        ActiveIngredient,
        through=IngredientInProduct,
        blank=True,
        verbose_name=_("principios activos"),
    )
    product_type = models.CharField(
        _("tipo de producto farmacéutico"),
        max_length=32,
        choices=PRODUCT_TYPE,
        default="MEDICINE",
    )
    manufacturer = models.ForeignKey(
        "locations.Organization",
        verbose_name=_("fabricante"),
        on_delete=models.CASCADE,
        related_name="medicinal_products",
    )
    critical_care = models.BooleanField(_("atención crítica"), default=False)
    acute_care = models.BooleanField(_("atención aguda"), default=False)
    available = models.BooleanField(_("disponible"), default=True)
    delivery_unit = models.ForeignKey(
        DeliveryUnit,
        on_delete=models.CASCADE,
        related_name="delivery_items",
        blank=True,
        null=True,
        verbose_name=_("unidad de entrega")
    )

    objects = MedicinalProductManager()

    def __str__(self):
        return f"{self.get_product_type_display()}: {self.name}"

    class Meta:
        verbose_name = _("Producto Farmacéutico")
        verbose_name_plural = _("Productos Farmacéuticos")


class PrescriptionItem(models.Model):
    """
    Artículo de Prescripción Médica o Récipe.
    """


    medicinal_product = models.ForeignKey(
        MedicinalProduct, on_delete=models.CASCADE, verbose_name=_("Producto medicinal")
    )
    medical_prescription = models.ForeignKey(
        "MedicalPrescription",
        on_delete=models.CASCADE,
        verbose_name=_("prescripción médica"),
    )
    quantity = models.DecimalField(
        _("cantidad"), max_digits=8, decimal_places=3, blank=True, null=True
    )
    quantity_unit = models.CharField(
        _("unidad de medida"), max_length=8, blank=True, null=True
    )

    def __str__(self):
        return f"{self.medical_prescription.description}: {self.medicinal_product.name}"

    class Meta:
        verbose_name = _("partida de la prescripción")
        verbose_name_plural = _("partidas de las prescripciones")


class MedicalPrescription(models.Model):
    """
    Prescripción Médica o Récipe.
    """

    created = models.DateTimeField(_("creado en"), auto_now_add=True, null=True)
    description = models.TextField(_("descripción"))
    medicinal_products = models.ManyToManyField(
        MedicinalProduct,
        through=PrescriptionItem,
        verbose_name=_("productos medicinales"),
    )

    def __str__(self):
        return f"{self.description}: {self.created:%Y-%m-%d %H:%M}"

    class Meta:
        verbose_name = _("Prescripción Médica")
        verbose_name_plural = _("Prescripciones Médicas")


class TriageQuestion(models.Model):
    """
    Pregunta de Triaje.
    """

    question_text = models.CharField(
        _("pregunta de triage"), max_length=255, blank=False
    )
    description = models.TextField(_("descripción"), blank=True, null=True)
    question_type = models.CharField(
        _("categoría de la pregunta"), max_length=30, blank=False
    )

    def __str__(self):
        return f"{self.question_text}, {self.question_type}"

    class Meta:
        verbose_name = _("Pregunta de Triage")
        verbose_name_plural = _("Preguntas de Triage")


class OptionalTriageQuestion(models.Model):
    """
    Pregunta de Opcional de Triaje.
    """

    question_text = models.CharField(
        _("pregunta de triage"), max_length=255, blank=False
    )
    description = models.TextField(_("descripción"), blank=True, null=True)
    question_type = models.CharField(
        _("categoría de la pregunta"), max_length=30, blank=False
    )

    def __str__(self):
        return f"{self.question_text}, {self.question_type}"

    class Meta:
        verbose_name = _("Pregunta complementaria de triage")
        verbose_name_plural = _("Preguntas complementarias de triage")


class CallingQuestionnaire(models.Model):
    """
    Cuestionario de Llamada.
    """

    tmg = models.ForeignKey(
        TMG,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="calling_questionnaires",
        verbose_name=_("OMT"),
    )
    answer_date = models.DateTimeField(
        _("fecha y hora de la respuesta"), default=timezone.now
    )
    triage_questions = models.ManyToManyField(
        TriageQuestion,
        through="TriageQuestionAnswer",
        related_name="calling_questionnaire",
        verbose_name=_("pregunta de triage"),
    )
    optional_triage_questions = models.ManyToManyField(
        OptionalTriageQuestion,
        through="OptionalTriageQuestionAnswer",
        related_name="calling_questionnaire",
        verbose_name=_("pregunta complementaria de triage"),
    )
    emergency_evaluation = models.BooleanField(
        _("evaluación urgente"),
        default=False,
        help_text=_("Indica si el afiliado requiere evaluación urgente."),
    )
    main_condition = models.CharField(
        _("síntoma/diagnóstico principal"), max_length=50, blank=True, null=True,
    )

    def __str__(self):
        return self.main_condition

    class Meta:
        verbose_name = _("Cuestionario de OMT")
        verbose_name_plural = _("Cuestionarios de OMT")


class TriageQuestionAnswer(models.Model):
    """
    Respuesta a Pregunta de Triaje.
    """

    affirmative_answer = models.BooleanField(
        _("¿Respuesta afirmativa?"),
        default=False,
        help_text=_("Indica si la respuesta fue afirmativa o negativa."),
    )

    calling_questionnaire = models.ForeignKey(
        CallingQuestionnaire, on_delete=models.CASCADE, blank=True, null=True
    )
    triage_question = models.ForeignKey(TriageQuestion, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.calling_questionnaire}: {self.triage_question} {'☑' if self.affirmative_answer else '☒'}"

    class Meta:
        verbose_name = _("Respuesta de pregunta de triage")
        verbose_name_plural = _("Respuestas de pregunta de triage")


class OptionalTriageQuestionAnswer(models.Model):
    """
    Respuesta a Pregunta Opcional de Triaje.
    """

    affirmative_answer = models.BooleanField(
        _("¿Respuesta afirmativa?"),
        default=False,
        help_text=_("Indica si la respuesta fue afirmativa o negativa."),
    )

    calling_questionnaire = models.ForeignKey(
        CallingQuestionnaire, on_delete=models.CASCADE, blank=True, null=True
    )
    triage_question = models.ForeignKey(
        OptionalTriageQuestion, on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.calling_questionnaire}: {self.triage_question} {'☑' if self.affirmative_answer else '☒'}"

    class Meta:
        verbose_name = _("Respuesta de pregunta opcional de triage")
        verbose_name_plural = _("Respuestas de pregunta opcional de triage")


class QuestionnaireMessage(models.Model):
    """
    Mensaje de Cuestionario.
    """

    ICD10_source = models.CharField(_("Origen CIE10"), max_length=500,)
    question_type_target = models.CharField(
        _("Destino Tipo de Pregunta"), max_length=50, unique=True,
    )
    question_type_message = models.CharField(
        _("Mensaje de Tipo de Pregunta"), max_length=50, blank=True, null=True,
    )
    advices = models.TextField(_("Recomendaciones generales"), max_length=500,)
    warning_signs = models.TextField(_("Signos de alarma"), max_length=300,)
    default_message = models.TextField(_("Mensaje por defecto"), max_length=100,)
    emergency_message = models.TextField(_("Mensaje de emergencia"), max_length=100,)

    def __str__(self):
        return f"{self.question_type_message}: {self.ICD10_source}"

    class Meta:
        verbose_name = _("Mensaje de cuestionario")
        verbose_name_plural = _("Mensajes de cuestionario")
