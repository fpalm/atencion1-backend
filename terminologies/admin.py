from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import (
    ICD10Chapter,
    ICD10Block,
    ICD10Category,
    ICD10Symptom,
    ICD10Diagnose,
    Condition,
    ActiveIngredient,
    IngredientInProduct,
    MedicationForm,
    DeliveryUnit,
    MedicinalProduct,
    PrescriptionItem,
    MedicalPrescription,
    TriageQuestion,
    OptionalTriageQuestion,
    CallingQuestionnaire,
    TriageQuestionAnswer,
    OptionalTriageQuestionAnswer,
    QuestionnaireMessage,
)


class ICD10SymptomResource(resources.ModelResource):
    class Meta:
        model = ICD10Symptom


class ICD10DiagnoseResource(resources.ModelResource):
    class Meta:
        model = ICD10Diagnose


class ICD10SymptomAdmin(ImportExportModelAdmin):
    model = ICD10Symptom


class ICD10DiagnoseAdmin(ImportExportModelAdmin):
    model = ICD10Diagnose


class ConditionResource(resources.ModelResource):
    class Meta:
        model = Condition


class ConditionAdmin(ImportExportModelAdmin):
    model = Condition
    fields = (("name", "description"), ("icd10_category", "snomed"))


class ICD10CategoryResource(resources.ModelResource):
    
    class Meta:
        model = ICD10Category


class ConditionInline(admin.TabularInline):
    model = Condition
    extra = 1


class ICD10CategoryAdmin(ImportExportModelAdmin):
    model = ICD10Category
    inlines = (ConditionInline,)


class ICD10BlockResource(resources.ModelResource):

    class Meta:
        model = ICD10Block

class ICD10CategoryInline(admin.TabularInline):
    model = ICD10Category
    extra = 1


class ICD10BlockAdmin(ImportExportModelAdmin):
    model = ICD10Block
    inlines = (ICD10CategoryInline,)


class ICD10ChapterResource(resources.ModelResource):

    class Meta:
        model = ICD10Chapter


class ICD10BlockInline(admin.TabularInline):
    model = ICD10Block
    extra = 1


class ICD10ChapterAdmin(ImportExportModelAdmin):
    model = ICD10Chapter
    inlines = (ICD10BlockInline,)


class MedicationFormResource(resources.ModelResource):

    class Meta:
        model = MedicationForm


class MedicationFormAdmin(ImportExportModelAdmin):
    model = MedicationForm


class DeliveryUnitResource(resources.ModelResource):
    class Meta:
        model = DeliveryUnit


class DeliveryUnitAdmin(ImportExportModelAdmin):
    model = DeliveryUnit


class MedicinalProductResource(resources.ModelResource):

    class Meta:
        model = MedicinalProduct


class ActiveIngredientResource(resources.ModelResource):

    class Meta:
        model = ActiveIngredient

class IngredientInProductInline(admin.TabularInline):
    model = IngredientInProduct
    extra = 1


class ActiveIngredientAdmin(ImportExportModelAdmin):
    model = ActiveIngredient
    inlines = (IngredientInProductInline,)


class IngredientInProductAdmin(ImportExportModelAdmin):
    model = IngredientInProduct


class IngredientInProductResource(resources.ModelResource):
    class Meta:
        model = IngredientInProduct


class PrescriptionItemInline(admin.TabularInline):
    model = PrescriptionItem
    extra = 1


class MedicinalProductAdmin(ImportExportModelAdmin):
    model = MedicinalProduct
    inlines = (IngredientInProductInline, PrescriptionItemInline)
    list_display = ("name", "description", "form", "available")
    list_filter = ("product_type", "critical_care", "acute_care", "available")
    search_fields = ["name", "active_ingredients__name"]


class MedicalPrescriptionAdmin(admin.ModelAdmin):
    model = MedicalPrescription
    inlines = (PrescriptionItemInline,)


class TriageQuestionAdmin(ImportExportModelAdmin):
    model = TriageQuestion


class TriageQuestionResource(resources.ModelResource):
    class Meta:
        model = TriageQuestion


class CallingQuestionnaireAdmin(ImportExportModelAdmin):
    model = CallingQuestionnaire


class OptionalTriageQuestionAdmin(ImportExportModelAdmin):
    model = OptionalTriageQuestion


class OptionalTriageQuestionResource(resources.ModelResource):
    class Meta:
        model = OptionalTriageQuestion


class TriageQuestionAnswerAdmin(ImportExportModelAdmin):
    model = TriageQuestionAnswer


class OptionalTriageQuestionAnswerAdmin(ImportExportModelAdmin):
    model = OptionalTriageQuestionAnswer


class QuestionnaireMessageAdmin(ImportExportModelAdmin):
    model = QuestionnaireMessage


class QuestionnaireMessageResource(resources.ModelResource):
    class Meta:
        model = QuestionnaireMessage

admin.site.register(ICD10Chapter, ICD10ChapterAdmin)
admin.site.register(ICD10Block, ICD10BlockAdmin)
admin.site.register(ICD10Category, ICD10CategoryAdmin)
admin.site.register(ICD10Symptom, ICD10SymptomAdmin)
admin.site.register(ICD10Diagnose, ICD10DiagnoseAdmin)
admin.site.register(Condition, ConditionAdmin)
admin.site.register(ActiveIngredient, ActiveIngredientAdmin)
admin.site.register(IngredientInProduct, IngredientInProductAdmin)
admin.site.register(MedicationForm, MedicationFormAdmin)
admin.site.register(DeliveryUnit, DeliveryUnitAdmin)
admin.site.register(MedicinalProduct, MedicinalProductAdmin)
admin.site.register(PrescriptionItem)
admin.site.register(MedicalPrescription, MedicalPrescriptionAdmin)
admin.site.register(TriageQuestion, TriageQuestionAdmin)
admin.site.register(OptionalTriageQuestion, OptionalTriageQuestionAdmin)
admin.site.register(CallingQuestionnaire, CallingQuestionnaireAdmin)
admin.site.register(TriageQuestionAnswer, TriageQuestionAnswerAdmin)
admin.site.register(OptionalTriageQuestionAnswer, OptionalTriageQuestionAdmin)
admin.site.register(QuestionnaireMessage, QuestionnaireMessageAdmin)
