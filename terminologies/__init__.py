"""
Modelos, vistas y serializadores con la estructura de síntomas, diagnósticos,
productos médicos y el cuestionario de criterios de emergencia diseñado por
Venemergencia. Esta estructura está inspirada en el sistema CIE-10.
"""

default_app_config = 'terminologies.apps.TerminologiesConfig'
