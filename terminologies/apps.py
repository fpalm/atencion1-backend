from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class TerminologiesConfig(AppConfig):
    name = 'terminologies'
    verbose_name = _("terminologías")
