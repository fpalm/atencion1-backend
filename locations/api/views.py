from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator

from locations.models import (
    Country,
    AdminFirstLevel,
    AdminSecondLevel,
    AdminThirdLevel,
    OrganizationType,
    Organization,
    OrganizationAddress,
)
from .serializers import (
    CountrySerializer,
    AdminFirstLevelMinimalSerializer,
    AdminSecondLevelMinimalSerializer,
    AdminThirdLevelMinimalSerializer,
    OrganizationTypeSerializer,
    OrganizationSerializer,
    OrganizationSimpleSerializer,
    OrganizationAddressSerializer,
)


class CountryListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Country.objects.all()
    serializer_class = CountrySerializer

    filter_fields = (
        "name",
        "iso3166_2",
    )


@method_decorator(cache_page(60*20), name='dispatch')
class AdminFirstLevelView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AdminFirstLevel.objects.all()
    serializer_class = AdminFirstLevelMinimalSerializer

    filter_fields = (
        "name",
        "code",
        "country",
    )
    ordering_fields = (
        "name",
        "country",
    )


@method_decorator(cache_page(60*20), name='dispatch')
class AdminSecondLevelView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AdminSecondLevel.objects.all()
    serializer_class = AdminSecondLevelMinimalSerializer

    filter_fields = (
        "name",
        "code",
        "first_level",
    )
    ordering_fields = (
        "name",
        "first_level",
    )


@method_decorator(cache_page(60*20), name='dispatch')
class AdminThirdLevelView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AdminThirdLevel.objects.all()
    serializer_class = AdminThirdLevelMinimalSerializer

    filter_fields = (
        "name",
        "code",
        "second_level",
    )
    ordering_fields = (
        "name",
        "second_level",
    )


class OrganizationTypeListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = OrganizationType.objects.all()
    serializer_class = OrganizationTypeSerializer

    search_fields = (
        "^code",
        "^name",
    )


class OrganizationTypeDetailView(generics.RetrieveAPIView):
    class OrganizationTypeListView(generics.ListAPIView):
        permission_classes = (IsAuthenticated,)
        queryset = OrganizationType.objects.all()
        serializer_class = OrganizationTypeSerializer


class OrganizationListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer

    filter_fields = (
        "organization_type",
        "code",
        "has_plans",
    )
    ordering_fields = (
        "name",
        "id",
    )
    search_fields = (
        "^name",
        "^code",
        "tax_id",
    )


class OrganizationDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer


class OrganizationSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Organization.objects.all()
    serializer_class = OrganizationSimpleSerializer


class OrganizationSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Organization.objects.all()
    serializer_class = OrganizationSimpleSerializer


class OrganizationAddressListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = OrganizationAddress.objects.all()
    serializer_class = OrganizationAddressSerializer

    filter_fields = (
        "organization",
        "organization__organization_type",
        "place",
        "region",
        "last_update_user",
    )
    ordering_fields = (
        "id",
        "last_update_time",
    )
    search_fields = (
        "address_line1",
        "address_line2",
        "^zip_code",
    )


class OrganizationAddressDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = OrganizationAddress.objects.all()
    serializer_class = OrganizationAddressSerializer
