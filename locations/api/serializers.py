from rest_framework import serializers

from locations.models import (
    Country,
    AdminFirstLevel,
    AdminSecondLevel,
    AdminThirdLevel,
    OrganizationType,
    Organization,
    OrganizationAddress,
    Place,
)


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = (
            "id",
            "name",
            "iso3166_2",
        )


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = "__all__"
        depth = 2


class AdminThirdLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminThirdLevel
        fields = "__all__"
        depth = 3


class AdminThirdLevelMinimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminThirdLevel
        fields = (
            "id",
            "name",
            "second_level",
        )


class AdminSecondLevelMinimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminSecondLevel
        fields = (
            "id",
            "name",
            "first_level",
        )


class AdminFirstLevelMinimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminFirstLevel
        fields = (
            "id",
            "name",
            "country",
        )


class OrganizationTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrganizationType
        fields = "__all__"


class OrganizationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = "__all__"
        depth = 1


class OrganizationSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = "__all__"


class OrganizationAddressSerializer(serializers.ModelSerializer):
    region = AdminThirdLevelSerializer()
    organization = OrganizationSerializer()

    class Meta:
        model = OrganizationAddress
        fields = "__all__"
