from django.urls import path

from . import views

app_name = 'locations'

urlpatterns = [
    path(
        'country',
        views.CountryListView.as_view(),
        name='country_list'
    ),
    path(
        'admin-first-level',
        views.AdminFirstLevelView.as_view(),
        name='admin_first_level_list'
    ),
    path(
        'admin-second-level',
        views.AdminSecondLevelView.as_view(),
        name='admin_second_level_list'
    ),
    path(
        'admin-third-level',
        views.AdminThirdLevelView.as_view(),
        name='admin_third_level_list'
    ),
    path(
        'organization_types',
        views.OrganizationTypeListView.as_view(),
        name='organization_type_list'
    ),
    path(
        'organization_types/<pk>',
        views.OrganizationTypeDetailView.as_view(),
        name='organization_type_detail'
    ),
    path(
        'organizations',
        views.OrganizationListView.as_view(),
        name='organization_list'
    ),
    path(
        'organizations/<pk>',
        views.OrganizationDetailView.as_view(),
        name='organization_detail'
    ),
    path(
        'organizations-simple',
        views.OrganizationSimpleListView.as_view(),
        name='organization_simple_list'
    ),
    path(
        'organizations-simple/<pk>',
        views.OrganizationSimpleDetailView.as_view(),
        name='organization_simple_detail'
    ),
    path(
        'organization-addresses',
        views.OrganizationAddressListView.as_view(),
        name='organization_address_list'
    ), path(
        'organization-addresses/<pk>',
        views.OrganizationAddressDetailView.as_view(),
        name='organization_address_detail'
    ),
]
