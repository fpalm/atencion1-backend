from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import (
    AdminFirstLevel,
    AdminSecondLevel,
    AdminThirdLevel,
    Country,
    Place,
    Location,
    LocationAddress,
    LocationEmail,
    LocationPhoneNumber,
    OrganizationType,
    Organization,
    OrganizationAddress,
)


class AdminThirdLevelResource(resources.ModelResource):
    
    class Meta:
        model = AdminThirdLevel


class AdminThirdLevelAdmin(ImportExportModelAdmin):
    model = AdminThirdLevel


class AdminThirdLevelInline(admin.TabularInline):
    model = AdminThirdLevel
    extra = 1


class AdminSecondLevelAdmin(ImportExportModelAdmin):
    model = AdminSecondLevel
    inlines = (AdminThirdLevelInline,)



class AdminSecondLevelResource(resources.ModelResource):
    
    class Meta:
        model = AdminSecondLevel


class AdminSecondLevelInline(admin.TabularInline):
    model = AdminSecondLevel
    extra = 1


class AdminFirstLevelAdmin(ImportExportModelAdmin):
    model = AdminFirstLevel
    inlines = (AdminSecondLevelInline,)


class AdminFirstLevelResource(resources.ModelResource):
    
    class Meta:
        model = AdminFirstLevel


class AdminFirstLevelInline(admin.TabularInline):
    model = AdminFirstLevel
    extra = 1


class CountryResource(resources.ModelResource):
    
    class Meta:
        model = Country


class CountryAdmin(ImportExportModelAdmin):
    model = Country
    inlines = (AdminFirstLevelInline,)


class PlaceResource(resources.ModelResource):
    
    class Meta:
        model = Place


class PlaceAdmin(ImportExportModelAdmin):
    model = Place


class LocationAddressResource(resources.ModelResource):
    
    class Meta:
        model = LocationAddress


class LocationAddressInline(admin.TabularInline):
    model = LocationAddress
    extra = 1


class LocationEmailResource(resources.ModelResource):
    
    class Meta:
        model = LocationEmail


class LocationEmailInline(admin.TabularInline):
    model = LocationEmail
    extra = 1


class LocationPhoneNumberResource(resources.ModelResource):
    
    class Meta:
        model = LocationPhoneNumber


class LocationPhoneNumberInline(admin.TabularInline):
    model = LocationPhoneNumber
    extra = 1


class LocationResource(resources.ModelResource):
    
    class Meta:
        model = Location


class LocationAdmin(ImportExportModelAdmin):
    model = Location
    inlines = (LocationEmailInline, LocationPhoneNumberInline, LocationAddressInline)


class LocationInline(admin.TabularInline):
    model = Location
    extra = 1


class OrganizationTypeResource(resources.ModelResource):
    
    class Meta:
        model = OrganizationType


class OrganizationTypeAdmin(ImportExportModelAdmin):
    model = OrganizationType


class OrganizationAddressResource(resources.ModelResource):
    
    class Meta:
        model = OrganizationAddress


class OrganizationAddressInline(admin.TabularInline):
    model = OrganizationAddress
    extra = 1


class OrganizationResource(resources.ModelResource):
    
    class Meta:
        model = Organization

class OrganizationAdmin(ImportExportModelAdmin):
    model = Organization
    inlines = (LocationInline, OrganizationAddressInline)
    list_display = ("name", "code", "tax_id")
    list_filter = ("has_plans", "organization_type")
    search_fields = [
        "name", "code", "tax_id"
    ]

    def service_attention_id(self, obj):
        return obj.attention.id
    service_attention_id.short_description = "Atención"

    def service_id(self, obj):
        return obj.id
    service_id.short_description = "PHD"

    def affiliate(self, obj):
        return str(obj.attention.affiliate)
    affiliate.short_description = "Afiliado"

admin.site.register(AdminThirdLevel, AdminThirdLevelAdmin)
admin.site.register(AdminSecondLevel, AdminSecondLevelAdmin)
admin.site.register(AdminFirstLevel, AdminFirstLevelAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Place, PlaceAdmin)
admin.site.register(LocationAddress)
admin.site.register(LocationEmail)
admin.site.register(LocationPhoneNumber)
admin.site.register(Location, LocationAdmin)
admin.site.register(OrganizationAddress)
admin.site.register(OrganizationType, OrganizationTypeAdmin)
admin.site.register(Organization, OrganizationAdmin)
