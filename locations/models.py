"""
Modelos relacionados con las divisiones político-administrativas, lugares,
ubicaciones y organizaciones.
"""

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext as _
from django.core.validators import RegexValidator

from phonenumber_field.modelfields import PhoneNumberField


class Country(models.Model):
    """
    País.
    """

    name = models.CharField(_("nombre"), max_length=255, unique=True)
    iso3166_2 = models.CharField(_("código"), max_length=2, unique=True)
    geonameid = models.IntegerField(_("geoname id"), unique=True, default=0)

    def __str__(self):
        return f"{self.name}, {self.iso3166_2}"

    class Meta:
        verbose_name = _("País")
        verbose_name_plural = _("Países")
        ordering = ("name",)


class AdminFirstLevel(models.Model):
    """
    Primer nivel administrativo.
    """

    name = models.CharField(_("nombre"), max_length=255, blank=True)
    code = models.CharField(_("código"), max_length=8, blank=True)
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE, related_name="adm1", verbose_name=_("país")
    )

    def __str__(self):
        return f"{self.name}, {self.country.name}"

    class Meta:
        verbose_name = _("Estado")
        verbose_name_plural = _("Estados")
        unique_together = ("name", "country")
        ordering = ("country", "name")


class AdminSecondLevel(models.Model):
    """
    Segundo nivel administrativo.
    """

    name = models.CharField(_("nombre"), max_length=255, blank=True)
    code = models.CharField(_("código"), max_length=8, blank=True)
    first_level = models.ForeignKey(
        AdminFirstLevel,
        on_delete=models.CASCADE,
        related_name="adm2",
        verbose_name=_("estado"),
    )

    def __str__(self):
        return f"{self.name}, {self.first_level.name}"

    class Meta:
        verbose_name = _("Municipio")
        verbose_name_plural = _("Municipios")
        unique_together = ("name", "first_level")
        ordering = ("first_level", "name")


class AdminThirdLevel(models.Model):
    """
    Tercer nivel administrativo.
    """

    name = models.CharField(_("nombre"), max_length=255, blank=True)
    code = models.CharField(_("código"), max_length=8, blank=True)
    second_level = models.ForeignKey(
        AdminSecondLevel,
        on_delete=models.CASCADE,
        related_name="adm3",
        verbose_name=_("municipio"),
    )

    def __str__(self):
        return f"{self.name}, {self.second_level.name},  {self.second_level.first_level.name}"

    class Meta:
        verbose_name = _("Parroquia")
        verbose_name_plural = _("Parroquias")
        unique_together = ("name", "second_level")
        ordering = ("second_level", "name")


class Place(models.Model):
    """
    Lugar.
    """

    name = models.CharField(_("nombre"), max_length=255, blank=True)
    code = models.CharField(_("código"), max_length=8, blank=True)
    lat = models.DecimalField(
        verbose_name=_("latitud"), blank=True, max_digits=9, decimal_places=6
    )
    lon = models.DecimalField(
        verbose_name=_("longitud"), blank=True, max_digits=9, decimal_places=6
    )
    state = models.ForeignKey(
        AdminFirstLevel,
        on_delete=models.CASCADE,
        related_name="places",
        verbose_name=_("estado"),
    )

    def __str__(self):
        return f"{self.name or self.code}"

    class Meta:
        verbose_name = _("Lugar")
        verbose_name_plural = _("Lugares")


class Address(models.Model):
    """
    Dirección.
    """

    address_line1 = models.CharField(_("dirección 1"), max_length=512)
    address_line2 = models.CharField(_("dirección 2"), max_length=512, blank=True)
    place = models.ForeignKey(
        Place,
        on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_addresses",
        null=True,
        blank=True,
        verbose_name=_("lugar"),
    )
    region = models.ForeignKey(
        AdminThirdLevel,
        on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_addresses",
        null=True,
        blank=True,
        verbose_name=_("parroquia"),
    )
    zip_code = models.CharField(
        _("código postal"), max_length=12, blank=True, null=True
    )
    last_update_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_addresses",
        verbose_name=_("usuario de la última actualización"),
    )
    last_update_time = models.DateTimeField(
        _("fecha de la última actualización"), auto_now=True
    )
    unknown_region = models.BooleanField(
        _("parroquia desconocida"), default=False
    )

    class Meta:
        verbose_name = _("Dirección")
        verbose_name_plural = _("Direcciones")
        abstract = True


class OrganizationType(models.Model):
    """
    Tipo de Organización.
    """

    name = models.CharField(_("nombre"), max_length=60)
    code = models.CharField(_("código"), max_length=16)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("tipo de organización")
        verbose_name_plural = _("tipos de organizaciones")


class Organization(models.Model):
    """
    Organización.
    """

    name = models.CharField(_("nombre"), max_length=200)
    code = models.CharField(_("código"), max_length=16, blank=True)
    organization_type = models.ForeignKey(
        OrganizationType,
        on_delete=models.CASCADE,
        verbose_name=_("tipo de organización"),
    )
    tax_id = models.CharField(
        _("RIF"),
        max_length=16,
        blank=False,
        null=True,
        unique=True,
        default=None,
        validators=[
            RegexValidator(
                regex="^[V|E|P|J|G|C]\d{6,9}$", message="RIF no válido",
            ),
        ],
    )
    has_plans = models.BooleanField(_("tiene planes"), default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Organización")
        verbose_name_plural = _("Organizaciones")


class OrganizationAddress(Address):
    """
    Dirección de Organización.
    """

    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        related_name="organization_addresses",
        verbose_name=_("organización"),
    )

    def __str__(self):
        return f"{self.organization.name}, {self.address_line1}, {self.region.name}"

    class Meta:
        verbose_name = _("Dirección de Organización")
        verbose_name_plural = _("Direcciones de Organizaciones")


class Location(models.Model):
    """
    Ubicación.
    """

    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        related_name="locations",
        verbose_name=_("organización"),
    )
    name = models.CharField(_("nombre"), max_length=200)

    class Meta:
        verbose_name = _("Sede de Organización")
        verbose_name_plural = _("Sedes de Organizaciones")


class LocationAddress(Address):
    """
    Dirección de Ubicación.
    """

    Location = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        related_name="location_addresses",
        verbose_name=_("sede"),
    )

    def __str__(self):
        return f"{self.Location.name}, {self.place.name}"

    class Meta:
        verbose_name = _("Dirección de Sede")
        verbose_name_plural = _("Direcciones de Sedes")


class LocationEmail(models.Model):
    """
    Correos electrónicos en ubicación.
    """

    location = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        related_name="location_emails",
        verbose_name=_("sede"),
    )
    email = models.EmailField(_("correo electrónico"))
    primary = models.BooleanField(_("principal"))

    class Meta:
        verbose_name = _("Email de Sede")
        verbose_name_plural = _("Emails de Sedes")


class LocationPhoneNumber(models.Model):
    """
    Número telefónico en ubicación.
    """

    location = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        related_name="location_phone_numbers",
        verbose_name=_("sede"),
    )
    phone_number = PhoneNumberField(_("número de teléfono"))
    primary = models.BooleanField(_("principal"))

    class Meta:
        verbose_name = _("Teléfono de Sede")
        verbose_name_plural = _("Teléfonos de Sedes")
