"""
Modelos, vistas y serializadores relacionados con ubicaciones geográficas,
direcciones y organizaciones diversas. Las ubicaciones geográficas siguen una
estructura país > estado > municipio > parroquia.
"""

default_app_config = 'locations.apps.LocationsConfig'
