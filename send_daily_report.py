import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "atencion1.settings")
import django

django.setup()

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.db.models import Q
import datetime
from django.utils import timezone
from django.shortcuts import render
from weasyprint import HTML, CSS
import pandas as pd
from itertools import chain

from attentions.models import Attention
from affiliates.models import AffiliateAddress


EMAIL_TO = ["yurely.camacho@gmail.com", "lino.urdaneta@gmail.com"]


def day_before(dt: datetime) -> datetime:
    y = dt - datetime.timedelta(days=1)
    return y.replace(hour=23, minute=59, second=59, microsecond=999999)


def local_tz(dt: datetime, hours: int) -> datetime:
    h = abs(hours)
    if hours > 0:
        return dt + datetime.timedelta(hours=h)
    elif hours < 0:
        return dt - datetime.timedelta(hours=h)
    else:
        return dt


def add_fields(fields: dict) -> dict:
    output_dict = dict()
    for f in fields.items():
        try:
            output_dict[f[0]] = eval(f[1])
        except:
            output_dict[f[0]] = None
    return output_dict


def service2row(service_list: list, attention, service_name: str):
    services = list()
    global serv
    for serv in service_list:
        serv_row = {
            "serv_type": service_name,
            "serv_id": serv.id,
            "start_d": local_tz(serv.start, local_time).strftime("%d/%m/%Y"),
            "start_t": local_tz(serv.start, local_time).strftime("%H:%M:%S"),
            "call_start_d": None,
            "call_start_t": None,
            "call_end_d": None,
            "call_end_t": None,
            "status": serv.status,
            "cancellation": serv.cancellation_reason,
            "scheduled": serv.scheduled,
            "scheduled_d": None,
            "scheduled_t": None,
            "imputable": serv.imputable,
            "attendant": f"{serv.attendant.first_name} {serv.attendant.last_name}" if serv.attendant else "",
            "prior_service": None,
            "policy_id": serv.policy.id,
            "plan": serv.policy.plan.name,
            "organization": serv.policy.plan.organization.name,
            "private_plan": serv.policy.plan.private,
            "sponsor": serv.policy.sponsor.name,
            "policy_type": serv.policy.policy_type,
            "symptoms": [symp.id for symp in serv.symptoms.all()],
            "diagnoses": [diag.id for diag in serv.diagnoses.all()],
            "crew": None,
            "crew_start_d": None,
            "crew_start_t": None,
            "crew_end_d": None,
            "crew_end_t": None,
            "mob_unit_type": None,
            "mob_unit_code": None,
            "base": None,
            "dispatch_d": None,
            "dispacth_t": None,
            "base_dep_d": None,
            "base_dep_t": None,
            "base_arriv_d": None,
            "base_arriv_t": None,
        }
        serv_row.update(
            add_fields(
                {
                    "call_start_d": "local_tz(serv.call_start, local_time).strftime('%d/%m/%Y')",
                    "call_start_t": "local_tz(serv.call_start, local_time).strftime('%H:%M:%S')",
                    "call_end_d": "local_tz(serv.call_end, local_time).strftime('%d/%m/%Y')",
                    "call_end_t": "local_tz(serv.call_end, local_time).strftime('%H:%M:%S')",
                    "pediatric": "serv.is_pediatric",
                }
            )
        )
        serv_row.update(
            add_fields(
                {
                    "scheduled_d": "local_tz(serv.scheduled_date_time, local_time).strftime('%d/%m/%Y')",
                    "scheduled_t": "local_tz(serv.scheduled_date_time, local_time).strftime('%H:%M:%S')",
                }
            )
        )
        serv_row.update(add_fields({"prior_service": "serv.prior_service_type.code"}))
        serv_row.update(
            add_fields(
                {
                    "crew": "serv.crew.name",
                    "crew_start_d": "local_tz(serv.crew.start, local_time).strftime('%d/%m/%Y')",
                    "crew_start_t": "local_tz(serv.crew.start, local_time).strftime('%H:%M:%S')",
                    "crew_end_d": "local_tz(serv.crew.end, local_time).strftime('%d/%m/%Y')",
                    "crew_end_t": "local_tz(serv.crew.end, local_time).strftime('%H:%M:%S')",
                    "mob_unit_type": "serv.crew.mobile_unit.unit_type",
                    "mob_unit_code": "serv.crew.mobile_unit.codename",
                    "base": "serv.crew.base.base_name",
                    "dispatch_d": "local_tz(serv.dispatch_datetime, local_time).strftime('%d/%m/%Y')",
                    "dispacth_t": "local_tz(serv.dispatch_datetime, local_time).strftime('%H:%M:%S')",
                    "base_dep_d": "local_tz(serv.base_departure_datetime, local_time).strftime('%d/%m/%Y')",
                    "base_dep_t": "local_tz(serv.base_departure_datetime, local_time).strftime('%H:%M:%S')",
                    "base_arriv_d": "local_tz(serv.base_arrival_datetime, local_time).strftime('%d/%m/%Y')",
                    "base_arriv_t": "local_tz(serv.base_arrival_datetime, local_time).strftime('%H:%M:%S')",
                    "place": "serv.destination_address.place.name",
                    "region": "serv.destination_address.region.name",
                    "state": "serv.destination_address.region.second_level.first_level.name",
                    "country": "serv.destination_address.region.second_level.first_level.country.name",
                    "aff_state": "AffiliateAddress.objects.filter(Affiliate_id=1, AddressType='HOME')[0].region.second_level.first_level.name",
                }
            )
        )
        serv_row.update(
            add_fields(
                {
                    "ori_arriv_d": "local_tz(serv.origin_arrival_datetime, local_time).strftime('%d/%m/%Y')",
                    "ori_arriv_t": "local_tz(serv.origin_arrival_datetime, local_time).strftime('%H:%M:%S')",
                    "ori_depar_d": "local_tz(serv.origin_departure_datetime, local_time).strftime('%d/%m/%Y')",
                    "ori_depar_t": "local_tz(serv.origin_departure_datetime, local_time).strftime('%H:%M:%S')",
                    "ori_arriv2_d": "local_tz(serv.origin_second_arrival_datetime, local_time).strftime('%d/%m/%Y')",
                    "ori_arriv2_t": "local_tz(serv.origin_second_arrival_datetime, local_time).strftime('%H:%M:%S')",
                    "ori_depar2_d": "local_tz(serv.origin_second_departure_datetime, local_time).strftime('%d/%m/%Y')",
                    "ori_depar2_t": "local_tz(serv.origin_second_departure_datetime, local_time).strftime('%H:%M:%S')",
                    "emergency": "serv.emergency",
                    "simp_trip": "serv.simple_trip",
                    "interinst": "serv.interinstitutional",
                    "interstate": "serv.interstate",
                    "icu": "serv.icu",
                    "covered": "serv.covered",
                    "lab_exam": "serv.lab_examination",
                    "critical_inv": "serv.critical_inventory",
                    "acute_inv": "serv.acute_inventory",
                    "equip_deliver": "serv.medical_equipment_delivery",
                    "private_pract": "serv.from_private_practitioner",
                }
            )
        )
        serv_row.update(attention)
        services.append(serv_row.copy())
    return services


def attention2table(att_obj):
    attentions = list()
    for att in att_obj:
        att_row = {
            "att_id": att.id,
            "att_reason": att.reason_for_care,
            "att_start_d": local_tz(att.created, local_time).strftime("%d/%m/%Y"),
            "att_start_t": local_tz(att.created, local_time).strftime("%H:%M:%S"),
            "att_mod_d": local_tz(att.last_modified, local_time).strftime("%d/%m/%Y"),
            "att_mod_t": local_tz(att.last_modified, local_time).strftime("%H:%M:%S"),
            "att_status": att.status,
            "att_follow_up": att.follow_up,
            "att_closing": att.closing_reason,
        }
        if att.services_hhc_attention.all():
            attentions.append(
                service2row(att.services_hhc_attention.all(), att_row, "AMD")
            )
        if att.services_lab_attention.all():
            attentions.append(
                service2row(att.services_lab_attention.all(), att_row, "LAB")
            )
        if att.services_transfer_attention.all():
            attentions.append(
                service2row(att.services_transfer_attention.all(), att_row, "TLD")
            )
        if att.services_hhp_attention.all():
            attentions.append(
                service2row(att.services_hhp_attention.all(), att_row, "PHD")
            )
        if att.services_hmd_attention.all():
            attentions.append(
                service2row(att.services_hmd_attention.all(), att_row, "EMD")
            )
        if att.services_tmg_attention.all():
            attentions.append(
                service2row(att.services_tmg_attention.all(), att_row, "OMT")
            )
    return attentions


# ----- Create table and export as xlsx -----

local_time = -4 # Caracas TZ.
yesterday = local_tz(day_before(timezone.now()), local_time)
two_days_ago = yesterday - datetime.timedelta(days=1)

att = Attention.objects.filter(
    (
        Q(services_hhc_attention__end__lt=yesterday)
        & Q(services_hhc_attention__end__gt=two_days_ago)
    )
    | (
        Q(services_lab_attention__end__lt=yesterday)
        & Q(services_lab_attention__end__gt=two_days_ago)
    )
    | (
        Q(services_transfer_attention__end__lt=yesterday)
        & Q(services_transfer_attention__end__gt=two_days_ago)
    )
    | (
        Q(services_hhp_attention__end__lt=yesterday)
        & Q(services_hhp_attention__end__gt=two_days_ago)
    )
    | (
        Q(services_hmd_attention__end__lt=yesterday)
        & Q(services_hmd_attention__end__gt=two_days_ago)
    )
    | (
        Q(services_tmg_attention__end__lt=yesterday)
        & Q(services_tmg_attention__end__gt=two_days_ago)
    )
)

# att = Attention.objects.all()  # TODO: Delete or comment this line
data = attention2table(att)
df = pd.DataFrame(list(chain.from_iterable(data)))

excel_filename = f"""Informe-{yesterday.strftime("%d-%m-%Y")}.xlsx"""
df.to_excel(excel_filename)
df.to_csv(excel_filename + ".csv")

# ----- Send email -----

subject, from_email, to = (
    "Informe diario de servicios de Atención1 (documentos adjuntos)",
    settings.EMAIL_HOST_USER,
    EMAIL_TO,
)
text_content = (
    f"Informe diario de servicios de Atención1. Se adjunta archivo {excel_filename}."
)
msg = EmailMultiAlternatives(subject, text_content, from_email, to,)
msg.attach_file(excel_filename)
msg.attach_file(excel_filename + ".csv")
msg.send()

# ----- Delete xlsx temporal file  -----

os.remove(excel_filename)
