"""
Modelos relacionados con los datos de los afiliados.
"""
import datetime

import pytz
from django.conf import settings
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from locations.models import Address
from policies.models import Policy

default_btime = datetime.datetime.strptime("1980-01-01", "%Y-%m-%d")
default_btime = default_btime.replace(tzinfo=pytz.timezone(settings.TIME_ZONE))


class Affiliate(models.Model):
    """
    
    **Afiliado**.

    Datos de las personas que reciben atenciones. Pueden ser afiliados a una
    póliza de seguros o particulares que se consideran afiliados a la póliza
    "autofinanciado".

    """

    GENDER = (
        ("FEMALE", _("Femenino")),
        ("MALE", _("Masculino")),
    )
    policies = models.ManyToManyField(
        Policy,
        related_name="affiliates",
        verbose_name=_("pólizas"),
        blank=True,
        through="AffiliateKinship",
    )
    first_name = models.CharField(_("nombres"), max_length=150)
    last_name = models.CharField(_("apellidos"), max_length=150)
    gender = models.CharField(
        _("género"), max_length=20, default="UNKNOWN", choices=GENDER
    )
    birthdate = models.DateField(
        _("fecha de nacimiento"), default=default_btime, blank=True
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Indica si este usuario debe ser tratado como activo. "
            "Deselecciona esto en lugar de eliminar cuentas."
        ),
    )
    date_joined = models.DateTimeField(_("fecha de ingreso"), default=timezone.now)
    dni = models.CharField(
        _("documento de identidad"),
        max_length=50,
        blank=False,
        null=True,
        default=None,
        unique=True,
        validators=[
            RegexValidator(
                regex="^[V|E|P]\d{6,9}([#]\d{1,2})?$", message="Documento de identidad no válido.",
            ),
        ],
    )
    from_validator = models.BooleanField(
        _("proviene del validador"),
        default=True,
        help_text=_("Indica si el afiliado registrado proviene del validador."),
    )
    registered_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name=_("registrado por"),
    )

    def __str__(self):
        return f"{self.first_name}, {self.last_name}: {self.dni}"

    class Meta:
        verbose_name = _("Afiliado")
        verbose_name_plural = _("Afiliados")
        ordering = ("-date_joined",)


class AffiliateKinship(models.Model):
    """
    **Parentesco del Afiliado**.

    Es la tabla intermedia entre los afiliados y las pólizas, se utiliza para
    indicar el parentesco del Afiliado en la póliza. Cada afiliado puede tener
    varias pólizas y en cada póliza tiene un parentesco diferente.

    """

    affiliate = models.ForeignKey(
        Affiliate,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name=_("afiliado"),
    )
    policy = models.ForeignKey(
        Policy, on_delete=models.CASCADE, verbose_name=_("póliza")
    )

    kinship = models.CharField(
        _("parentesco del afiliado"), max_length=15, blank=True, null=True
    )

    def __str__(self):
        return f"Afiliado: {self.affiliate.first_name} {self.affiliate.last_name} / Poliza:{self.policy.code} / Relación: {self.kinship}"

    class Meta:
        verbose_name = _("Parentesco del Afiliado")
        verbose_name_plural = _("Parentescos de los Afiliados")
        unique_together = [
            ('policy', 'affiliate'),
        ]
        indexes = [
            models.Index(fields=['policy', 'affiliate']),
        ]


class AffiliateEmail(models.Model):
    """
    **Correo electrónico del Afiliado**
    """

    affiliate = models.ForeignKey(
        Affiliate,
        on_delete=models.CASCADE,
        related_name="emails",
        blank=True,
        null=True,
    )
    email = models.EmailField(_("correo electrónico"))
    primary = models.BooleanField(_("principal"))

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _("Email de Afiliado")
        verbose_name_plural = _("Emails de Afiliados")


class AffiliatePhoneNumber(models.Model):
    """
    **Número telefónico del afiliado**
    """

    affiliate = models.ForeignKey(
        Affiliate,
        on_delete=models.CASCADE,
        related_name="phone_numbers",
        blank=True,
        null=True,
    )
    phone_number = PhoneNumberField(_("número de teléfono"))
    primary = models.BooleanField(_("principal"), default=True)

    def __str__(self):
        return str(self.phone_number)

    class Meta:
        verbose_name = _("Teléfono de Afiliado")
        verbose_name_plural = _("Teléfonos de Afiliados")


class AffiliateAddress(Address):
    """
    **Dirección del Afiliado**
    """

    ADDRESS_USE = (
        ("HOME", _("Domicilio")),
        ("WORK", _("Trabajo")),
        ("TEMP", _("Temporal")),
        ("OLD", _("Antigüo / Incorrecto")),
        ("BILLING", _("Facturación")),
    )
    AddressType = models.CharField(
        _("tipo de dirección"), max_length=10, choices=ADDRESS_USE
    )
    Affiliate = models.ForeignKey(  # ¿Alguna razón para la mayúscula inicial?
        Affiliate,
        on_delete=models.CASCADE,
        related_name="affiliate_addresses",
        blank=True,
        null=True,
    )

    def __str__(self):
        return "{0}, {1}".format(self.Affiliate.dni, self.AddressType)

    class Meta:
        verbose_name = _("Dirección de Afiliado")
        verbose_name_plural = _("Direcciones de Afiliados")
