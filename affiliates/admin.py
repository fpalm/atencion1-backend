from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import (
    Affiliate,
    AffiliateKinship,
    AffiliateEmail,
    AffiliatePhoneNumber,
    AffiliateAddress,
)


class AffiliateKinshipResource(resources.ModelResource):
    class Meta:
        model = AffiliateKinship


class AffiliateKinshipAdmin(ImportExportModelAdmin):
    model = AffiliateKinship


class AffiliateResource(resources.ModelResource):
    class Meta:
        model = Affiliate


class AffiliateKinshipInline(admin.TabularInline):
    model = AffiliateKinship
    extra = 1
    raw_id_fields = ("policy",)


class AffiliateAddressInline(admin.TabularInline):
    model = AffiliateAddress
    extra = 1


class AffiliateEmailInline(admin.TabularInline):
    model = AffiliateEmail
    fields = ('email', 'primary')
    extra = 1


class AffiliatePhoneNumberInline(admin.TabularInline):
    model = AffiliatePhoneNumber
    fields = ('phone_number', 'primary')
    extra = 1


class AffiliateAdmin(ImportExportModelAdmin):
    model = Affiliate
    date_hierarchy = 'date_joined'
    fields = (
        ('first_name', 'last_name', 'dni'),
        ('gender', 'birthdate'),
        ('is_active', 'date_joined'),
        ('from_validator', 'registered_by'),
    )
    list_display = ('first_name', 'last_name', 'dni', 'date_joined', 'is_active')
    list_filter = ('is_active', 'registered_by', 'date_joined')
    search_fields = ['first_name', 'last_name', 'dni']

    inlines = (
        AffiliateKinshipInline,
        AffiliatePhoneNumberInline,
        AffiliateEmailInline,
        AffiliateAddressInline,
    )


class AffiliateEmailResource(resources.ModelResource):
    class Meta:
        model = AffiliateEmail


class AffiliateEmailAdmin(ImportExportModelAdmin):
    model = AffiliateEmail


class AffiliatePhoneNumberResource(resources.ModelResource):
    class Meta:
        model = AffiliatePhoneNumber


class AffiliatePhoneNumberAdmin(ImportExportModelAdmin):
    model = AffiliatePhoneNumber


class AffiliateAddressResource(resources.ModelResource):
    class Meta:
        model = AffiliateAddress


class AffiliateAddressAdmin(ImportExportModelAdmin):
    model = AffiliateAddress


admin.site.register(Affiliate, AffiliateAdmin)
admin.site.register(AffiliateEmail, AffiliateEmailAdmin)
admin.site.register(AffiliatePhoneNumber, AffiliatePhoneNumberAdmin)
admin.site.register(AffiliateAddress, AffiliateAddressAdmin)
admin.site.register(AffiliateKinship, AffiliateKinshipAdmin)