from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AffiliatesConfig(AppConfig):
    name = 'affiliates'
    verbose_name = _("afiliados")
    