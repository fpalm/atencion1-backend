"""
Los modelos, vistas y serializadores relacionados con la información de los
afiliados en Atención1. Además del modelo base para afiliados, incluye modelos
para los teléfonos, correos electrónicos y direcciones de los afiliados.
"""
default_app_config = 'affiliates.apps.AffiliatesConfig'
