import os
import mimetypes
import argparse

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'atencion1.settings')

from django.conf import settings

from django.apps import apps as django_apps
from django.utils.module_loading import import_string

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from django.utils.encoding import force_text
from django.utils.translation import ugettext as _

from import_export.formats import base_formats
from import_export.resources import modelresource_factory

from affiliates.models import Affiliate
from affiliates.admin import AffiliateResource
from policies.models import Policy

import pandas as pd

FORMATS = {
    None: base_formats.CSV,
    'text/csv': base_formats.CSV,
    'application/json': base_formats.JSON,
    'text/yaml': base_formats.YAML,
    'text/tab-separated-values': base_formats.TSV,
    'application/vnd.oasis.opendocument.spreadsheet': base_formats.ODS,
    'text/html': base_formats.HTML,
    'application/vnd.ms-excel': base_formats.XLS,
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
        base_formats.XLSX,
}

class Command(BaseCommand):
    help = 'Migrate data from files to the database'

    def add_arguments(self, parser):
        parser.formatter_class = argparse.ArgumentDefaultsHelpFormatter
        resource_def = parser.add_mutually_exclusive_group(required=True)

        parser.add_argument(
            'file-path',
            metavar='file-path',
            nargs=1,
            help='File path to import')
        resource_def.add_argument(
            '--resource-class',
            dest='resource_class',
            default=None,
            help='Resource class as dotted path,'
            'ie: mymodule.resources.MyResource')
        resource_def.add_argument(
            '--model-name',
            dest='model_name',
            default=None,
            help='Model name, ie: auth.User')

    def get_resource_class(self, resource_class, model_name):
        if resource_class:
            return import_string(resource_class)
        return modelresource_factory(django_apps.get_model(model_name))

    @transaction.atomic
    def handle(self, **options):
        export_file_name = options.get('file-path')[0]
        mimetype = mimetypes.guess_type(export_file_name)[0]
        export_format = FORMATS[mimetype]()
        resource_class = self.get_resource_class(
            options.get('resource_class'),
            options.get('model_name')
        )
        resource = resource_class()
        write_mode = f"w{'b' if export_format.is_binary() else ''}"
        dataset = resource.export()
        data = getattr(dataset, export_format.get_title())
        with open(export_file_name, write_mode) as export_file:
            export_file.write(data)