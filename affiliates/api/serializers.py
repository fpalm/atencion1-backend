from django.contrib.auth.models import User
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers

from affiliates.models import (
    Affiliate,
    AffiliateEmail,
    AffiliatePhoneNumber,
    AffiliateAddress,
    Policy,
    AffiliateKinship,
)
from policies.api.serializers import PolicySerializer, KinshipAffiliateSerializer
from profiles.api.serializers import UserSerializer
from locations.models import AdminThirdLevel, Place


class AffiliateEmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffiliateEmail
        fields = ("id", "affiliate", "email", "primary")


class AffiliatePhoneNumberSerializer(serializers.ModelSerializer):

    phone_number = PhoneNumberField()

    class Meta:
        model = AffiliatePhoneNumber
        fields = ("id", "affiliate", "phone_number", "primary")


class AffiliateAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffiliateAddress
        exclude = ("last_update_user",)
        depth = 4


class AffiliateAddressSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffiliateAddress
        fields = "__all__"

    depth = 1


class AffiliateKinshipSerializer(serializers.ModelSerializer):

    policy = PolicySerializer(read_only=True)
    affiliate = KinshipAffiliateSerializer(read_only=True)

    class Meta:
        model = AffiliateKinship
        fields = ("id", "policy", "affiliate", "kinship")


class AffiliateKinshipSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffiliateKinship
        fields = ("policy", "kinship")


class AffiliateSerializer(serializers.ModelSerializer):

    policies = AffiliateKinshipSerializer(
        source="affiliatekinship_set", many=True, read_only=True
    )
    emails = AffiliateEmailSerializer(many=True)
    phone_numbers = AffiliatePhoneNumberSerializer(many=True)
    gender = serializers.CharField(source="get_gender_display")
    affiliate_addresses = AffiliateAddressSerializer(many=True)
    registered_by = UserSerializer()

    class Meta:
        model = Affiliate
        fields = "__all__"


class AffiliateSimpleSerializer(serializers.ModelSerializer):

    policies = AffiliateKinshipSimpleSerializer(
        source="affiliatekinship_set", many=True, read_only=True
    )
    emails = AffiliateEmailSerializer(many=True)
    phone_numbers = AffiliatePhoneNumberSerializer(many=True)
    affiliate_addresses = AffiliateAddressSimpleSerializer(many=True)

    class Meta:
        model = Affiliate
        fields = "__all__"

    def create(self, validated_data):
        phone_numbers = validated_data.pop("phone_numbers")
        emails = validated_data.pop("emails")
        addresses = validated_data.pop("affiliate_addresses")
        affiliate = Affiliate.objects.create(**validated_data)

        if "policies" in self.initial_data:
            policies = self.initial_data.get("policies")
            for policy in policies:
                policy_id = policy.get("policy")
                kinship = policy.get("kinship")
                policy_instance = Policy.objects.get(pk=policy_id)
                AffiliateKinship(
                    affiliate=affiliate, policy=policy_instance, kinship=kinship
                ).save()

        for phone_number in phone_numbers:
            AffiliatePhoneNumber.objects.create(affiliate=affiliate, **phone_number)
        for email in emails:
            AffiliateEmail.objects.create(affiliate=affiliate, **email)
        for address in addresses:
            AffiliateAddress.objects.create(Affiliate=affiliate, **address)

        return affiliate

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.gender = validated_data.get("gender", instance.gender)
        instance.birthdate = validated_data.get("birthdate", instance.birthdate)
        instance.is_active = validated_data.get("is_active", instance.is_active)
        instance.dni = validated_data.get("dni", instance.dni)
        instance.from_validator = validated_data.get("from_validator", instance.from_validator)
        instance.registered_by = validated_data.get("registered_by", instance.registered_by)

        if "policies" in self.initial_data:
            policies = self.initial_data.get("policies")
            # AffiliateKinship.objects.filter(affiliate=instance.pk).delete() # No delete old kinship
            instance.policies.clear()
            for policy in policies:
                policy_id = policy.get("policy")
                kinship = policy.get("kinship")
                policy_instance = Policy.objects.get(pk=policy_id)
                AffiliateKinship(
                    affiliate=instance, policy=policy_instance, kinship=kinship
                ).save()
        if "phone_numbers" in self.initial_data:
            phone_numbers = self.initial_data.get("phone_numbers")
            AffiliatePhoneNumber.objects.filter(affiliate=instance.pk).delete()
            instance.phone_numbers.clear()
            for phone_number in phone_numbers:
                AffiliatePhoneNumber.objects.create(affiliate=instance, **phone_number)
        if "emails" in self.initial_data:
            emails = self.initial_data.get("emails")
            AffiliateEmail.objects.filter(affiliate=instance.pk).delete()
            instance.emails.clear()
            for email in emails:
                AffiliateEmail.objects.create(affiliate=instance, **email)
        if "affiliate_addresses" in self.initial_data:
            affiliate_addresses = self.initial_data.get("affiliate_addresses")
            instance.affiliate_addresses.clear()
            for address in affiliate_addresses:
                user = address.pop("last_update_user", None)
                last_update_user = User.objects.get(pk=user)
                region = address.pop("region", None)
                place = address.pop("place", None)
                if region:
                    region = AdminThirdLevel.objects.get(pk=region)
                if place:
                    place = Place.objects.get(pk=place)
                AffiliateAddress.objects.create(Affiliate=instance, last_update_user=last_update_user, region=region, place=place, **address)

        instance.save()
        return instance


class AffiliateMinimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Affiliate
        fields = ("id", "first_name", "last_name")
