from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from .serializers import (
    AffiliateSerializer,
    AffiliateEmailSerializer,
    AffiliatePhoneNumberSerializer,
    AffiliateAddressSerializer,
    AffiliateSimpleSerializer,
    AffiliateKinshipSerializer,
)
from ..models import (
    Affiliate,
    AffiliateEmail,
    AffiliatePhoneNumber,
    AffiliateAddress,
    AffiliateKinship,
)


# from profiles.custom_permissions import (IsAnyAuthenticatedUser,
#                                          IsOperatorUser,
#                                          IsCoordinatorUser,
#                                          IsManagerUser
#                                          )


class AffiliateListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Affiliate.objects.all()
    serializer_class = AffiliateSerializer

    filter_fields = (
        "policies",
        "is_active",
        "gender",
        "from_validator",
        "registered_by",
        "dni",
    )
    search_fields = (
        "^first_name",
        "^last_name",
        "dni",
        "^emails__email",
        "^affiliatekinship__policy__code",
    )
    ordering_fields = (
        "first_name",
        "last_name",
        "policies",
        "birthdate",
        "date_joined",
        "dni",
    )


class AffiliateSimpleListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Affiliate.objects.all()
    serializer_class = AffiliateSimpleSerializer

    filter_fields = ("policies", "is_active", "gender")
    search_fields = (
        "^first_name",
        "^last_name",
        "dni",
        "^emails__email",
        "^affiliatekinship__policy__code",
    )
    ordering_fields = (
        "first_name",
        "last_name",
        "policies",
        "dni",
    )


class AffiliateDetailView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Affiliate.objects.all()
    serializer_class = AffiliateSerializer


class AffiliateSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Affiliate.objects.all()
    serializer_class = AffiliateSimpleSerializer


class AffiliateEmailListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AffiliateEmail.objects.all()
    serializer_class = AffiliateEmailSerializer

    filter_fields = (
        "affiliate",
        "primary",
    )
    search_fields = ("^email",)


class AffiliateEmailDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AffiliateEmail.objects.all()
    serializer_class = AffiliateEmailSerializer


class AffiliatePhoneNumberListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AffiliatePhoneNumber.objects.all()
    serializer_class = AffiliatePhoneNumberSerializer

    filter_fields = (
        "affiliate",
        "primary",
    )
    search_fields = ("^phone_number",)


class AffiliatePhoneNumberDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AffiliatePhoneNumber.objects.all()
    serializer_class = AffiliatePhoneNumberSerializer


class AffiliateAddressListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AffiliateAddress.objects.all()
    serializer_class = AffiliateAddressSerializer

    filter_fields = (
        "AddressType",
        "Affiliate",
        "place",
        "region",
        "last_update_user",
    )
    search_fields = (
        "^address_line1",
        "^address_line2",
        "^zip_code",
    )
    ordering_fields = ("last_update_time",)


class AffiliateAddressDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AffiliateAddress.objects.all()
    serializer_class = AffiliateAddressSerializer


class AffiliateKinshipListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AffiliateKinship.objects.all()
    serializer_class = AffiliateKinshipSerializer


class AffiliateKinshipDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AffiliateKinship.objects.all()
    serializer_class = AffiliateKinshipSerializer
