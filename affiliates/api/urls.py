#from rest_framework import routers
from django.urls import path
from . import views

app_name = 'affiliates'

urlpatterns = [
    path(
        'affiliates',
        views.AffiliateListView.as_view(),
        name='affiliate_list'
    ),
    path(
        'affiliates/<pk>',
        views.AffiliateDetailView.as_view(),
        name='affiliate_detail'
    ),
    path(
        'affiliates-simple',
        views.AffiliateSimpleListView.as_view(),
        name='affiliate_simple_list'
    ),
    path(
        'affiliates-simple/<pk>',
        views.AffiliateSimpleDetailView.as_view(),
        name='affiliate_simple_detail'
    ),
    path(
        'affiliate-addresses',
        views.AffiliateAddressListView.as_view(),
        name='affiliate_address_list'
    ),
    path(
        'affiliate-addresses/<pk>',
        views.AffiliateAddressDetailView.as_view(),
        name='affiliate_address_detail'
    ),
    path(
        'affiliate-phone-numbers',
        views.AffiliatePhoneNumberListView.as_view(),
        name='affiliate_phone_number_list'
    ),
    path(
        'affiliate-phone-numbers/<pk>',
        views.AffiliatePhoneNumberDetailView.as_view(),
        name='affiliate_phone_number_detail'
    ),
    path(
        'affiliate-emails',
        views.AffiliateEmailListView.as_view(),
        name='affiliate_email_list'
    ),
    path(
        'affiliate-emails/<pk>',
        views.AffiliateEmailDetailView.as_view(),
        name='affiliate_email_detail'
    ),
    path(
        'affiliate-kinships',
        views.AffiliateKinshipListView.as_view(),
        name='affiliate_kinship_list'
    ),
    path(
        'affiliate-kinships/<pk>',
        views.AffiliateKinshipDetailView.as_view(),
        name='affiliate_kinship_detail'
    ),
]