"""
Modelos, vistas y serializadores relacionados con las atenciones.
"""

default_app_config = 'attentions.apps.AttentionsConfig'