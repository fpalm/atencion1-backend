from django.db import models
from django.db.models import Case, When
from django.db.models import Q
from django.utils import timezone

from services.models import TMG, HHC, LAB, HHP, HMD, Transfer


class AttentionManager(models.Manager):
    """Manager para filtrar el modelo de Atención.
    """

    def in_progress(self):
        """Atenciones en las cuales cualquiera de sus servicios tiene el status IN PROGRESS (en progreso).

        Arguments:
            None.
        
        Returns:
            list -- Lista de atenciones filtradas.
        """
        return self.filter(
            Q(status="OPEN")
            & (
                Q(services_tmg_attention__status="IN_PROGRESS")
                | Q(services_hhc_attention__status="IN_PROGRESS")
                | Q(services_lab_attention__status="IN_PROGRESS")
                | Q(services_hhp_attention__status="IN_PROGRESS")
                | Q(services_hmd_attention__status="IN_PROGRESS")
                | Q(services_transfer_attention__status="IN_PROGRESS")
            )
        ).distinct()

    def open_or_delayed(self):
        """Atenciones en las cuales hay por lo menos un servicio con el status OPEN (abierto) o DELAYED (retrasado).
        Incluye atenciones sin servicios relacionados.

        Arguments:
            None.
        
        Returns:
            list -- Lista de atenciones filtradas.
        """
        return self.filter(
            Q(status="OPEN")
            & (
                (
                    Q(services_tmg_attention__status="OPEN",)
                    | Q(services_hhc_attention__status="OPEN",)
                    | Q(services_lab_attention__status="OPEN",)
                    | Q(services_hhp_attention__status="OPEN",)
                    | Q(services_transfer_attention__status="OPEN",)
                    | Q(services_tmg_attention__status="DELAYED",)
                    | Q(services_hhc_attention__status="DELAYED",)
                    | Q(services_lab_attention__status="DELAYED",)
                    | Q(services_hhp_attention__status="DELAYED",)
                    | Q(services_hmd_attention__status="DELAYED",)
                    | Q(services_transfer_attention__status="DELAYED",)
                )
                | (
                    Q(services_tmg_attention=None)
                    & Q(services_hhc_attention=None)
                    & Q(services_lab_attention=None)
                    & Q(services_hhp_attention=None)
                    & Q(services_hmd_attention=None)
                    & Q(services_transfer_attention=None)
                )
            )
        ).distinct()

    def attentions_in_verification(self):
        """Atenciones que tienen al menos un servicio HMD (EMD) con status OPEN (abierto).

        Arguments:
            None.

        Returns:
            list -- Lista de atenciones filtradas.
        """
        return self.filter(
            Q(status="OPEN") & Q(services_hmd_attention__status="OPEN"),
        ).distinct()

    def completed(self):
        """Atenciones en las cuales haya un servicio con el status COMPLETED (completado) o CANCELLED (cancelado).

        Arguments:
            None.

        Returns:
            list -- Lista de atencions filtradas.
        """
        return self.filter(
            Q(status="OPEN")
            & (
                Q(services_tmg_attention__status="COMPLETED")
                | Q(services_hhc_attention__status="COMPLETED")
                | Q(services_lab_attention__status="COMPLETED")
                | Q(services_hhp_attention__status="COMPLETED")
                | Q(services_hmd_attention__status="COMPLETED")
                | Q(services_transfer_attention__status="COMPLETED")
                | Q(services_tmg_attention__status="CANCELED")
                | Q(services_hhc_attention__status="CANCELED")
                | Q(services_lab_attention__status="CANCELED")
                | Q(services_hhp_attention__status="CANCELED")
                | Q(services_hmd_attention__status="CANCELED")
                | Q(services_transfer_attention__status="CANCELED")
            )
        ).distinct()

    def next_scheduled(self):
        """Atenciones con servicios programados cuya fecha/hora de programación sea mayor que la fecha/hora actual.
        Las atenciones se ordenan de forma creciente de acuerdo con la fecha/hora de programación de los servicios.

                Arguments:
                    None.

                Returns:
                    list -- Lista de atenciones filtradas.
                """
        now = timezone.now()
        tmgs = TMG.objects.filter((Q(scheduled_date_time__gt=now))).values(
            "attention", "scheduled_date_time"
        )
        hhcs = HHC.objects.filter((Q(scheduled_date_time__gt=now))).values(
            "attention", "scheduled_date_time"
        )
        labs = LAB.objects.filter((Q(scheduled_date_time__gt=now))).values(
            "attention", "scheduled_date_time"
        )
        hhps = HHP.objects.filter((Q(scheduled_date_time__gt=now))).values(
            "attention", "scheduled_date_time"
        )
        hmds = HMD.objects.filter((Q(scheduled_date_time__gt=now))).values(
            "attention", "scheduled_date_time"
        )
        transfers = Transfer.objects.filter((Q(scheduled_date_time__gt=now))).values(
            "attention", "scheduled_date_time"
        )
        services = (
            list(tmgs)
            + list(hhcs)
            + list(labs)
            + list(hhps)
            + list(hmds)
            + list(transfers)
        )
        sorted_atts_ids = [
            service["attention"]
            for service in sorted(services, key=lambda x: x["scheduled_date_time"])
        ]
        preserved = Case(
            *[When(pk=pk, then=pos) for pos, pk in enumerate(sorted_atts_ids)]
        )
        attentions = self.filter(pk__in=sorted_atts_ids).order_by(preserved)

        return attentions


class AttentionPhoneNumberManager(models.Manager):
    """Manager para filtrar el modelo de AttentionPhoneNumber.
    """

    def by_affiliate(self, affiliate_id: int):
        """Todos los números de teléfonos relacionados con un afiliado.

        Arguments:
            affiliate {pk} -- Affiliate ID.

        Returns:
            list -- Lista de números telefónicos.
        """
        return (
            self.filter(Q(attention__affiliate__pk=affiliate_id))
            .values("phone_number")
            .distinct()
        )
