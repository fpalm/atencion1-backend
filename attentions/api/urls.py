#from rest_framework import routers
from django.urls import path

from . import views

app_name = 'attentions'

urlpatterns = [
    path(
        'attentions',
        views.AttentionListView.as_view(),
        name='attention_list'
    ),
    path(
        'attentions/<pk>',
        views.AttentionDetailView.as_view(),
        name='attention_detail'
    ),
    path(
        'attention-phone-numbers',
        views.AttentionPhoneNumberListView.as_view(),
        name='attention_phone_number_list'
    ),
    path(
        'attention-phone-numbers-by-affiliate/<pk>',
        views.AttentionPhoneNumberByAffiliateListView.as_view(),
        name='attention_phone_number_by_affiliate_list'
    ),
    path(
        'attention-phone-numbers/<pk>',
        views.AttentionPhoneNumberDetailView.as_view(),
        name='attention_detail'
    ),
    path(
        'attentions-simple',
        views.AttentionSimpleListView.as_view(),
        name='attention_list_no_embedded'
    ),
    path(
        'attentions-simple/<pk>',
        views.AttentionSimpleDetailView.as_view(),
        name='attention_detail_no_embedded'
    ),
    path(
        'attentions-in-progress',
        views.AttentionListInProgressView.as_view(),
        name='attention_list_services_in_progress'
    ),
    path(
        'attentions-open-or-delayed',
        views.AttentionListOpenOrDelayedView.as_view(),
        name='attention_list_services_open_or_delayed'
    ),
    path(
        'attentions-in-verification',
        views.AttentionListInVerification.as_view(),
        name='attention_list_in_verification'
    ),
    path(
        'attentions-completed',
        views.AttentionListCompletedView.as_view(),
        name='attention_list_services_completed'
    ),
    path(
        'attentions-next-scheduled',
        views.AttentionListNextScheduledView.as_view(),
        name='attention_list_services_next_scheduled'
    ),
    path(
        'attentions-report',
        views.AttentionListReport.as_view({'get': 'list'}),
        name='attention_list_report'
    ),
]
