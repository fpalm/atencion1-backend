from django_filters import rest_framework as filters
from django.utils.translation import gettext

from drf_renderer_xlsx.mixins import XLSXFileMixin
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ReadOnlyModelViewSet

from atencion1.report_styles import header_style, body_style
from .serializers import (
    AttentionSerializer,
    AttentionSimpleSerializer,
    AttentionMinimalSerializer,
    AttentionPhoneNumberSerializer,
    AttentionReportSerializer,
)
from ..models import Attention, AttentionPhoneNumber

from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator

# from profiles.custom_permissions import (IsAnyAuthenticatedUser,
#                                          IsOperatorUser,
#                                          IsCoordinatorUser,
#                                          IsManagerUser
#                                          )


FILTER_FIELDS_ATTENTION = (
    "user",
    "affiliate",
    "follow_up",
    "status",
    "operation_state",
    "services_tmg_attention__status",
    "services_hhc_attention__status",
    "services_hhp_attention__status",
    "services_hmd_attention__status",
    "services_lab_attention__status",
    "services_transfer_attention__status",
    "services_tmg_attention__scheduled",
    "services_hhc_attention__scheduled",
    "services_hhp_attention__scheduled",
    "services_hmd_attention__scheduled",
    "services_lab_attention__scheduled",
    "services_transfer_attention__scheduled",
)


@method_decorator(cache_page(30), name="dispatch")
class AttentionListView(generics.ListAPIView):
    permission_classes = (
        IsAuthenticated,
        # IsOperatorUser |
        # IsCoordinatorUser |
        # IsManagerUser,
    )

    queryset = Attention.objects.all()
    serializer_class = AttentionSerializer

    filter_fields = FILTER_FIELDS_ATTENTION
    search_fields = ("^user", "^affiliate", "created", "@reason_for_care")
    ordering_fields = (
        "user",
        "affiliate",
        "created",
        "last_modified",
        "end",
    )


class AttentionDetailView(generics.RetrieveAPIView):
    permission_classes = (
        IsAuthenticated,
        # IsOperatorUser |
        # IsCoordinatorUser |
        # IsManagerUser,
    )

    queryset = Attention.objects.all()
    serializer_class = AttentionSerializer


class AttentionSimpleListView(generics.ListCreateAPIView):
    permission_classes = (
        IsAuthenticated,
        # IsOperatorUser |
        # IsCoordinatorUser |
        # IsManagerUser,
    )

    queryset = Attention.objects.all()
    serializer_class = AttentionSimpleSerializer

    filter_fields = FILTER_FIELDS_ATTENTION
    search_fields = ("created", "@reason_for_care")
    ordering_fields = (
        "user",
        "affiliate",
        "created",
        "last_modified",
    )


class AttentionSimpleDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (
        IsAuthenticated,
        # IsOperatorUser |
        # IsCoordinatorUser |
        # IsManagerUser,
    )

    queryset = Attention.objects.all()
    serializer_class = AttentionSimpleSerializer


@method_decorator(cache_page(30), name="dispatch")
class AttentionListInProgressView(generics.ListAPIView):
    permission_classes = (
        IsAuthenticated,
        # IsOperatorUser |
        # IsCoordinatorUser |
        # IsManagerUser,
    )

    serializer_class = AttentionMinimalSerializer

    filter_fields = FILTER_FIELDS_ATTENTION
    search_fields = ("^user", "^affiliate", "created", "@reason_for_care")
    ordering_fields = (
        "user",
        "affiliate",
        "created",
        "last_modified",
    )

    def get_queryset(self):
        return Attention.objects.in_progress()


@method_decorator(cache_page(30), name="dispatch")
class AttentionListOpenOrDelayedView(generics.ListAPIView):
    permission_classes = (
        IsAuthenticated,
        # IsOperatorUser |
        # IsCoordinatorUser |
        # IsManagerUser,
    )

    serializer_class = AttentionMinimalSerializer

    filter_fields = FILTER_FIELDS_ATTENTION
    search_fields = ("^user", "^affiliate", "created", "@reason_for_care")
    ordering_fields = (
        "user",
        "affiliate",
        "created",
        "last_modified",
    )

    def get_queryset(self):
        return Attention.objects.open_or_delayed()


@method_decorator(cache_page(30), name="dispatch")
class AttentionListInVerification(generics.ListAPIView):
    permission_classes = (
        IsAuthenticated,
        # IsOperatorUser |
        # IsCoordinatorUser |
        # IsManagerUser,
    )

    serializer_class = AttentionMinimalSerializer

    filter_fields = FILTER_FIELDS_ATTENTION
    search_fields = ("^user", "^affiliate", "created", "@reason_for_care")
    ordering_fields = (
        "user",
        "affiliate",
        "created",
        "last_modified",
    )

    def get_queryset(self):
        return Attention.objects.attentions_in_verification()


@method_decorator(cache_page(30), name="dispatch")
class AttentionListCompletedView(generics.ListAPIView):
    permission_classes = (
        IsAuthenticated,
        # IsOperatorUser |
        # IsCoordinatorUser |
        # IsManagerUser,
    )

    serializer_class = AttentionMinimalSerializer

    filter_fields = FILTER_FIELDS_ATTENTION
    search_fields = ("^user", "^affiliate", "created", "@reason_for_care")
    ordering_fields = (
        "user",
        "affiliate",
        "created",
        "last_modified",
    )

    def get_queryset(self):
        return Attention.objects.completed()


@method_decorator(cache_page(30), name="dispatch")
class AttentionListNextScheduledView(generics.ListAPIView):
    permission_classes = (
        IsAuthenticated,
        # IsOperatorUser |
        # IsCoordinatorUser |
        # IsManagerUser,
    )

    serializer_class = AttentionMinimalSerializer

    filter_fields = FILTER_FIELDS_ATTENTION
    search_fields = ("^user", "^affiliate", "created", "@reason_for_care")
    ordering_fields = (
        "user",
        "affiliate",
        "created",
        "last_modified",
    )

    def get_queryset(self):
        return Attention.objects.next_scheduled()


class AttentionPhoneNumberListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AttentionPhoneNumber.objects.all()
    serializer_class = AttentionPhoneNumberSerializer

    search_field = ("^phone_number",)


class AttentionPhoneNumberDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AttentionPhoneNumber.objects.all()
    serializer_class = AttentionPhoneNumberSerializer


class AttentionPhoneNumberByAffiliateListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AttentionPhoneNumberSerializer

    def get_queryset(self):
        affiliate_id = self.kwargs.get("pk")
        return AttentionPhoneNumber.objects.by_affiliate(affiliate_id)


class AttentionCreatedRange(filters.FilterSet):
    created = filters.DateFromToRangeFilter()
    end = filters.DateFromToRangeFilter()
    client = filters.CharFilter(
        field_name="affiliate__policies__plan__organization__name",
        lookup_expr="icontains",
        label="cliente",
    )

    class Meta:
        model = Attention
        fields = ["created"]


attentions_head = [
    (gettext("#"), 5),
    (gettext("Clientes"), 20),
    (gettext("Contratantes"), 20),
    (gettext("Creación"), 20),
    (gettext("Finalización"), 20),
    (gettext("Estado"), 10),
    (gettext("Operador"), 30),
    (gettext("Nombres"), 20),
    (gettext("Apellidos"), 20),
    (gettext("Cédula"), 15),
    (gettext("Fecha de Nacimiento"), 15),
    (gettext("Sexo"), 10),
    (gettext("Teléfonos afiliado"), 30),
    (gettext("Teléfonos atención"), 30),
    (gettext("Motivo de consulta"), 15),
    # (gettext("Motivo de cierre"), 20),
    (gettext("Observaciones"), 40),
    (gettext("OMTs"), 15),
    (gettext("AMDs"), 15),
    (gettext("TLDs"), 15),
    (gettext("EMDs"), 15),
    (gettext("PHDs"), 15),
    (gettext("LABs"), 15),
]
attentions_titles = [title for title, width in attentions_head]

attentions_widths = [width for title, width in attentions_head]


class AttentionListReport(XLSXFileMixin, ReadOnlyModelViewSet):
    queryset = Attention.objects.all()
    serializer_class = AttentionReportSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = AttentionCreatedRange

    column_header = {
        "titles": attentions_titles,
        "column_width": attentions_widths,
        "height": 20,
        "style": header_style,
    }
    body = {
        "height": 20,
        "style": body_style,
    }
