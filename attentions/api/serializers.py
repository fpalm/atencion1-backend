import phonenumbers
import pytz
from django.contrib.auth.models import User
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers

from affiliates.api.serializers import AffiliateSerializer, AffiliateMinimalSerializer
from attentions.models import Attention, AttentionPhoneNumber
from documents.api.serializers import MedicalDocumentSerializer
from policies.models import Policy
from profiles.api.serializers import UserSerializer
from services.api.serializers import (
    TMGSerializer,
    HHCSerializer,
    LABSerializer,
    HHPSerializer,
    HMDSerializer,
    TransferSerializer,
    ServiceSource,
    TMGMinimalSerializer,
    ExternalServiceMinimalSerializer,
)
from services.models import HHP, ServiceAddress


class AttentionPhoneNumberSerializer(serializers.ModelSerializer):

    phone_number = PhoneNumberField()

    class Meta:
        model = AttentionPhoneNumber
        fields = "__all__"


class AttentionSerializer(serializers.ModelSerializer):

    status = serializers.CharField(source="get_status_display")
    affiliate = AffiliateSerializer()
    user = UserSerializer()
    phone_numbers = AttentionPhoneNumberSerializer(many=True)

    services_tmg_attention = serializers.SerializerMethodField()
    services_hhc_attention = serializers.SerializerMethodField()
    services_lab_attention = serializers.SerializerMethodField()
    services_hhp_attention = serializers.SerializerMethodField()
    services_hmd_attention = serializers.SerializerMethodField()
    services_transfer_attention = serializers.SerializerMethodField()
    medical_documents = MedicalDocumentSerializer(read_only=True, many=True)

    class Meta:
        model = Attention
        fields = "__all__"

    def get_services_tmg_attention(self, obj):
        TMGs = obj.services_tmg_attention.all().order_by("start")
        return TMGSerializer(TMGs, many=True).data

    def get_services_hhc_attention(self, obj):
        HHCs = obj.services_hhc_attention.all().order_by("start")
        return HHCSerializer(HHCs, many=True).data

    def get_services_lab_attention(self, obj):
        LABs = obj.services_lab_attention.all().order_by("start")
        return LABSerializer(LABs, many=True).data

    def get_services_hhp_attention(self, obj):
        HHPs = obj.services_hhp_attention.all().order_by("start")
        return HHPSerializer(HHPs, many=True).data

    def get_services_hmd_attention(self, obj):
        HMDs = obj.services_hmd_attention.all().order_by("start")
        return HMDSerializer(HMDs, many=True).data

    def get_services_transfer_attention(self, obj):
        tranfers = obj.services_transfer_attention.all().order_by("start")
        return TransferSerializer(tranfers, many=True).data

    def create(self, validated_data):
        phone_numbers = validated_data.pop("phone_numbers")
        attention = Attention.objects.create(**validated_data)
        for phone_number in phone_numbers:
            AttentionPhoneNumber.objects.create(attention=attention, **phone_number)
        return attention


class AttentionSimpleSerializer(serializers.ModelSerializer):

    phone_numbers = AttentionPhoneNumberSerializer(many=True)

    class Meta:
        model = Attention
        fields = "__all__"

    def create(self, validated_data):
        phone_numbers = validated_data.pop("phone_numbers")
        # affiliate = validated_data.get("affiliate")
        # queryset = Attention.objects.all()
        # for att in queryset.filter(affiliate=affiliate):
        #     if att.status == "OPEN":
        #         raise serializers.ValidationError(
        #             "Error. El afiliado tiene una atención abierta."
        #         )
        attention = Attention.objects.create(**validated_data)
        for phone_number in phone_numbers:
            AttentionPhoneNumber.objects.create(attention=attention, **phone_number)
        return attention

    def update(self, instance, validated_data):
        instance.user = validated_data.get("user", instance.user)
        instance.affiliate = validated_data.get("affiliate", instance.affiliate)
        instance.status = validated_data.get("status", instance.status)
        instance.observations = validated_data.get(
            "observations", instance.observations
        )
        instance.follow_up = validated_data.get("follow_up", instance.follow_up)
        instance.closing_reason = validated_data.get(
            "closing_reason", instance.closing_reason
        )
        instance.end = validated_data.get("end", instance.end)

        if "phone_numbers" in self.initial_data:
            phone_numbers = self.initial_data.get("phone_numbers")
            AttentionPhoneNumber.objects.filter(attention=instance.pk).delete()
            instance.phone_numbers.clear()
            for phone_number in phone_numbers:
                AttentionPhoneNumber.objects.create(attention=instance, **phone_number)

        if "hhps_datetime" in self.initial_data and "hhp" in self.initial_data:
            instance.operation_state = "ASLEEP"
            hhps_datetime = self.initial_data.get("hhps_datetime")
            hhp = self.initial_data.get("hhp")
            hhp.pop("operation_state", "ASLEEP")
            hhp.pop("crew", None)
            hhp.pop("scheduled_date_time", None)
            hhp.pop("successive_requester", None)
            hhp.pop("dispatch_datetime", None)
            hhp_user = hhp.pop("user")
            hhp_destination_address = hhp.pop("destination_address")
            hhp_source = hhp.pop("source")
            hhp_policy = hhp.pop("policy")
            hhp_symptoms = hhp.pop("symptoms", None)
            hhp_diagnoses = hhp.pop("diagnoses", None)
            for hhp_instance in sorted(hhps_datetime):
                hhp_instance = HHP.objects.create(
                    attention=instance,
                    operation_state="ASLEEP",
                    scheduled_date_time=hhp_instance,
                    user=User.objects.get(pk=hhp_user),
                    policy=Policy.objects.get(pk=hhp_policy),
                    destination_address=ServiceAddress.objects.get(pk=hhp_destination_address),
                    source=ServiceSource.objects.get(pk=hhp_source),
                    **hhp
                )
                hhp_instance.save()
                if hhp_symptoms:
                    hhp_instance.symptoms.set(hhp_symptoms)
                if hhp_diagnoses:
                    hhp_instance.diagnoses.set(hhp_diagnoses)

        instance.save()
        return instance


class AttentionMinimalSerializer(serializers.ModelSerializer):

    affiliate = AffiliateMinimalSerializer()
    services_tmg_attention = serializers.SerializerMethodField()
    services_hhc_attention = serializers.SerializerMethodField()
    services_lab_attention = serializers.SerializerMethodField()
    services_hhp_attention = serializers.SerializerMethodField()
    services_hmd_attention = serializers.SerializerMethodField()
    services_transfer_attention = serializers.SerializerMethodField()

    class Meta:
        model = Attention
        fields = (
            "id",
            "operation_state",
            "status",
            "affiliate",
            "created",
            "services_tmg_attention",
            "services_hhc_attention",
            "services_transfer_attention",
            "services_hmd_attention",
            "services_hhp_attention",
            "services_lab_attention"
        )

    def get_services_tmg_attention(self, obj):
        TMGs = obj.services_tmg_attention.all().order_by("start")
        return TMGMinimalSerializer(TMGs, many=True).data

    def get_services_hhc_attention(self, obj):
        HHCs = obj.services_hhc_attention.all().order_by("start")
        return ExternalServiceMinimalSerializer(HHCs, many=True).data

    def get_services_lab_attention(self, obj):
        LABs = obj.services_lab_attention.all().order_by("start")
        return ExternalServiceMinimalSerializer(LABs, many=True).data

    def get_services_hhp_attention(self, obj):
        HHPs = obj.services_hhp_attention.all().order_by("start")
        return ExternalServiceMinimalSerializer(HHPs, many=True).data

    def get_services_hmd_attention(self, obj):
        HMDs = obj.services_hmd_attention.all().order_by("start")
        return ExternalServiceMinimalSerializer(HMDs, many=True).data

    def get_services_transfer_attention(self, obj):
        tranfers = obj.services_transfer_attention.all().order_by("start")
        return ExternalServiceMinimalSerializer(tranfers, many=True).data


class AttentionReportSerializer(serializers.HyperlinkedModelSerializer):
    clients = serializers.SerializerMethodField()
    sponsors = serializers.SerializerMethodField()
    created = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    end = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S%z",
        read_only=True,
        default_timezone=pytz.timezone("America/Caracas"),
    )
    status = serializers.CharField(source="get_status_display")
    operator = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    dni = serializers.SerializerMethodField()
    birthdate = serializers.SerializerMethodField()
    gender = serializers.SerializerMethodField()
    affiliate_phonenumbers = serializers.SerializerMethodField()
    attention_phonenumbers = serializers.SerializerMethodField()
    tmgs = serializers.SerializerMethodField()
    hhcs = serializers.SerializerMethodField()
    transfers = serializers.SerializerMethodField()
    hmds = serializers.SerializerMethodField()
    hhps = serializers.SerializerMethodField()
    labs = serializers.SerializerMethodField()

    def get_clients(self, obj):
        if obj.affiliate.policies:
            return ", ".join(
                [item.plan.organization.name for item in obj.affiliate.policies.all()]
            )
        else:
            return ""

    def get_sponsors(self, obj):
        if obj.affiliate.policies:
            return ", ".join(
                [item.sponsor.name if item.sponsor else "---" for item in obj.affiliate.policies.all()]
            )
        else:
            return ""

    def get_operator(self, obj):
        return "{} {}".format(obj.user.first_name, obj.user.last_name)

    def get_first_name(self, obj):
        return obj.affiliate.first_name

    def get_last_name(self, obj):
        return obj.affiliate.last_name

    def get_dni(self, obj):
        return obj.affiliate.dni

    def get_gender(self, obj):
        return obj.affiliate.get_gender_display()

    def get_birthdate(self, obj):
        return f"{obj.affiliate.birthdate:%Y-%m-%d}"

    # def get_affiliate(self, obj):
    #     return "{} {}".format(obj.affiliate.first_name, obj.affiliate.last_name)

    def get_affiliate_phonenumbers(self, obj):
        if obj.affiliate.phone_numbers:
            return ", ".join(
                [
                    phonenumbers.format_number(
                        item.phone_number, phonenumbers.PhoneNumberFormat.INTERNATIONAL
                    )
                    for item in obj.affiliate.phone_numbers.all()
                ]
            )
        else:
            return ""

    def get_attention_phonenumbers(self, obj):
        if obj.phone_numbers:
            return ", ".join(
                [
                    phonenumbers.format_number(
                        item.phone_number, phonenumbers.PhoneNumberFormat.INTERNATIONAL
                    )
                    for item in obj.phone_numbers.all()
                ]
            )
        else:
            return ""

    def get_tmgs(self, obj):
        if obj.services_tmg_attention:
            return ", ".join(
                [f"OMT{item.id}" for item in obj.services_tmg_attention.all()]
            )
        else:
            return ""

    def get_hhcs(self, obj):
        if obj.services_hhc_attention:
            return ", ".join(
                [f"AMD{item.id}" for item in obj.services_hhc_attention.all()]
            )
        else:
            return ""

    def get_transfers(self, obj):
        if obj.services_transfer_attention:
            return ", ".join(
                [f"TLD{item.id}" for item in obj.services_transfer_attention.all()]
            )
        else:
            return ""

    def get_hmds(self, obj):
        if obj.services_hmd_attention:
            return ", ".join(
                [f"EMD{item.id}" for item in obj.services_hmd_attention.all()]
            )
        else:
            return ""

    def get_hhps(self, obj):
        if obj.services_hhp_attention:
            return ", ".join(
                [f"PHD{item.id}" for item in obj.services_hhp_attention.all()]
            )
        else:
            return ""

    def get_labs(self, obj):
        if obj.services_lab_attention:
            return ", ".join(
                [f"LAB{item.id}" for item in obj.services_lab_attention.all()]
            )
        else:
            return ""


    class Meta:
        model = Attention
        fields = [
            "id",
            "clients",
            "sponsors",
            "created",
            "end",
            "status",
            "operator",
            "first_name",
            "last_name",
            "dni",
            "birthdate",
            "gender",
            "affiliate_phonenumbers",
            "attention_phonenumbers",
            "reason_for_care",
            # "closing_reason",
            "observations",
            "tmgs",
            "hhcs",
            "transfers",
            "hmds",
            "hhps",
            "labs"
        ]
