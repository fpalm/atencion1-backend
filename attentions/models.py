"""
Modelos relacionados con las atenciones. Las atenciones agrupan a un conjunto de
servicios que se ejecutan a partir de una misma solicitud inicial.
"""

import datetime

import pytz
from babel.dates import format_datetime
from django.conf import settings
from django.core.cache import cache
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from affiliates.models import Affiliate
from attentions.managers import AttentionManager, AttentionPhoneNumberManager

default_btime = datetime.datetime.strptime("1980-01-01", "%Y-%m-%d")
default_btime = default_btime.replace(tzinfo=pytz.timezone(settings.TIME_ZONE))


class Attention(models.Model):
    """
    **Atención**

    Una atención agrupa los servicios que se generan desde una misma solicitud.
    Los servicios dentro de una atención se consideran entonces "sucesivos" entre si.
    """

    ATTENTION_STATUS = (
        ("OPEN", _("Abierta")),
        ("CLOSED", _("Cerrada")),
    )
    ATTENTION_OPERATION_STATE = (
        ("ASLEEP", _("Dormido")),
        ("AWAKE", _("Despierto")),
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="attentions",
        verbose_name="usuario",
    )
    affiliate = models.ForeignKey(
        Affiliate,
        on_delete=models.CASCADE,
        related_name="attentions",
        verbose_name="afiliado",
    )
    reason_for_care = models.TextField(
        _("motivo de consulta"),
        help_text=_(
            "Descripción breve de la razón que motiva la solicitud de servicios."
        ),
        default=_("Motivo de la consulta no especificada."),
    )
    created = models.DateTimeField(_("creado en"), blank=True, null=True)
    last_modified = models.DateTimeField(_("última modificación"), auto_now=True)
    status = models.CharField(
        _("estado"), max_length=25, choices=ATTENTION_STATUS, default="OPEN"
    )
    operation_state = models.CharField(
        _("estado de la operación"), max_length=25, choices=ATTENTION_OPERATION_STATE, default="AWAKE"
    )
    observations = models.TextField(_("observaciones"), blank=True)
    follow_up = models.BooleanField(_("seguimiento"), default=False)
    closing_reason = models.TextField(_("motivo de cierre"), blank=True, null=True)
    end = models.DateTimeField(_("finalización"), blank=True, null=True)

    objects = AttentionManager()

    def save(self, *args, **kwargs):
        stata = list()
        for hhc in self.services_hhc_attention.all():
            stata.append(hhc.status)
        for tmg in self.services_tmg_attention.all():
            stata.append(tmg.status)
        for hhp in self.services_hhp_attention.all():
            stata.append(hhp.status)
        for hmd in self.services_hmd_attention.all():
            stata.append(hmd.status)
        for lab in self.services_lab_attention.all():
            stata.append(lab.status)
        for transfer in self.services_transfer_attention.all():
            stata.append(transfer.status)
        if len(stata) > 0:
            if (len(set(stata)) <= 1) and (stata[0] == 'CLOSED'):
                self.status = 'CLOSED'
                self.end = timezone.now()

        operation_states = list()
        for hhc in self.services_hhc_attention.all():
            operation_states.append(hhc.operation_state)
        for tmg in self.services_tmg_attention.all():
            operation_states.append(tmg.operation_state)
        for hhp in self.services_hhp_attention.all():
            operation_states.append(hhp.operation_state)
        for hmd in self.services_hmd_attention.all():
            operation_states.append(hmd.operation_state)
        for lab in self.services_lab_attention.all():
            operation_states.append(lab.operation_state)
        for transfer in self.services_transfer_attention.all():
            operation_states.append(transfer.operation_state)
        if len(operation_states) > 0:
            if (len(set(operation_states)) <= 1) and (operation_states[0] == 'ASLEEP'):
                self.operation_state = 'ASLEEP'
        if not self.created:
            self.created = datetime.datetime.now()

        super(Attention, self).save(*args, **kwargs)
        cache.clear()

    def __str__(self):
        return f"AT-{self.id}: {self.affiliate}; {self.get_status_display().upper()}; {format_datetime(self.created, 'EEE dd MMM YYYY : HH:mm', locale='es')}"

    class Meta:
        verbose_name = _("Atención")
        verbose_name_plural = _("Atenciones")
        ordering = ("-created",)


class AttentionPhoneNumber(models.Model):
    """
    **Número telefónico de Atención**
    """

    attention = models.ForeignKey(
        Attention,
        on_delete=models.CASCADE,
        related_name="phone_numbers",
        blank=True,
        null=True,
    )
    phone_number = PhoneNumberField(_("número de teléfono"))

    objects = AttentionPhoneNumberManager()

    def __str__(self):
        return str(self.phone_number)

    class Meta:
        verbose_name = _("Número telefónico de la atención")
        verbose_name_plural = _("Números telefónicos de las atenciones")
