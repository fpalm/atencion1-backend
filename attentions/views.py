from datetime import datetime
from datetime import timedelta
from openpyxl import Workbook
from django.http import HttpResponse

from .models import Attention
from services.models import TMG, HHG, Transfer, HMD


def generate_attentions_report(request):
    """Generate attentions report

    Arguments:
        request {[type]} -- [description]
    """
    Attentions.objects.filter(created__gt="")

    response = HttpResponse(
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    response[
        "Content-Disposition"
    ] = "attachment; filename={date}-atenciones.xlsx".format(
        date=datetime.now().strftime("%Y-%m-%d"),
    )
