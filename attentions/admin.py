from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import Attention, AttentionPhoneNumber


class AttentionPhoneNumberAdmin(admin.ModelAdmin):
    model = AttentionPhoneNumber


class AttentionPhoneNumberInline(admin.StackedInline):
    model = AttentionPhoneNumber
    extra = 1


class AttentionResource(resources.ModelResource):
    class Meta:
        model = Attention


class AttentionAdmin(ImportExportModelAdmin):
    model = Attention
    readonly_fields = ("created", "last_modified")
    raw_id_fields = ("affiliate",)
    fields = (
        "user",
        "affiliate",
        "reason_for_care",
        ("created", "end"),
        "last_modified",
        ("status", "operation_state"),
        "observations",
    )
    inlines = (AttentionPhoneNumberInline,)
    list_display = ("attention_id", "affiliate", "created", "end", "services")
    list_filter = ("status", "operation_state", "created", "end")
    search_fields = [
        "id",
        "affiliate__first_name",
        "affiliate__last_name",
        "affiliate__dni__exact"
    ]

    def attention_id(self, obj):
        return obj.id
    attention_id.short_description = "Atención"

    def affiliate(self, obj):
        return str(obj.affiliate)
    affiliate.short_description = "Afiliado"

    def services(self, obj):
        services = []
        services += [f"OMT-{tmg.id}" for tmg in obj.services_tmg_attention.all()]
        services += [f"AMD-{hhc.id}" for hhc in obj.services_hhc_attention.all()]
        services += [f"TLD-{transfer.id}" for transfer in obj.services_transfer_attention.all()]
        services += [f"EMD-{hmd.id}" for hmd in obj.services_hmd_attention.all()]
        services += [f"LAB-{lab.id}" for lab in obj.services_lab_attention.all()]
        services += [f"PHD-{hhp.id}" for hhp in obj.services_hhp_attention.all()]
        return ", ".join(services)
    services.short_description = "Servicios"

admin.site.register(AttentionPhoneNumber, AttentionPhoneNumberAdmin)
admin.site.register(Attention, AttentionAdmin)
