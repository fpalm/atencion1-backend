from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AttentionsConfig(AppConfig):
    name = 'attentions'
    verbose_name = _("Atenciones")
